//
//  BiteBCMainViewController.h
//  BiteBC
//
//  Created by brst on 7/1/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "LoginViewController.h"

@interface BiteBCMainViewController : UIViewController<FBLoginViewDelegate,NSURLConnectionDelegate,UITabBarControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
     UITextView* detailTextView;
    
    UIButton * fastForwdBtn;
    
    NSMutableDictionary* userArray;
    NSMutableDictionary* dict;
    LoginViewController * login;
    NSMutableData* data;

    UIButton * loginButton;
    
    NSURLConnection* connection;
    UIAlertView* alertTextfield;
    
    UIView *customView;
    BOOL popUp;
}
-(void)tabBar;
 
@property(nonatomic,strong) UITabBarController* tabBarController;
@property(nonatomic,strong) UITextField *txt_Mobile_Number;

@end
