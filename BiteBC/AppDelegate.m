//
//  AppDelegate.m
//  BiteBC
//
//  Created by brst on 7/1/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "AppDelegate.h"
#import "BiteBCMainViewController.h"
#import "PaymentViewController.h"
#import "Stripe.h"

@implementation AppDelegate

NSString * const StripePublishableKey = @"pk_test_nfoI2Lwdjj7gyFTLDOQycFSs";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    self.window.backgroundColor =[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
      
    
     appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
     NSLog(@"%@",appVersionString);
    
    [self upgradeApiHit];
    
   // NSString *bundleName = infoDictionary[(NSString *)kCFBundleNameKey];
   
    
    {
       BiteBCMainViewController* biteBc=[[BiteBCMainViewController alloc]init];
       UINavigationController* navigate=[[UINavigationController alloc]initWithRootViewController:biteBc];
       self.window.rootViewController=navigate;
   }
   
    
  
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [Stripe setDefaultPublishableKey:StripePublishableKey];
    [self.window makeKeyAndVisible];
    return YES;
}



#pragma mark - VERSIONAPI Method

-(void)upgradeApiHit
{
        
   
    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/upgrade.php"];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    
    
    NSLog(@"About to send req %@",req.URL);
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if (data ==nil|| data==(id)[NSNull null]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSLog(@"%@",result);
        //if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
            
            NSString *appVersion = [result valueForKey:@"version"];
            
            NSString *message = [result valueForKey:@"message"];
        NSLog(@"%@",message);
            urlLink = [result valueForKey:@"link"];
        
        NSLog(@"%@",appVersionString);
         NSLog(@"%@",appVersion);
            
            if ([appVersion isEqualToString:appVersionString]) {
                
            }else{
              
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Upgrade", nil];
//                
//                [alert show];
            }
            
            
       // }
        
            }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        
    }else{
    
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlLink]];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

 
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [FBSession.activeSession close];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
