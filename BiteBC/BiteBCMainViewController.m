
//
//  BiteBCMainViewController.m
//  BiteBC
//
//  Created by brst on 7/1/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "BiteBCMainViewController.h"
#import "SelectPlanViewController.h"
#import "RestaurantTableViewController.h"
#import "DetailPageTableViewController.h"
#import "SettingTableViewController.h"
#import "NearMeViewController.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "SliderPageViewController.h"
#import "TwillioIntegrationViewController.h"

@interface BiteBCMainViewController ()
{
    UIButton *getStartedBtn;
    NSString* text;
}

@end
 
@implementation BiteBCMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
//    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"fbStatus"] isEqualToString:@"true"])
//    {
//         [FBSession.activeSession close];
//    }
         self.navigationController.navigationBarHidden=YES;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"popUp"])
    {
        [self customAlert];
    }
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"getstart"]isEqualToString:@"success"])
    {
        [self myLoginView];
    }
    
}

 - (void)viewDidLoad
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"]);

    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"]isEqualToString:@"verify"])
    {
            TwillioIntegrationViewController * twilioVC=[[TwillioIntegrationViewController alloc]init];
            [self.navigationController pushViewController:twilioVC animated:YES];
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"fbStatus"];
    [[NSUserDefaults standardUserDefaults]synchronize];
 
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"login.png"]];
 
    text=[NSString stringWithFormat:@"Bite BC is a large scale diners club in the form of a mobile app providing members with discounts on eating out.\n\nEating out can be very expensive, which more often than not stops people from doing it as often as they would like to.\n\nWith our intuitive app you will be able to locate all of our participating restaurants at a touch of a button and will save hundreds of dollars over the course of the year."];
   
    
    if (!IS_IPHONE_5)
    {
        detailTextView=[[UITextView alloc]initWithFrame:CGRectMake(65, 200, 210, 170)];
           }else
    {
        detailTextView=[[UITextView alloc]initWithFrame:CGRectMake(60, 215, 210, 230)];
       
        
    }
        detailTextView.backgroundColor=[UIColor clearColor];
        detailTextView.editable=NO;
        detailTextView.text= text;
        detailTextView.textAlignment=NSTextAlignmentLeft;
        detailTextView.layer.masksToBounds=YES;
        detailTextView.font=[UIFont boldSystemFontOfSize:14];
        detailTextView.textColor=[UIColor whiteColor];
        [self.view addSubview: detailTextView];
  
    getStartedBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [getStartedBtn setFrame:CGRectMake(40, detailTextView.frame.size.height + detailTextView.frame.origin.y + 30, self.view.frame.size.width-80, 35)];
    [getStartedBtn addTarget:self action:@selector(getStartedBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [getStartedBtn setTitle:@"Get Started" forState:UIControlStateNormal];
    
    getStartedBtn.layer.cornerRadius=2.0;
    getStartedBtn.layer.borderWidth=1.0;
    getStartedBtn.layer.borderColor= [UIColor whiteColor].CGColor;
    [getStartedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getStartedBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15];
    //getStartedBtn.backgroundColor=[UIColor colorWithRed:0.5333 green:0.7412 blue:0.7373 alpha:0.5];
    [self.view addSubview:getStartedBtn];
//    
//    fastForwdBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [fastForwdBtn setFrame:CGRectMake(40, getStartedBtn.frame.size.height + getStartedBtn.frame.origin.y + 13, self.view.frame.size.width-80, 20)];
//    [fastForwdBtn addTarget:self action:@selector(fastForwdBtnPressed) forControlEvents:UIControlEventTouchUpInside];
//    [fastForwdBtn setTitle:@"Fast forward it !" forState:UIControlStateNormal];
//    fastForwdBtn.titleLabel.font=[UIFont systemFontOfSize:14];
//    [fastForwdBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//  //getStartedBtn.backgroundColor=[UIColor colorWithRed:0.5333 green:0.7412 blue:0.7373 alpha:0.5];
//    [self.view addSubview:fastForwdBtn];


    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]);

    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]isEqualToString:@"bitebcMode"] && [[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"] isEqualToString:@"verified"])
    {
        [self postcustomerID];
        [customView removeFromSuperview];
    }
    else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]isEqualToString:@"fbMode"]&& [[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"] isEqualToString:@"verified"])
    {
        [self postcustomerID];
        [customView removeFromSuperview];
        }
    
    
    
   }
-(void)myLoginView
{
    [fastForwdBtn removeFromSuperview];
    [getStartedBtn removeFromSuperview];
    
        FBLoginView *loginview =[[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]];
        loginview.frame=CGRectOffset(loginview.frame, 50, detailTextView.frame.size.height + detailTextView.frame.origin.y + 10);
    
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail" ]);
    
        if ([[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]==nil)
        {
                    loginview.delegate=self;
    
    
        }else
        {
    
        }
         [self.view addSubview:loginview];
    
    loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setFrame:CGRectMake(40, loginview.frame.size.height + loginview.frame.origin.y + 4, self.view.frame.size.width-80, 43)];
    [loginButton addTarget:self action:@selector(login_Email_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"Log in with Email" forState:UIControlStateNormal];
    loginButton.titleLabel.font=[UIFont systemFontOfSize:14];
    loginButton.layer.cornerRadius=3.0;
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // loginButton.backgroundColor=[UIColor colorWithRed:0.5333 green:0.7412 blue:0.7373 alpha:0.5];
    [self.view addSubview:loginButton];
     
        

    
}
-(void)getStartedBtnPressed
{
    SliderPageViewController * slideVC=[[SliderPageViewController alloc]init];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:slideVC animated:NO];
}
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
	 	NSLog(@"logout");
       [FBSession.activeSession close];
    
}
 - (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
	NSLog(@"welcome fb");
     
}


- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user
{
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"popUp"];
    [[NSUserDefaults standardUserDefaults]synchronize];
     [self customAlert];
    
     NSString* mail=[user objectForKey:@"email"];
  
    [[NSUserDefaults standardUserDefaults]setObject:mail forKey:@"mail"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:user.first_name forKey:@"first"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:user.last_name forKey:@"last"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:user.name forKey:@"fullname"];
    [[NSUserDefaults standardUserDefaults]synchronize];
  
    [[NSUserDefaults standardUserDefaults]setObject:[user objectForKey:@"id"] forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"fbMode" forKey:@"login"];
    [[NSUserDefaults standardUserDefaults]synchronize];

       
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"base64"];
  
    NSLog(@"%@",[user objectForKey:@"id"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"]);


    
    if ( [[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]==nil)
    {
        
        alertTextfield=[[UIAlertView alloc]initWithTitle:@"Please confirm your Facebook Email" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertTextfield.tag=1;
        [alertTextfield setAlertViewStyle:UIAlertViewStylePlainTextInput];
        
        [alertTextfield show];

        
        
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex== [alertView cancelButtonIndex] && alertView.tag==1)
    {
        UITextField* textF=[alertView textFieldAtIndex:0];
        if(![self NSStringIsValidEmail:textF.text]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=2;
            [alert show];

        }else
        {
            NSLog(@"%@",textF.text);
            [[NSUserDefaults standardUserDefaults]setObject:textF.text forKey:@"mail"];
            [[NSUserDefaults standardUserDefaults]synchronize];

//            if (flag==0)
//            {   [[NSUserDefaults standardUserDefaults]setObject:@"fbMode" forKey:@"login"];
//                [[NSUserDefaults standardUserDefaults]synchronize];
//               [self postData];
//                flag=1;
//            }

        }
        
        
    }
    else if (buttonIndex== [alertView cancelButtonIndex] && alertView.tag==2)
    {
        [alertTextfield show];
    }
   }

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark - LogionApiHit Method

-(void)logionApiHit
{
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"popUp"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"%@",uid);
    
    //base64String=[base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
     NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]);
    
    [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@&section=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"],@"email"];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@login.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];

}
#pragma mark - Login_Email_ButtonPressed
-(void)login_Email_ButtonPressed
{
   login=[[LoginViewController alloc]init];
 
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:login animated:YES];
}
//#pragma mark - FastForwdBtnPressed Method
//-(void)fastForwdBtnPressed
//{
//    
//    [[NSUserDefaults standardUserDefaults]setValue:@"success" forKey:@"getstart"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    
//    [self myLoginView];
//}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
	NSString *alertMessage, *alertTitle;
	
	if ([FBErrorUtility shouldNotifyUserForError:error])
	{
		alertTitle = @"Facebook error";
		alertMessage = [FBErrorUtility userMessageForError:error];
		
	}
	else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
	{
		alertTitle = @"Session Error";
		alertMessage = @"Your current session is no longer valid. Please log in again.";
		
	}
	else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
		NSLog(@"user cancelled login");
		
	}
	else
	{
		alertTitle  = @"Something went wrong";
		alertMessage = @"Please try again later.";
		NSLog(@"Unexpected error:%@", error);
	}
	
	if (alertMessage)
	{
		[[[UIAlertView alloc] initWithTitle:alertTitle
									message:alertMessage
								   delegate:nil
						  cancelButtonTitle:@"OK"
						  otherButtonTitles:nil] show];
	}
}
-(void)postcustomerID
{
    
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"popUp"];
    [[NSUserDefaults standardUserDefaults]synchronize];

  
    
    [self showLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
}
 
//-(void)postDataForRelogin
//{
//    NSLog(@"email =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
//    NSLog(@"first name is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
//    NSLog(@"password is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]);
//    NSLog(@"lastname is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
//  
//    [self showLoadingView];
//    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&profileimg=%@@platform=%@&device_id=%@&cell_number=%@&section=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"],[[NSUserDefaults standardUserDefaults]valueForKey:@"last"],[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"],[[NSUserDefaults standardUserDefaults]valueForKey:@"base64"],@"ios",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],@"facebook" ];
//    
//    NSLog(@"posting value is = %@",post);
//    
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
// 
//    NSString *urlString=[NSString stringWithFormat:@"%@registeration.php?",post_url];
//    
//    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
//    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//    
//    [request setTimeoutInterval:180];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setHTTPBody:postData];
//  
//    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
//    
//}

#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=  \n%@",dict);
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    
    [self hideLoadingView];
   
        NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
         NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"status"]];
        
        NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
      
        NSString* image_profile=[NSString stringWithFormat:@"%@",[dict valueForKey:@"profileimg_url"]];
        NSLog(@"%@",image_profile);
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"daysleft"] forKey:@"daysleft"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"package_name"] forKey:@"pack"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"planid"] forKey:@"planid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
     [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"city"] forKey:@"city"];
      [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"cityid"] forKey:@"CITYID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]length]>0)
    {
        
        
        if  ([sucess isEqualToString:@"0"] && [status isEqualToString:@"not purchased"])
        {
            SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
            [self.navigationController pushViewController:selectPlan animated:YES];
        }else if ([sucess isEqualToString:@"0"] && [status isEqualToString:@"customer not found"])
        {
            login=[[LoginViewController alloc]init];
            
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            [self.navigationController pushViewController:login animated:YES];

        }
        else if([daysleft isEqualToString:@"0"])
        {
            if ([[dict valueForKey:@"status"] isEqualToString:@"paused"])
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your membership plan has expired" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your Bite membership has expired please purchase a new membership plan." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
                [self.navigationController pushViewController:selectPlan animated:YES];
                
            }
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self tabBar];
            [self hideLoadingView];
        }
        
        

    }else
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"You are already registered with this device." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        login=[[LoginViewController alloc]init];
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        [self.navigationController pushViewController:login animated:YES];

    }
    
    
//    if (tag==0)
//    {
//        NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
//        if ([status isEqualToString:@"1"])
//        {
//             tag=1;
//            
//            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"sessionid"] forKey:@"sessionId"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            
//            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"customerid"] forKey:@"customerid"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            
//            NSLog(@"customerid = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
//            
//            [self postcustomerID];
//        }
//    else
//        {
//            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"message"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            [self hideLoadingView];
//            
//        }
//     }
    
    }

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


#pragma mark - UITabBarController Method
-(void)tabBar
{
   
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"my.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Restaurants";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"my.png"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"nearme.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"nearme.png"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"search.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"search.png"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"settings.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"settings.png"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
    
    
    
        
    //    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
    //    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    [self.navigationController pushViewController:self.tabBarController animated:YES];
    
    
}
#pragma mark - CustomAlert Method
-(void)customAlert
{
    customView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height)];
    customView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:customView];
    
    UIView * alertview=[[UIView alloc]initWithFrame:CGRectMake(15,  self.view.frame.size.height/2-100, self.view.frame.size.width-30,150)];
    alertview.layer.cornerRadius=5.0;
    alertview.backgroundColor=[UIColor colorWithRed:0.8980 green:0.8980 blue:0.8980 alpha:5.0];
    [customView addSubview:alertview];
    
    UILabel* lbl_free=[[UILabel alloc]initWithFrame:CGRectMake(0, 2,alertview.frame.size.width, 35)];
    lbl_free.text=@"Please enter your cell phone number below to receive your verification code";
    lbl_free.textColor=[UIColor blackColor];
    lbl_free.adjustsFontSizeToFitWidth=YES;
    lbl_free.numberOfLines=2;
    lbl_free.textAlignment=NSTextAlignmentCenter;
    lbl_free.font=[UIFont boldSystemFontOfSize:15];
    lbl_free.textAlignment=NSTextAlignmentCenter;
    [alertview addSubview:lbl_free];
    
    self.txt_Mobile_Number=[[UITextField alloc]initWithFrame:CGRectMake(5,  lbl_free.frame.origin.y + lbl_free.frame.size.height+20 , alertview.frame.size.width-10, 35)];
    UIView *viewPadding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txt_Mobile_Number.leftView = viewPadding6;
    self.txt_Mobile_Number.leftViewMode = UITextFieldViewModeAlways;
    self.txt_Mobile_Number.layer.cornerRadius=2;
    self.txt_Mobile_Number.tag=1;
    self.txt_Mobile_Number.returnKeyType=UIReturnKeyDone;
    self.txt_Mobile_Number.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    self.txt_Mobile_Number.borderStyle=UITextBorderStyleNone;
    self.txt_Mobile_Number.backgroundColor=[UIColor whiteColor];
    self.txt_Mobile_Number.layer.borderWidth=.8;
    self.txt_Mobile_Number.layer.borderColor=[UIColor blackColor].CGColor;
    self.txt_Mobile_Number.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"612 123 1234"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    self.txt_Mobile_Number.delegate=self;
    [alertview addSubview:self.txt_Mobile_Number];
   
    UIView * lineView=[[UIView alloc]initWithFrame:CGRectMake( 0,  self.txt_Mobile_Number.frame.origin.y + self.txt_Mobile_Number.frame.size.height+20, alertview.frame.size.width, .7)];
    lineView.backgroundColor=[UIColor lightGrayColor];
    [alertview addSubview:lineView];

    UIButton*  okBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [okBtn setFrame:CGRectMake(0,  self.txt_Mobile_Number.frame.origin.y + self.txt_Mobile_Number.frame.size.height+28, alertview.frame.size.width, 34)];
    [okBtn addTarget:self action:@selector(okBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [okBtn setTitle:@"OK" forState:UIControlStateNormal];
    okBtn.titleLabel.font=[UIFont boldSystemFontOfSize:19];
    okBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    okBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [okBtn setTitleColor:[UIColor colorWithRed:0.0000 green:0.5373 blue:0.9765 alpha:1.0] forState:UIControlStateNormal];
    [alertview addSubview:okBtn];
}
-(void)okBtnPressed
{

    if (self.txt_Mobile_Number.text.length!=10)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid cell number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else
    {
            [self showLoadingView];
        [customView removeFromSuperview];
        [[NSUserDefaults standardUserDefaults]setValue:self.txt_Mobile_Number.text forKey:@"cell_number"];
        [[NSUserDefaults standardUserDefaults]synchronize];
  
        [self generateRandomCode];
    }
}

#pragma mark - Generate_Random_Code sending Method
-(void)generateRandomCode
{
    int min = 10000;
    int max = 99999;
    
    int randNum = rand() % (max - min) + min;
   
    NSString* randomCode=[NSString stringWithFormat:@"Bite Confirmation Code \n %d",randNum];
       NSString * addString=[@"+1" stringByAppendingString:self.txt_Mobile_Number.text];
     addString = [addString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [self sendCode:addString randomCode:randomCode];
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_Mobile_Number.text forKey:@"cell_number"];
    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInt:randNum] forKey:@"codeIs"];
    [[NSUserDefaults standardUserDefaults]synchronize];
 }

-(void)sendCode:(NSString *)toContact randomCode:(NSString *)randomCode

{
    
  
    NSMutableDictionary *dict_Value = [[NSMutableDictionary alloc]init];
    [dict_Value setObject:@"+17786553948" forKey:@"From"];
    [dict_Value setObject:toContact forKey:@"To"];
    [dict_Value setObject:randomCode forKey:@"Body"];
    
    NSLog(@"DICT=%@",dict_Value);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"ACf656d70460e43f77cbbaa9bfaf535875" password:@"218b1d1ff3fd6125ff5d113aca779dbc"];
    [manager POST:sms_url2 parameters:dict_Value success:
     ^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"%@",responseObject);
         
         id status=[responseObject valueForKey:@"status"];
         NSLog(@"status=%@",status);
         if ([status isEqualToString:@"queued"])
         {
            
             [self hideLoadingView];
            TwillioIntegrationViewController * twilioVC=[[TwillioIntegrationViewController alloc]init];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            [self.navigationController pushViewController:twilioVC animated:YES];

//                     if (flag==0)
//                     {
//                         [self postData];
//                         flag=1;
//                     }
           }else
           {
               UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid cell number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
               [self hideLoadingView];
           }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [self hideLoadingView];
         NSLog(@"error=%@",error);
         NSLog(@"response str = %@  response data is = %@",operation.responseString,operation.responseData);
         UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:@"The Internet connection appears to be offline." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         
     }];
    
    
}

#pragma mark - UITextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txt_Mobile_Number resignFirstResponder];
    
}

 - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 
@end
