//
//  NearMeViewController.m
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "NearMeViewController.h"
#import "MBProgressHUD.h"
#import "DetailOfRestaurantViewController.h"
#import "MapAnnonation.h"
#import "AppDelegate.h"
#import "DataClass.h"

@interface NearMeViewController ()
{
    
    id count;
    
    
}

@end
static int apiCall=0;
static int curnt_Loc=0;
@implementation NearMeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    
    
}
-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
    self.tag = 0;
    
    count=0;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];

      self.navigationItem.title=@"Near Me";
    
    allDataArr=[[NSMutableArray alloc]init];
    
    // UIBARBUTTON HERE
    UIBarButtonItem* refreshButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshLocation)];
    [refreshButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem=refreshButton;

    //SEARCHBAR HERE
    self.search=[[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    self.search.searchBarStyle=UISearchBarStyleMinimal;
    //self.search.placeholder=@"search";
    //self.search.tintColor=[UIColor whiteColor];
    self.search.backgroundColor=[UIColor  whiteColor];

    self.search.delegate=self;
    
    //     [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    //
    //      [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    for (UIView *subView in self.search.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UIColor * color=[UIColor lightGrayColor];
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //set font color here
                searchBarTextField.textColor = [UIColor blackColor];
                searchBarTextField.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@"Search by Zip Code" attributes:@{NSForegroundColorAttributeName: color}];
                
                break;
            }
        }
    }
    [self.view addSubview:self.search];
    
    
    UILabel* newLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 35, 320, 20)];
    newLabel.text=@" New Restaurant";
    newLabel.textAlignment=NSTextAlignmentLeft;
    newLabel.font=[UIFont boldSystemFontOfSize:14];
    newLabel.textColor=[UIColor whiteColor];
    newLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.4392 blue:0.3922 alpha:1.0];
    [self.view addSubview:newLabel];
    
    self.mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0,55, 320, 400)];
    self.mapView.delegate=self;
    self.mapView.zoomEnabled = YES;
    self.mapView.showsUserLocation = YES;
    [self.view addSubview:self.mapView];
    self.mapView.mapType=MKMapTypeStandard;
    
    self.locationManager=[[CLLocationManager alloc]init];
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManager.delegate=self;

    
    int value=[[[UIDevice currentDevice] systemVersion]intValue];
    NSLog(@"%d",value);
    if (value>=8)
    {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
        
    }

    [self.locationManager startUpdatingLocation];
     
 


}
-(void)refreshLocation
{
    NSLog(@"button pressed");
    [self.locationManager startUpdatingLocation];
    curnt_Loc=0;
    apiCall=0;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self hideLoadingView];
    [self.locationManager stopUpdatingLocation];
    
    float currentlatitute=[[NSString stringWithFormat:@"%f",self.locationManager.location.coordinate.latitude]floatValue];
    float currentlongitide=[[NSString stringWithFormat:@"%f",self.locationManager.location.coordinate.longitude]floatValue];
    
    
    NSLog(@"%f",currentlatitute); 
    NSLog(@"%f",currentlongitide);

    
    
    CLLocation * currentLocation=[[CLLocation alloc]initWithLatitude:currentlatitute longitude:currentlongitide];
    NSLog(@"%@",currentLocation);
    
    if (curnt_Loc==0)
    {
        [self postZipcode:currentlatitute longitude:currentlongitide];
        
    }
    MKCoordinateSpan span;
    //You can set span for how much Zoom to be display like below
    span.latitudeDelta=.01;
    span.longitudeDelta=.01 ;
    
    //set Region to be display on MKMapView
    MKCoordinateRegion cordinateRegion;
    cordinateRegion.center=currentLocation.coordinate;
    cordinateRegion.span=span;
    //set That Region mapView
    [self.mapView setRegion:cordinateRegion animated:YES];
    
    
    
   locationsArr=[[NSMutableArray alloc]init];
    NSLog(@"count = %@",count);
   
    int v= [count intValue] ;
   NSLog(@"%d",v);
    
    
    if (v==0)
    {
        
    }
    else
    {
                CLLocationCoordinate2D loc[v];
        
        DataClass* locData;
        for (int i=0;i<v; i++)
        {
            
            locData=[arrayOfproductArr objectAtIndex:i];
            
            float lat=[locData.latitude_Str floatValue];
            float lon=[locData.longitude_Str floatValue];
            
            loc[i]=CLLocationCoordinate2DMake(lat, lon);
        }
        
        
        //    loc[0]=CLLocationCoordinate2DMake(49.249660,-123.1193400);
        //    loc[1]=CLLocationCoordinate2DMake(45.24,-75.41);
        //    loc[2]=CLLocationCoordinate2DMake(43.67023,-79.38676);
        
     
        if (apiCall==1)
        {
               [self.mapView setCenterCoordinate:loc[0] animated:YES];
        }
    
        
        
        
        for (int i=0; i<v; i++)
        {
            locData=[arrayOfproductArr objectAtIndex:i];
    
            MapAnnonation *notaion=[[MapAnnonation alloc]init];
            
            notaion.coordinate=loc[i];
            
            notaion.title=locData.hotelName;
            notaion.subtitle=locData.hotelAdress;
            notaion.id_str=locData.hotel_id;
            notaion.adress=locData.hotelAdress;
            notaion.imageurl=locData.hotelPhoto;
            notaion.cuisine=locData.hotelCuisine;
            
            [self.mapView addAnnotation:notaion];

           //[locationsArr addObject:notaion];
            NSLog(@"%@",locationsArr);

        }
     }
  }

 #pragma mark -MKMapView Delegates

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    
    MapAnnonation *annotation =(MapAnnonation *)[view annotation];
    annotation.id_str= [annotation id_str];
    annotation.adress= [annotation adress];
    annotation.title= [annotation title];
    annotation.imageurl= [annotation imageurl];
    annotation.cuisine= [annotation cuisine];

    NSLog(@"%@",annotation.adress);
    
    DetailOfRestaurantViewController * detail_cls=[[DetailOfRestaurantViewController alloc]init];
    detail_cls.product_id=annotation.id_str;
    detail_cls.adressOfRestaurant_String=annotation.adress;
     detail_cls.imageOfHotel_string=annotation.imageurl;
    detail_cls.nameOfpRestaureant_String=annotation.title;
    detail_cls.cuisine_String=annotation.cuisine;

    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [self.navigationController pushViewController:detail_cls animated:YES];
 
    
    

}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
   //         DataClass * obj;
//    obj=[arrayOfproductArr objectAtIndex:mapView.tag];
//    NSString * strID=obj.hotelAdress;
//    
//    NSLog(@"%@",strID);
//    DetailOfRestaurantViewController *detailOfres=[[DetailOfRestaurantViewController alloc]init];
//       detailOfres.product_id=obj.hotel_id;
//    detailOfres.adressOfRestaurant_String=obj.hotelAdress;
//     detailOfres.imageOfHotel_string=obj.hotelPhoto;
//    
//    
//   [self.navigationController pushViewController:detailOfres animated:YES];
  
}



-(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    
    
    
    MKAnnotationView *pinView = nil;
    if(annotation != self.mapView.userLocation)
    {
    static NSString *defaultPinID = @"test";
    
    
    
 pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:nil];
    
    
        
    
    pinView.image = [UIImage imageNamed:@"map.png"];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    imgView.image =[UIImage imageNamed:@"ic_launcher.png"];
    
    infoButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
  
    pinView.canShowCallout=YES;
    pinView.rightCalloutAccessoryView = infoButton;
    pinView.leftCalloutAccessoryView =imgView;
    }
    else
    {
        [self.mapView.userLocation setTitle:@"I am here"];

    }
    return pinView;
    
   
}
  #pragma mark - DataFetch Method

-(void)postZipcode
{
    apiCall=1;

    [self showLoadingView];
    
    NSLog(@"%@",searchString);
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"datasearch=%@",searchString];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@nearby.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    
    NSLog(@"%@",postLength);
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencode" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSLog(@"request=%@",request);
    //NSOperationQueue* qu=[[NSOperationQueue alloc]init];
    
    connectionIS= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    
}

-(void)postZipcode:(float )latitude  longitude:(float )longitude
{
 
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    NSString *post = [NSString stringWithFormat:@"lat=%f&lang=%f",latitude,longitude];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@nearme.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    connectionIS= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    dataIS=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [dataIS appendData:theData];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDict=[NSJSONSerialization JSONObjectWithData:dataIS options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"detail data of res is  = \n %@",dataDict);
    
    
    
    NSString * status= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
    
    
    if (curnt_Loc==0)
    {
        
        if ([status isEqualToString:@"0"])
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"No Restaurants Found" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }else
        {
            arrayOfproductArr=[[NSMutableArray alloc]init];
            allDataArr=[dataDict valueForKey:@"products"];
            count=[dataDict valueForKey:@"count"];
            
            for (int i=0; i<allDataArr.count; i++)
            {
                restaurantDetail=[allDataArr objectAtIndex:i];
                detail=[restaurantDetail valueForKey:@"detail"];
                
                DataClass* dataClass=[DataClass new];
                dataClass.hotelName= [detail valueForKey:@"name"];
                dataClass.hotelPhoto= [detail valueForKey:@"image_url"];
                dataClass.hotel_id= [detail valueForKey:@"entity_id"];
                dataClass.hotelAdress=[detail valueForKey:@"address"];
                dataClass.latitude_Str=[detail valueForKey:@"latitude"];
                dataClass.longitude_Str=[detail valueForKey:@"longitude"];
                 dataClass.hotelCuisine=[detail valueForKey:@"cuisine"];
                
                NSLog(@"latitude_Str = %@",dataClass.latitude_Str);
                NSLog(@"longitude_Str = %@",dataClass.longitude_Str);
                
                
                dataClass.hotelrating=[restaurantDetail valueForKey:@"rating"];
                [arrayOfproductArr addObject:dataClass];
                dataIS=nil;
                
            }
            

            [self.locationManager startUpdatingLocation];

        }
    
        
        NSLog(@"%@",arrayOfproductArr);
        curnt_Loc=1;
    }
    
    if (apiCall==1)
    {
        
    
    if ([status isEqualToString:@"0"])
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"No Restaurant Found" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
    
    
        arrayOfproductArr=[[NSMutableArray alloc]init];
        allDataArr=[dataDict valueForKey:@"products"];
        count=[dataDict valueForKey:@"count"];
        
    for (int i=0; i<allDataArr.count; i++)
    {
         restaurantDetail=[allDataArr objectAtIndex:i];
         detail=[restaurantDetail valueForKey:@"detail"];
        
        DataClass* dataClass=[DataClass new];
        dataClass.hotelName= [detail valueForKey:@"name"];
        dataClass.hotelPhoto= [detail valueForKey:@"image_url"];
        dataClass.hotelAdress=[detail valueForKey:@"address"];
        dataClass.hotelCuisine=[detail valueForKey:@"cuisine"];

        dataClass.hotel_id= [detail valueForKey:@"entity_id"];
        dataClass.latitude_Str=[detail valueForKey:@"latitude"];
        dataClass.longitude_Str=[detail valueForKey:@"longitude"];
        
        NSLog(@"latitude_Str = %@",dataClass.latitude_Str);
        NSLog(@"longitude_Str = %@",dataClass.longitude_Str);
        
        
       
        dataClass.hotelrating=[restaurantDetail valueForKey:@"rating"];
        [arrayOfproductArr addObject:dataClass];
        dataIS=nil;
 
    }
    
    NSLog(@"arrayOfproductArr = %@",arrayOfproductArr);
        
       
    [self.locationManager startUpdatingLocation];
     }
    }
    
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideLoadingView];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}




#pragma mark - UISearchBar Delegates


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if ([searchText length] == 0)
    {
        
        [searchBar performSelector:@selector(resignFirstResponder)withObject:nil afterDelay:0];
        
        
        
    }
    
    if (![searchText isEqualToString:@""]) // here you check that the search is not null, if you want add another check to avoid searches when the characters are less than 3
    {
        
        
        searchString=searchText;
        
        NSLog(@"%@",searchString);
        
    }
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
     [self postZipcode];
    [searchBar resignFirstResponder];
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
