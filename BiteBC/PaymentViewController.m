//
//  PaymentViewController.m
//  BiteBC
//
//  Created by brst on 7/3/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "PaymentViewController.h"

#import "AppDelegate.h"
#import "Stripe.h"
#import "STPCard.h"
#import "PKPayment+STPTestKeys.h"
#import "Stripe+ApplePay.h"
#import "StripeError.h"
#import "HelperViewController.h"
#import "RestaurantTableViewController.h"
#import "NearMeViewController.h"
#import "DetailPageTableViewController.h"
#import "SearchViewController.h"
#import "SettingTableViewController.h"
#import "MBProgressHUD.h"
static int buttonV=0;
static int flag=0;

@interface PaymentViewController ()<PKPaymentAuthorizationViewControllerDelegate>

@end
static int list_tag=0;
@implementation PaymentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Checkout";
        
}

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}
#pragma mark -  viewDidLoad Method
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    
   // cardNameArray=[[NSMutableArray alloc]initWithObjects:@"Card Number",@"Card Type",@"Expiry Date",@"Start Date",@"CVC Number",@"Security Card", nil];
   
    cardNameArray=[[NSMutableArray alloc]initWithObjects:@"Card Number",@"Card Type",@"Expiry Date",@"CVC Number", nil];
    smartCardsArr=[[NSMutableArray alloc]initWithObjects:@"MasterCard",@"VISA",@"American Express",@"RuPay",nil];
    
     monthArr=[[NSMutableArray alloc]initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",nil];
    
    showMonthArr=[[NSMutableArray alloc]initWithObjects:@"Jan",@"Feb",@"Mar",@"April",@"May",@"June",@"July",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil];

    yearArr=[[NSMutableArray alloc]initWithObjects:@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2024",nil];
    // Do any additional setup after loading the view.
    [self cardDetailView];
}
-(void)cardDetailView
{
    if (!IS_IPHONE_5)
    {
        cardDetailView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,420)];
        cardDetailView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        [self.view addSubview:cardDetailView];

        
    }
    else
    {
        cardDetailView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,505)];
        cardDetailView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        [self.view addSubview:cardDetailView];

    }
    
    int h=25;
    
    for (int i=0;i<cardNameArray.count; i++)
    {
        
        UILabel* label=[[UILabel alloc]initWithFrame:CGRectMake(10,h,100, 15)];
        label.textAlignment=NSTextAlignmentLeft;
        label.font=[UIFont systemFontOfSize:14];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.text=[cardNameArray objectAtIndex:i];
        h+=50;
        [cardDetailView addSubview:label];
        
         switch (i)
        {
            case 0:
            {
                cardNo_Textfield=[[UITextField alloc]initWithFrame:CGRectMake(120,19,190,30)];
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 28)];
                cardNo_Textfield.leftView = paddingView;
                cardNo_Textfield.leftViewMode = UITextFieldViewModeAlways;
                //cardNo_Textfield.layer.cornerRadius=3;
                //cardNo_Textfield.layer.borderWidth=1.0;
                //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
                cardNo_Textfield.delegate=self;
                cardNo_Textfield.layer.borderWidth=1.0;
                cardNo_Textfield.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                
                cardNo_Textfield.keyboardType=UIKeyboardTypeNumberPad;
                cardNo_Textfield.backgroundColor=[UIColor whiteColor];
                cardNo_Textfield.textAlignment=NSTextAlignmentNatural;
               [cardNo_Textfield addTarget:self action:@selector(checkTextField:) forControlEvents:UIControlEventEditingChanged];
                cardNo_Textfield.returnKeyType=UIReturnKeyDone;
                cardNo_Textfield.placeholder=@"Enter your Card No.";
                cardNo_Textfield.font=[UIFont systemFontOfSize:13];
                
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
                view.backgroundColor = [UIColor colorWithRed:(193/255.0f) green:(196/255.0f) blue:(200/255.0f) alpha:1.0f];
                //value.keyboardType = UIKeyboardTypeNumberPad;
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button setTitle:@"Done" forState:UIControlStateNormal];
                [button setTintColor:[UIColor blackColor]];
                [button addTarget:self action:@selector(dissmissKeyboad) forControlEvents:UIControlEventTouchUpInside];
                button.frame = CGRectMake(240, 0, 80, 40);
                button.tag=i;
                [view addSubview:button];
                cardNo_Textfield.inputAccessoryView = view;
                
                [cardDetailView addSubview:cardNo_Textfield];
                
            }
                break;
                
            case 1:
            {
                
                cardType_Btn=[UIButton buttonWithType:UIButtonTypeSystem];
                cardType_Btn.frame=CGRectMake(120,68,190,30);
                [cardType_Btn addTarget:self action:@selector(cardTypeMethod) forControlEvents:UIControlEventTouchUpInside];
                [cardType_Btn setTitle:@"Enter Card Type"forState:UIControlStateNormal];
                [cardType_Btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                cardType_Btn.titleLabel.font=[UIFont systemFontOfSize:12];
                cardType_Btn.backgroundColor=[UIColor whiteColor];
                cardType_Btn.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                cardType_Btn.layer.borderWidth=1.0;
                [cardDetailView addSubview:cardType_Btn];
                
       
                
            }
           
            break;
                
            case 2:
            {
                
                int xPos=120;
               // int xForBtn=185;
                
                
                for (int textfield=0; textfield<2; textfield++)
                {
                                        if (textfield==0)
                    {
                        
                        
                        expYear_Btn=[UIButton buttonWithType:UIButtonTypeSystem];
                        expYear_Btn.frame=CGRectMake(xPos,116,87,30);
                        [expYear_Btn addTarget:self action:@selector(expYear_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
                        expYear_Btn.backgroundColor=[UIColor whiteColor];
                        expYear_Btn.tag=0;
                        [expYear_Btn setTitle:@"Year"forState:UIControlStateNormal];
                        [expYear_Btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                        expYear_Btn.titleLabel.font=[UIFont systemFontOfSize:12];

                        expYear_Btn.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                        expYear_Btn.layer.borderWidth=1.0;
                        [cardDetailView addSubview:expYear_Btn];

                        
//                        expYearTextField=[[UITextField alloc]initWithFrame:CGRectMake(xPos,116,65,30)];
//                        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 28)];
//                        expYearTextField.leftView = paddingView;
//                        expYearTextField.leftViewMode = UITextFieldViewModeAlways;
//                        //cardNo_Textfield.layer.cornerRadius=3;
//                        //cardNo_Textfield.layer.borderWidth=1.0;
//                        //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
//                        expYearTextField.delegate=self;
//                        expYearTextField.tag=textfield;
//                        expYearTextField.enabled=YES;
//                        expYearTextField.borderStyle=UITextBorderStyleBezel;
//                        expYearTextField.backgroundColor=[UIColor whiteColor];
//                        expYearTextField.textAlignment=NSTextAlignmentNatural;
//                        expYearTextField.returnKeyType=UIReturnKeyDone;
//                        expYearTextField.layer.borderWidth=1.0;
//                        expYearTextField.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
//                        expYearTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
//
//                        expYearTextField.placeholder=@"Year";
//                        expYearTextField.font=[UIFont systemFontOfSize:15];
//                        [cardDetailView addSubview:expYearTextField];
                        
                        
                    }
                    else
                    {
                        
                        
                        expMonth_Btn=[UIButton buttonWithType:UIButtonTypeSystem];
                        expMonth_Btn.frame=CGRectMake(xPos,116,87,30);
                        expMonth_Btn.tag=1;
                        
                        [expMonth_Btn addTarget:self action:@selector(expMonth_BtnMethod:) forControlEvents:UIControlEventTouchUpInside];
                        expMonth_Btn.backgroundColor=[UIColor whiteColor];
                        [expMonth_Btn setTitle:@"Month"forState:UIControlStateNormal];
                        [expMonth_Btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                        expMonth_Btn.titleLabel.font=[UIFont systemFontOfSize:12];
                        
                        expMonth_Btn.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                        expMonth_Btn.layer.borderWidth=1.0;
                        [cardDetailView addSubview:expMonth_Btn];

                        
//                        expMonthTextField=[[UITextField alloc]initWithFrame:CGRectMake(xPos,116,65,30)];
//                        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 28)];
//                        expMonthTextField.leftView = paddingView;
//                        expMonthTextField.leftViewMode = UITextFieldViewModeAlways;
//                        //cardNo_Textfield.layer.cornerRadius=3;
//                        //cardNo_Textfield.layer.borderWidth=1.0;
//                        //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
//                        expMonthTextField.delegate=self;
//                        expMonthTextField.tag=textfield;
//                        expMonthTextField.enabled=NO;
//                        expMonthTextField.borderStyle=UITextBorderStyleBezel;
//                        expMonthTextField.backgroundColor=[UIColor whiteColor];
//                        expMonthTextField.textAlignment=NSTextAlignmentNatural;
//                        expMonthTextField.returnKeyType=UIReturnKeyDone;
//                        expMonthTextField.layer.borderWidth=1.0;
//                        expMonthTextField.placeholder=@"Month";
//                        expMonthTextField.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
//                        expMonthTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
//                        expMonthTextField.font=[UIFont systemFontOfSize:15];
//                        [cardDetailView addSubview:expMonthTextField];
//

                    }
                  
                    xPos+=105;
                    
//                    expire_listButton=[UIButton buttonWithType: UIButtonTypeCustom];
//                    UIImage* img2=[UIImage imageNamed:@"small.png"];
//                    [expire_listButton setImage:img2 forState:UIControlStateNormal];
//                    expire_listButton.backgroundColor=[UIColor whiteColor];
//                    expire_listButton.layer.borderWidth=1.0;
//                    expire_listButton.tag=textfield;
//                    expire_listButton.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
//                    expire_listButton.frame=CGRectMake(xForBtn,116.5,22,29);
//                    [expire_listButton addTarget:self action:@selector(expire_listButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//                    xForBtn+=105;
//                    [cardDetailView addSubview:expire_listButton];

                }
            }
                
                break;
            case 4:
            {
                
                int xPos=120;
                int xForBtn=185;
                
                
                for (int textfield=0; textfield<2; textfield++)
                {
                    start_Textfield=[[UITextField alloc]initWithFrame:CGRectMake(xPos,167,65,30)];
                    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
                    start_Textfield.leftView = paddingView;
                    start_Textfield.leftViewMode = UITextFieldViewModeAlways;
                    //cardNo_Textfield.layer.cornerRadius=3;
                    //cardNo_Textfield.layer.borderWidth=1.0;
                    //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
                    start_Textfield.delegate=self;
                    start_Textfield.tag=textfield;
                    start_Textfield.borderStyle=UITextBorderStyleBezel;
                    start_Textfield.backgroundColor=[UIColor whiteColor];
                    start_Textfield.textAlignment=NSTextAlignmentNatural;
                    start_Textfield.returnKeyType=UIReturnKeyDone;
                    start_Textfield.layer.borderWidth=1.0;
                    start_Textfield.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                    start_Textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    if (textfield==0)
                    {
                        start_Textfield.placeholder=@"Year";
                    }
                    else
                    {
                        start_Textfield.placeholder=@"Month";
                    }
                    
                    xPos+=105;
                    start_Textfield.font=[UIFont systemFontOfSize:12];
                    [cardDetailView addSubview:start_Textfield];
                    
                    start_listButton=[UIButton buttonWithType: UIButtonTypeCustom];
                    UIImage* img2=[UIImage imageNamed:@"small.png"];
                    [start_listButton setImage:img2 forState:UIControlStateNormal];
                    start_listButton.backgroundColor=[UIColor whiteColor];
                    start_listButton.layer.borderWidth=1.0;
                    start_listButton.tag=textfield;
                    start_listButton.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                    start_listButton.frame=CGRectMake(xForBtn,167.5,22,29);
                    [start_listButton addTarget:self action:@selector(start_listButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    xForBtn+=105;
                    [cardDetailView addSubview:start_listButton];
                    
                }
            }
                break;
            case 3:
            
            {
                
                issueTextfield=[[UITextField alloc]initWithFrame:CGRectMake(120,167,190,30)];
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
                issueTextfield.leftView = paddingView;
                issueTextfield.leftViewMode = UITextFieldViewModeAlways;
                //cardNo_Textfield.layer.cornerRadius=3;
                //cardNo_Textfield.layer.borderWidth=1.0;
                //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
                issueTextfield.delegate=self;
                issueTextfield.backgroundColor=[UIColor whiteColor];
                issueTextfield.textAlignment=NSTextAlignmentNatural;
                issueTextfield.returnKeyType=UIReturnKeyDone;
                issueTextfield.keyboardType=UIKeyboardTypeNumberPad;
                issueTextfield.layer.borderWidth=1.0;
                issueTextfield.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                issueTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                [issueTextfield addTarget:self action:@selector(issueTextfield:) forControlEvents:UIControlEventEditingChanged];
                issueTextfield.placeholder=@"Your cvc No.";
                issueTextfield.font=[UIFont systemFontOfSize:13];
                [cardDetailView addSubview:issueTextfield];
               
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
                view.backgroundColor = [UIColor colorWithRed:(193/255.0f) green:(196/255.0f) blue:(200/255.0f) alpha:1.0f];
                //value.keyboardType = UIKeyboardTypeNumberPad;
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button setTitle:@"Done" forState:UIControlStateNormal];
                [button setTintColor:[UIColor blackColor]];
                [button addTarget:self action:@selector( dissmissKeyboad) forControlEvents:UIControlEventTouchUpInside];
                button.frame = CGRectMake(240, 0, 80, 40);
                button.tag=i;
                [view addSubview:button];
                issueTextfield.inputAccessoryView = view;

            }
                
                break;
            case 5:
                
            {
                
                securityTextfield=[[UITextField alloc]initWithFrame:CGRectMake(120,270,190,30)];
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
                securityTextfield.leftView = paddingView;
                securityTextfield.leftViewMode = UITextFieldViewModeAlways;
                //cardNo_Textfield.layer.cornerRadius=3;
                //cardNo_Textfield.layer.borderWidth=1.0;
                //cardNo_Textfield.layer.borderColor=[UIColor blackColor].CGColor;
                securityTextfield.delegate=self;
                securityTextfield.borderStyle=UITextBorderStyleBezel;
                securityTextfield.backgroundColor=[UIColor whiteColor];
                securityTextfield.textAlignment=NSTextAlignmentNatural;
                securityTextfield.returnKeyType=UIReturnKeyDone;
                securityTextfield.layer.borderWidth=1.0;
                securityTextfield.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                securityTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                securityTextfield.placeholder=@"Your security Code";
                securityTextfield.font=[UIFont systemFontOfSize:15];
                [cardDetailView addSubview:securityTextfield];
                
            }
                
                break;
                
            default:
                break;
        }
        
     }
    
    
    
    
    
  #pragma mark - Submit Button
    
    if (!IS_IPHONE_5)
    {
        submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
        [submit_Button setFrame:CGRectMake(2,290,316,30)];
        submit_Button.layer.borderWidth=1.0;
        submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
        [submit_Button addTarget:self action:@selector(submit_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [submit_Button setTitle:@"SUBMIT" forState:UIControlStateNormal];
        submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:17];
        [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        [self.view addSubview:submit_Button];
        

    }
    else
    {
        submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
        [submit_Button setFrame:CGRectMake(2,300,316,30)];
        submit_Button.layer.borderWidth=1.0;
        submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
        [submit_Button addTarget:self action:@selector(submit_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [submit_Button setTitle:@"SUBMIT" forState:UIControlStateNormal];
        submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:17];
        [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        
        [self.view addSubview:submit_Button];
        

    }
    
    
}
-(void)dissmissKeyboad
{
    [issueTextfield resignFirstResponder];
    [cardNo_Textfield resignFirstResponder];
}

-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)expYearTextField.inputView;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateStyle:NSDateFormatterMediumStyle];
    //[df setTimeStyle:NSDateFormatterMediumStyle];
   expYearTextField.text = [df stringFromDate:picker.date];
}
-(void)cardTypeMethod
{
    [self listButtonPressed];
}
-(void)expYear_BtnMethod:(UIButton * )sender
{
    [self expire_listButtonPressed:sender];
}
-(void)expMonth_BtnMethod:(UIButton * )sender
{
    [self expire_listButtonPressed:sender];
}
-(void)checkTextField:(UITextField*)sender{
    UITextField* textField = (UITextField*)sender;
    if ([textField.text length] == 16) {
        [textField resignFirstResponder];// No cargo-culting please, this color is very ugly...
    } else {
        textField.textColor = [UIColor blackColor];
        /* Must be done in case the user deletes a key after adding 8 digits,
         or adds a ninth digit */
    }
}
-(void)issueTextfield:(UITextField*)sender{
    
    UITextField* textField = (UITextField*)sender;
    if ([textField.text length] == 3) {
        [textField resignFirstResponder];// No cargo-culting please, this color is very ugly...
    } else {
        textField.textColor = [UIColor blackColor];
        
    }

}

#pragma mark - UITabBarController Method
-(void)tabBar
{
    self.navigationController.navigationBarHidden=YES;
//    //self.tabBarController=[[UITabBarController alloc]init];
//    self.tabBarController.delegate=self;
//    [self.tabBarController.tabBar setTranslucent:NO];
//    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]];
//    self.tabBarController.tabBar.tintColor=[UIColor blackColor];
//
    
    
//    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
//    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"my.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    restaurantTable.tabBarItem.title=@"Activity";
//    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
//    
//        
//    //restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"activity.png"];
//    restaurantTable.tabBarItem.tag=1;
//    
//   [self.navigationController pushViewController:restaurantTable animated:YES];
//    
//   // UINavigationController* navigateTOrestaurant=[[UINavigationController alloc]init];
//
//  
//    
//    
//    NSArray* array=[NSArray arrayWithObjects:navigateTOrestaurant, nil];
//    self.tabBarController.viewControllers = array;
//
    
    // create the tab controller and add the view controllers
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    
    

   NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"my.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        restaurantTable.tabBarItem.title=@"Restaurants";
        [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"my.png"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
     [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"nearme.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";

    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
 nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"nearme.png"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];

    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"search.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"search.png"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"settings.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"settings.png"];

    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];

    

    
    
//    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
//    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];

        // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    //[self.navigationController pushViewController:self.tabBarController animated:YES];

    
}
#pragma mark - Uitextfield Delegate
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [cardNo_Textfield resignFirstResponder];
    [cardType_Textfield resignFirstResponder];
    [issueTextfield resignFirstResponder];
    
    [pickerViewData removeFromSuperview];
    
}
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



#pragma mark -  SendPaymentDetail Method

 -(void)sendPaymentDetail
{
    
    
    
    
    NSLog(@"cardNo_Textfield= %@",cardNo_Textfield.text);
    NSLog(@"expMonthTextField= %@",expMonth_Btn.titleLabel.text);
    NSLog(@"expYearTextField= %@",expYear_Btn.titleLabel.text);
    
    
    if ([cardNo_Textfield.text isEqualToString:@""] || [expYear_Btn.titleLabel.text isEqualToString:@""]  || [expMonth_Btn.titleLabel.text isEqualToString:@""] ||[issueTextfield.text isEqualToString:@""])
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"All Fields Are Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self showLoadingView];
        NSLog(@"%lu",(unsigned long)cardNo_Textfield.text.length);
//        if (!(cardNo_Textfield.text.length==16))
//        {
//            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Please Fill The Correct Card No." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//
//        }
      
        NSString * const StripePublishableKey = @"pk_live_F6QdbFuE5EwCKy6tDEpg8Mie";
        
        STPCard *card = [[STPCard alloc] init];
        card.number = cardNo_Textfield.text;
        card.expMonth =[expMonth_Btn.titleLabel.text intValue];
        card.expYear =[expYear_Btn.titleLabel.text intValue];
        card.cvc = issueTextfield.text;
        [Stripe createTokenWithCard:card publishableKey:StripePublishableKey completion:^(STPToken *token, NSError *error)
         
        {
            if (error)
            {
                [self hideLoadingView];
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your card's number is invalid" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];

                NSLog(@"%@",error);
                //[self handleError:error];
            } else
            {
                NSLog(@"%@",token.tokenId);
                //[self createBackendChargeWithToken:token completion:PKPaymentAuthorizationStatusSuccess];
                //[self sendData:token.tokenId];
                
                [self postUserPlan:token.tokenId];
                
            }
        }];
      
        
    }
    
   }



 //- (void)createBackendChargeWithToken:(STPToken *)token
//                          completion:(void (^)(PKPaymentAuthorizationStatus))completion {
//    NSURL *url = [NSURL URLWithString:@"http://bitebc.ca/api/api/charge.php"];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
//    request.HTTPMethod = @"POST";
//    NSString *body     = [NSString stringWithFormat:@"stripeToken=%@", token.tokenId];
//    request.HTTPBody   = [body dataUsingEncoding:NSUTF8StringEncoding];
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response,
//                                               NSData *data,
//                                               NSError *error) {
//                               if (error) {
//                                   NSLog(@"%@",error);
//                                   
//                               } else {
//                                     NSLog(@"%@",token.tokenId);
//                               }
//                           }];
//}

#pragma mark -  UITableView Method

-(void)showOptions
{
    if (buttonV==1)
    {
        pickerViewData=[[UIPickerView alloc]initWithFrame:CGRectMake(120, 99,190,110)];

    }
    else if (buttonV==2)
    {
         pickerViewData=[[UIPickerView alloc]initWithFrame:CGRectMake(120, 117+30,90, 140)];
    }
    else if (buttonV==3)
    {
          pickerViewData=[[UIPickerView alloc]initWithFrame:CGRectMake(225, 117+30,85, 140)];
    }

        pickerViewData.backgroundColor=[UIColor whiteColor];
    pickerViewData.dataSource=self;
    pickerViewData.delegate=self;
    [self.view addSubview:pickerViewData];
    
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (buttonV==1)
    {
        return [smartCardsArr objectAtIndex:row];    }
    else if(buttonV==2)
    {
      return [yearArr objectAtIndex:row];
        
    }
    else if(buttonV==3)
    {
      return [monthArr objectAtIndex:row];
        
    }
    else
    {
        return nil;
    }
    

    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (buttonV==1)
    {
        return smartCardsArr.count;
        
        NSLog(@"%lu",(unsigned long)smartCardsArr.count);
    }
    else if (buttonV==2)
    {
        NSLog(@"%lu",(unsigned long)yearArr.count);
        return yearArr.count;
        
    }
    else if (buttonV==3)
    {
        return monthArr.count;
        NSLog(@"%lu",(unsigned long)monthArr.count);
    }
    else
    {
        return 4;
    }

}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (buttonV==1)
    {
        //cardNo_Textfield.text=[smartCardsArr objectAtIndex:indexPath.row];
        
        [cardType_Btn setTitle:[smartCardsArr objectAtIndex:row] forState:UIControlStateNormal];
        [cardType_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cardType_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
    }
    else if(buttonV==2)
    {
        //   e.text=[yearArr objectAtIndex:indexPath.row];
        
        [expYear_Btn setTitle:[yearArr objectAtIndex:row] forState:UIControlStateNormal];
        [expYear_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        expYear_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
        
        
    }
    else if(buttonV==3)
    {
        
        
        [expMonth_Btn setTitle:[monthArr objectAtIndex:row] forState:UIControlStateNormal];
        [expMonth_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        expMonth_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
        
        
        // expMonthTextField.text=[monthArr objectAtIndex:indexPath.row];
        
    }
    list_tag=0;
    [pickerViewData removeFromSuperview];
    
    

    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (buttonV==1)
    {
       return smartCardsArr.count;
        
        NSLog(@"%lu",(unsigned long)smartCardsArr.count);
    }
    else if (buttonV==2)
    {
        NSLog(@"%lu",(unsigned long)yearArr.count);
        return yearArr.count;
 
    }
    else if (buttonV==3)
    {
        return monthArr.count;
         NSLog(@"%lu",(unsigned long)monthArr.count);
    }
    else
    {
        return 4;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
      
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor colorWithRed:0.2510 green:0.2078 blue:0.2980 alpha:0.8];
        
    }
    if (buttonV==1)
    {
          cell.textLabel.text=[smartCardsArr objectAtIndex:indexPath.row];
    }
    else if(buttonV==2)
    {
        cell.textLabel.text=[yearArr objectAtIndex:indexPath.row];;

    }
    else if(buttonV==3)
    {
        cell.textLabel.text=[showMonthArr objectAtIndex:indexPath.row];;
        
    }

    
    
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.font=[UIFont boldSystemFontOfSize:12];
    
    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
    
     if (buttonV==1)
     {
         //cardNo_Textfield.text=[smartCardsArr objectAtIndex:indexPath.row];
         
         [cardType_Btn setTitle:[smartCardsArr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
         [cardType_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         cardType_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
     }
     else if(buttonV==2)
     {
                //   e.text=[yearArr objectAtIndex:indexPath.row];
         
         [expYear_Btn setTitle:[yearArr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [expYear_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         expYear_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];

       
              }
     else if(buttonV==3)
     {
         
        
         [expMonth_Btn setTitle:[monthArr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
          [expMonth_Btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         expMonth_Btn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
         

         // expMonthTextField.text=[monthArr objectAtIndex:indexPath.row];
         
     }
   list_tag=0;
    [optionTableView removeFromSuperview];
    

}
 #pragma mark -  Buttons Pressed Method
-(void)listButtonPressed
{
    if (list_tag==0)
    {
        buttonV=1;
          list_tag=1;

        [self showOptions];
        
        

    }
    else
    {
         list_tag=0;
        [pickerViewData removeFromSuperview];
       
       
    }
    
    
    
//    NSLog(@"list button pressed for card type");
////    UIView * view;
//    UIButton *button;
//    
//    int h=69+30;
//    
//    
//    if (list_tag==0)
//    {
//        
//        for (int i=0; i<4; i++)
//        {
//            
//            button=[UIButton buttonWithType:UIButtonTypeCustom];
//            button.frame=CGRectMake(230, h, 80, 20);
//            button.backgroundColor=[UIColor lightGrayColor];
//            button.tag=i;
//            [button setTitle:@"MasterCard" forState:UIControlStateNormal];
//            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            
//            button.titleLabel.font=[UIFont boldSystemFontOfSize:13];
//            h+=21;
//            [self.view addSubview:button];
//        }
//        list_tag=1;
//    }
//    else
//    {
//        [button removeFromSuperview];
//    }
//    
    
}

-(void)expire_listButtonPressed:(UIButton*)sender
{
    if (sender.tag==0)
    {
        if (list_tag==0)
        {
            list_tag=1;
            buttonV=2;
            [self showOptions];
        }
        else
        {
            list_tag=0;
            [pickerViewData removeFromSuperview];

        }
            
       
    }
    else if (sender.tag==1)
    {
         if (list_tag==0)
         {
             list_tag=1;
             buttonV=3;
             [self showOptions];
             
         }
        else
        {
            list_tag=0;
            [optionTableView removeFromSuperview];

        }
        
          NSLog(@"expire_listButtonPressed= %ld",(long)sender.tag);
    }
    
}
-(void)start_listButtonPressed:(UIButton*)sender
{
    if (sender.tag==0)
    {
        NSLog(@"%ld",(long)sender.tag);
    }
    else if (sender.tag==1)
    {
        NSLog(@"%ld",(long)sender.tag);
    }
    
}

 #pragma mark -  Submit method
-(void)submit_ButtonPressed
{
    NSLog(@"submitt pressed");
   
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"save"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if ([cardNo_Textfield.text isEqualToString:@""] || [expYear_Btn.titleLabel.text isEqualToString:@""]  || [expMonth_Btn.titleLabel.text isEqualToString:@""] ||[issueTextfield.text isEqualToString:@""])
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"All Fields Are Required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else
    {
        [self sendPaymentDetail];

    }

        //[self postUserPlan];
    
}


-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Processing..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark -  POST Method
-(void)postcustomerID
{
    
   
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
     flag=1;
    
}

-(void)postUserPlan:(NSString *)token
{
    
    NSLog(@"%@",token);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    NSLog(@"%@",self.coupn_Code);
 
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"planid=%@&customerid=%@&stripeToken=%@&couponcode=%@",self.planID,[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],token,self.coupn_Code];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@payment.php?",post_url];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    
    [request setTimeoutInterval:180];
    
    
    
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencode" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    //NSOperationQueue* qu=[[NSOperationQueue alloc]init];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}

#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSLog(@"flag value is == %d",flag);
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"data is = \n %@",dataDict);
    
    
    if (flag==1)
    {
        NSString* status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
        if ([status isEqualToString:@"1"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"daysleft"] forKey:@"daysleft"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"package_name"] forKey:@"pack"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"planid"] forKey:@"planid"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]);
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"]);
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"planid"]);
            
            
//            HelperViewController * helper =[[HelperViewController alloc]init];
//            helper.planTime=self.planMonths;
//            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//            [self.navigationController pushViewController:helper animated:YES];
            [self tabBar];
        }
        
        
    }
    else if(flag==0)
        
    {
        
        NSString* status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
        
        
        if ([status isEqualToString:@"1"])
        {
            
            
            [self postcustomerID];
            //[self tabBar];
        }
        else
        {
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Invalid Transaction" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        }
        

    }
    

    
    
   
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UIPicker




@end
