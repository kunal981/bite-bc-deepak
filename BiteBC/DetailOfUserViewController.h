//
//  DetailOfUserViewController.h
//  BiteBC
//
//  Created by brst on 7/11/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailOfUserViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDataDelegate,UIPickerViewDelegate,UIPickerViewDataSource>


{
    NSMutableArray *imagesArr;
     NSMutableArray *labelTextArr;
    
    
    NSURLConnection *connection;
    
    NSMutableData *data;
    
    NSMutableDictionary *dataDictionary;
    
    NSString *daysleft,*packName,*plan_id;
    NSMutableArray *cityArray;
    UIPickerView *pickerViewData;
    UITextField *city;
    NSString *city_id;
    NSMutableArray *cityIDArray;
    NSString *checkString;
    UIToolbar *toolBarForPeriodPicker;
}
@property(strong,nonatomic)NSString *idString;
@property(strong,nonatomic)NSString *titleStr;


@end
