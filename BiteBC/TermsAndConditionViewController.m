//
//  TermsAndConditionViewController.m
//  BiteBC
//
//  Created by brst on 8/29/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "TermsAndConditionViewController.h"
#import "AppDelegate.h"

@interface TermsAndConditionViewController ()

@end

@implementation TermsAndConditionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIView * navigateView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,50)];
    navigateView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]];
    [self.view addSubview:navigateView];
    
    UIButton * doneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame=CGRectMake(5,20 ,50,25);
    [doneBtn setTitle:@"Back" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15.5];
    [doneBtn addTarget:self action:@selector(doneBtn_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [navigateView addSubview:doneBtn];
    
    UILabel * labelAgree=[[UILabel alloc]initWithFrame:CGRectMake(75,22,180,20)];
    labelAgree.text=@"Terms And Conditions";
    labelAgree.textColor=[UIColor whiteColor];
    labelAgree.backgroundColor=[UIColor clearColor];
    labelAgree.font=[UIFont boldSystemFontOfSize:17];
    [navigateView addSubview:labelAgree];
    

    
//        NSString* myText=[NSString stringWithFormat:@"%@",@"Bite BC is a large scale diners club in the form of a mobile app providing members with discounts on eating out.\n\n\nEating out can be very expensive, which more often than not stops people from doing it as often as they would like to. With our intuitive app you will be able to locate all of our participating restaurants at a touch of a button and will save hundreds of dollars over the course of the year.\nAs a company we have two main aims; to help members by making eating out more affordable and to also help restaurants by encouraging people to eat out more often.\n\n\nAll of our restaurants will offer unlimited usage, meaning that our members are not restricted to just one visit per restaurant; you can go back as many times as you wish. With your Bite BC membership you get half price dining at all our restaurants (either 50% off or 2 for 1 across all courses"];
//   
    
    
    NSString * myText=[NSString stringWithFormat:@"%@",@"1. INTRODUCTION\n 1.1	This page (together with the documents referred to on it) tells you the terms and conditions on which we supply Bite diner’s club membership. Further details regarding Bite diner’s club can be found on our website www.bitebc.ca (our website). Please read these terms and conditions carefully and make sure that you understand them, before ordering your membership for our app. You should understand that by ordering a Bite membership, you agree to be bound by these terms and conditions.\n\n1.2	You should print a copy of these terms and conditions for future reference.\n\n1.3	Please click on the button marked \" Click here to confirm you have read and agree to our terms and conditions and that you consent to us processing your data in accordance with our privacy policy\" on the Bite diner’s club purchase page on our app if you accept them. Please understand that if you refuse to accept these terms and conditions, you will not be able to purchase a membership with us nor become a member of the club.\n1.4	We reserve the right to amend these terms and conditions at any time by giving you notice by posting the amended terms and conditions on our site. However, please note that you will be subject to the terms and conditions in force at the time that you ordered a membership from us, unless any change to these terms and conditions is required to be made by law or governmental authority (in which case it will apply to orders previously placed by you).\n\n2. INFORMATION ABOUT US\n\n2.1	We operate our site and mobile apps. We are Bite Diners Club Ltd. a company registered in Canada with our registered office at 1325 Rolston street, Vancouver, BC, V6B0M2.\n\n3. YOUR STATUS\n\n3.1	By placing an order for a Bite membership through our site or mobile app, you warrant that you are legally capable of entering into binding contracts and you are at least 18 years old.\n\n4. HOW THE CONTRACT BETWEEN YOU AND US IS FORMED\n\n4.1	After placing an order for a Bite membership, you will receive an e-mail from us acknowledging that we have received your order. Please note that this does not mean that your order has been accepted. Your order constitutes an offer to us to become a member of the club. All orders are subject to acceptance by us, and we will confirm such acceptance to you by sending you an e-mail that confirms that your membership has been accepted and activated. The contract between us will only be formed when we send you a confirmation of acceptance of payment for membership.\n\n5. TERM\n\n5.1	The club is an on-going subscription service so your membership is continuous and your membership is renewed automatically at the end of each membership period. If you have supplied an email address we will send you an electronic reminder approximately 2 weeks before your renewal is due to advise you of the details of the new subscription. If you do not want to renew your membership you should contact us by email to info@bitebc.ca at any point within your membership period and no later than 5pm on the working day preceding your renewal date. Your renewal date being 3 days prior to the expiry date stated on your membership plan. You are required to inform us if you change your correspondence email address. We will not be liable for any non-receipt of communication from us, including non-receipt of the renewal reminder.\n\n6. CONSUMER RIGHTS\n\n6.1 You have the right to cancel your initial registration of membership with us within 14 days of your original purchase. This does not apply to subsequent renewals.\n6.2	To cancel your membership, you must email us at info@bitebc.ca Monday to Friday 9.00am to 5.00pm. Upon cancellation you will receive a confirmation email; it is recommended that this is kept for your own personal records.\n6.3	If you cancel your membership within the cooling off period, you will be entitled to a refund of your membership, less our $10 administration charge for cancellations.\n\n7.AVAILABILITY AND DELIVERY\n\n7.1	You will receive access to your membership immediately once payment has cleared and you receive confirmation email, unless there are any exceptional circumstances. In which case please contact us at info@bitebc.ca.\n\n8. PRICE AND  PAYMENT\n\n8.1	The price of membership of the club will be subject to change and as quoted on our site or mobile app from time to time, except in cases of obvious error.\n8.2 Prices include TAX.\n8.3	Prices are liable to change at any time, but changes will not affect orders in respect of which we have already sent you confirmation.\n 8.4	Payment must be by credit or debit card, or by such other method as we may agree from time to time. We will charge your credit or debit card when your order is placed.\n\n9. Participating Restaurants AND USE OF YOUR Bite Membership\n\n9.1	On presentation of your Bite membership, participating restaurants will offer either 50% off your total food bill (applying to everyone dining in the group; although, the relevant restaurant may place a limit on the maximum number of people per booking (please check our site for details) or 2 for 1 across all courses order (the cheapest item from each course will be deducted from the bill).\n\n9.2	Participating restaurants may exclude Fridays, Saturdays, all of December and bank holidays. Please check our app for which restrictions apply for each restaurant. Mothers' Day, Fathers' Day and Valentines' Day are automatically excluded from the offer. Please refer to individual restaurant details page in our app as other exclusions may apply.\n\n9.3	If a participating restaurant has a telephone icon listed on its page on our app or site advance bookings must be made and by calling the restaurant and your use of Bite membership must be mentioned.\n\n9.4 Offers advertised on our site or app are only available to members who present their checkin on their mobile phone and their membership is confirmed by the server. Such offers are not available in conjunction with any other offers that participating restaurants may be running, which may include set menus or any other menus other than the standard a la carte menu.\n\n9.5	The expiry date of each Bite Membership will vary and will always be checked at each restaurant. Expired Bite memberships are not accepted by participating restaurants. Memberships are strictly non-transferable and can only be used by named members and their dining partners, up to the limited specified by participating restaurants on our site. Any attempted misuse of your Bite Membership may result in your membership being revoked.\n\n9.6 We will use reasonable endeavours to update our site and mobile app to show the particulars of participating restaurants and the terms of their availability for participation in the club. Participating restaurants may, however, be entitled to withdraw from the club or to change the terms and conditions of their availability after you have become a member and we shall have no liability for any such withdrawals or changes in terms and conditions or availability.\n\n9.7 Members will have the benefit of any additional restaurants which join the club at a later date and any increase in availability of participating restaurants.\n\n9.8	Our printed marketing material is intended as a guide of restaurants who are participating at the time of publication and, therefore, may not include all participating restaurants at any one time.\n\n10. OUR LIABILITY\n\n10.1	Subject to clause 10.3, if we fail to comply with these terms and conditions, we shall only be liable for the membership fee.\n\n10.2	Subject to clause 10.3, we will not be liable for losses that result from our failure to comply with these terms and conditions that fall into the following categories:\n10.2.1	loss of income or revenue;\n10.2.2	loss of business;\n10.2.3  loss of profits; or\n10.2.4	loss of anticipated savings.\n\n10.3 Nothing in this agreement excludes or limits our liability for:\n10.3.1	death or personal injury caused by our negligence;\n10.3.2	fraud or fraudulent misrepresentation;\n\n10.4	Where you purchase food from any participating restaurant, any losses or liability arising out of, or in connection with, such food shall be the relevant participating restaurant's liability. We accept no liability for any bad experiences or bad food at any of the participating restaurants. We will not become involved in any dispute between you and any restaurant.\n10.5 We do not give any warranty for any goods or services accessed through, or displayed on, our site or mobile app.\n\n11. WRITTEN  COMMUNICATION\n\n11.1 Applicable laws require that some of the information or communications we send to you should be in writing. When using our site, you accept that communication with us will be mainly electronic. We will contact you by e-mail or provide you with information by posting notices on our website. For contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights.\n\n12. NOTICES\n\n12.1 All notices given by you to us must be given to The Operations Director at info@bitebc.ca. We may give notice to you at either the e-mail or postal address you provide to us when placing an order, or in any of the ways specified in clause 11 above. Notice will be deemed received and properly served immediately when posted on our website, 24 hours after an e-mail is sent, or three days after the date of posting of any letter. In proving the service of any notice, it will be sufficient to prove, in the case of a letter, that such letter was properly addressed, stamped and placed in the post and, in the case of an e-mail, that such e-mail was sent to the specified e-mail address of the addressee.\n\n13.WAIVER\n\n13.1 Failure by us to enforce any of these terms and conditions will not prevent us from subsequently relying on, or enforcing, them.\n\n14. SEVERABILITY\n\n14.1	If any court or competent authority decides that any of the provisions of these terms and conditions are invalid, unlawful or unenforceable to any extent, the term will, to that extent only, be severed from the remaining terms, which will continue to be valid to the fullest extent permitted by law.\n\n15. THIRD PARTY RIGHTS\n\n15.1 A person who is not party to these terms and conditions shall not have any rights under or in connection with it under the Contracts.\n\n16	ENTIRE AGREEMENT\n\n16.1 These terms and conditions and any document expressly referred to in them constitute the whole agreement between us and supersede all previous discussions, correspondence, negotiations, previous arrangement, understanding or agreement between us relating to the subject matter of these terms and conditions. We each acknowledge that, in entering into these terms and conditions, neither of us relies on, or will have any remedies in respect of, any representation or warranty (whether made innocently or negligently) that is not set out in these terms and conditions or the documents referred to in them. Nothing in this clause limits or excludes any liability for fraud."];
    
    UITextView *review_Textview;
    if (!IS_IPHONE_5)
    {
        review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,54,316,420)];
    }
    else
    {
        review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,54,316,510)];
    }
    
    review_Textview.backgroundColor=[UIColor whiteColor];
    review_Textview.layer.borderWidth=1.0;
    review_Textview.layer.masksToBounds=YES;
    review_Textview.layer.cornerRadius=4;
    review_Textview.text=myText;
    review_Textview.editable=NO;
    review_Textview.font=[UIFont systemFontOfSize:14];
    review_Textview.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
    
    
    [self.view addSubview:review_Textview];

    
    

    
}
-(void)doneBtn_Pressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
