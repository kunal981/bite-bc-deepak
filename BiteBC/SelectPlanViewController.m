//
//  SelectPlanViewController.m
//  BiteBC
//
//  Created by brst on 7/1/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "SelectPlanViewController.h"
#import "DataClass.h"
#import "MBProgressHUD.h"
#import "ReviewViewController.h"
#import "RestaurantTableViewController.h"
#import "AppDelegate.h"

@interface SelectPlanViewController ()
{
    UIColor* custom;
    UIColor* backColor;
    UIScrollView *scroll;
    
}


@end

@implementation SelectPlanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Select Membership Plan";

}
-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}
- (void)viewDidLoad
{
    custom=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    backColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];

    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton=YES;
    
    scroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height)];
    [self.view addSubview:scroll];
    [self fetchPlansData];
    
    
    
}


-(void)makeViews
{
    
    DataClass * getData;
    
    NSLog(@"%lu",(unsigned long)arrData.count);
    int h=0;
    for (int i=0; i<arrData.count; i++,h=h+170)
    {
        getData = [arrData objectAtIndex:i];
        
        UIView *planView=[[UIView alloc]initWithFrame:CGRectMake(0, h,self.view.frame.size.width, 160)];
        planView.tag=i;
        planView.backgroundColor=[UIColor clearColor];
        [scroll addSubview:planView];
    
        
        UIView* upperView=[[UIView alloc]initWithFrame:CGRectMake(30,14, 260, 10)];
        upperView.backgroundColor=custom;
        [planView addSubview:upperView];
        
        freeView=[[UIView alloc]initWithFrame:CGRectMake(30,25,260, 100)];
        freeView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        [planView addSubview:freeView];

        
        freeButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [freeButton setFrame:CGRectMake(30,127,260,30)];
        freeButton.tag=i;
        [freeButton addTarget:self action:@selector(freeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [freeButton setTitle:@"Sign up" forState:UIControlStateNormal];
        freeButton.titleLabel.font=[UIFont boldSystemFontOfSize:20];
        [freeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        freeButton.backgroundColor=custom;
        [planView addSubview:freeButton];

        
        UILabel * memberShipLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,5,240,20)];
        memberShipLabel.text=getData.nameOfPlan;
        memberShipLabel.textColor=custom;
        memberShipLabel.textAlignment=NSTextAlignmentCenter;
        memberShipLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [memberShipLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:18]];
        [freeView addSubview:memberShipLabel];
        
        UILabel * atLabel=[[UILabel alloc]initWithFrame:CGRectMake(105,28,50,20)];
        atLabel.text=@"at";
        atLabel.textColor=custom;
        atLabel.textAlignment=NSTextAlignmentCenter;
        atLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [atLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:18]];
        [freeView addSubview:atLabel];
        
        UILabel * dollarLabel=[[UILabel alloc]initWithFrame:CGRectMake(80,55,110,40)];
        dollarLabel.text=getData.priceOfPlan;
        dollarLabel.textColor=backColor;
        dollarLabel.textAlignment=NSTextAlignmentCenter;
        dollarLabel.adjustsFontSizeToFitWidth=YES;
        dollarLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [dollarLabel setFont:[UIFont boldSystemFontOfSize:30]];
        [freeView addSubview:dollarLabel];
        
        
        
        UITapGestureRecognizer* freeTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(taponFree:)];
        freeTap.numberOfTapsRequired=1;
        freeTap.numberOfTouchesRequired=1;
        [planView addGestureRecognizer:freeTap];
        

        
    }
    if (!IS_IPHONE_5)
    {
         scroll.contentSize=CGSizeMake(self.view.frame.size.width, h+80);
    }
    else
    {
         scroll.contentSize=CGSizeMake(self.view.frame.size.width, h+40);
    }
   
    
}
-(void)taponFree:(UITapGestureRecognizer *)reco
{
    
    NSLog(@"%ld",(long)reco.view.tag);
    
    
    DataClass * getData= [arrData objectAtIndex: reco.view.tag];
    
    ReviewViewController * review=[[ReviewViewController alloc]init];
    review.idString=getData.idOfPlan;
    review.planNameStr=getData.nameOfPlan;
    review.priceString=getData.priceOfPlan;
    review.subTotalString=getData.subTotalofPlan;
    review.orderTotalStr=getData.subTotalofPlan;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [self.navigationController pushViewController:review animated:YES];
    
}

 #pragma mark - FetchPlansData
-(void)fetchPlansData
{
    
    
    
    [self showLoadingView];
    
//    NSString* strURL=[NSString stringWithFormat:@"%@plans.php",post_url];
//    NSURL *url3=[NSURL URLWithString:strURL];
//    NSURLRequest *request=[NSURLRequest requestWithURL:url3];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
//    connection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"customerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@plans.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",postData);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dict);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
    
    
    
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
        
    {
    
        
        arrayOfData=[[NSMutableArray alloc]init];
         arrData=[[NSMutableArray alloc]init];
        
        arrayOfData=[dict valueForKey:@"products"];
        
        for (int i=0 ; i< arrayOfData.count; i++)
        {
     
        
            
            DataClass * obj1=[DataClass new];
            
            detail=[arrayOfData objectAtIndex:i];
            
            obj1.nameOfPlan=[detail valueForKey:@"name"];
            obj1.idOfPlan=[detail valueForKey:@"id"];
            obj1.priceOfPlan=[detail valueForKey:@"price"];
            obj1.subTotalofPlan=[detail valueForKey:@"subtotal"];
            
            [arrData addObject:obj1];
            obj1=nil;
            
            
        }
  
        [self makeViews];

         }
    else
    {
        [self hideLoadingView];
    }
    
    
    
}

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


-(void)freeButtonPressed:(UIButton *)sender
{
    NSLog(@"%ld",(long)sender.tag);
         
    DataClass * getData= [arrData objectAtIndex:sender.tag];
    
    ReviewViewController * review=[[ReviewViewController alloc]init];
    review.idString=getData.idOfPlan;
    review.planNameStr=getData.nameOfPlan;
    review.priceString=getData.priceOfPlan;
    review.subTotalString=getData.subTotalofPlan;
    review.orderTotalStr=getData.subTotalofPlan;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController pushViewController:review animated:YES];

    
    
    
}


-(void)oneMonthButtonPressed
{
    
    
      NSLog(@"%ld",(long)oneMonthButton.tag);
 }
-(void)yearlyButtonPressed
{
      NSLog(@"%ld",(long)yearlyButton.tag);
    
     
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
