//
//  MapViewViewController.h
//  BiteBC
//
//  Created by brst on 7/30/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
 
@interface MapViewViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,NSURLConnectionDelegate>


{
    NSURLConnection *connectionIS;
    
    NSMutableData * data;
    NSDictionary *dataDict,*detailDict;
    
    
    NSString * latitude,*longitude,*restName,*rest_id;
    
    NSMutableArray * dataArr;
    
    
}
@property(strong,nonatomic)CLLocationManager* locationManager;
@property(strong,nonatomic)MKMapView* mapView;
@property(strong,nonatomic)NSString *zipcode_str;

@property(strong,nonatomic)NSString *entityId_str;
@property(strong,nonatomic)NSString *nameOfRest;




@end
