//
//  ImageModel.h
//  PageController
//
//  Created by brst on 11/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageModel : NSObject
- (id)initWithImageName:(NSString *)imageName;

@property (nonatomic, strong) NSString *imageName;

@property (nonatomic) NSInteger rating;
@end
