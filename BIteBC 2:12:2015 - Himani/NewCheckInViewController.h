//
//  NewCheckInViewController.h
//  BiteBC
//
//  Created by brst on 10/17/15.
//  Copyright © 2015 Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCheckInViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate>{
    UIToolbar *toolBar;
    UIToolbar *toolBar1;
    BOOL keyboardVisible;
    NSURLConnection *connection;
    
    NSMutableData *data;
    
    NSMutableDictionary *dataDictionary;
    int flag;
    NSString *bill;
    NSString *discountstr;
}
@property (weak, nonatomic) IBOutlet UIView *backVIew;
@property(nonatomic,strong)NSString *restid;
@property(nonatomic,strong)NSString * expiry_Date;
@property(nonatomic,strong)NSString *hotelNAME;
@property(nonatomic,strong)NSString *profileImage_String;
@property(nonatomic,strong)NSString *cust_id;
@property(nonatomic,strong)NSString *memberShip;

@property (weak, nonatomic) IBOutlet UIView *monthlyMemberShipPlanView;

@property (strong, nonatomic) IBOutlet UIImageView *imgView_Profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_MemberID;
@property (strong, nonatomic) IBOutlet UILabel *lbl_monthlyMemberShip;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dayLeft;
@property (strong, nonatomic) IBOutlet UITextField *text_bill;
@property (strong, nonatomic) IBOutlet UITextField *text_discount;
- (IBAction)btn_confirmPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
