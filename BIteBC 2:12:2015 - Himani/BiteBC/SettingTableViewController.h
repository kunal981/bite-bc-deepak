//
//  SettingTableViewController.h
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewController : UITableViewController

{
    
    
    NSMutableArray* settingArray,*iconArray;
    
    UIView *cellView;
    UILabel *nameLabel;
    
    UIImageView *iconsView,*arrowImageView;
}

@end
