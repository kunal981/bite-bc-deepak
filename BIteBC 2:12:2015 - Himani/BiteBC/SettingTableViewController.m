//
//  SettingTableViewController.m
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "SettingTableViewController.h"
#import "DetailOfUserViewController.h"
#import "SliderPageViewController.h"
#define NAME_TAG 1
#define IMAGE_TAG 2
#define PHOTO_TAG 3

@interface SettingTableViewController ()

@end

@implementation SettingTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
}

- (void)viewDidLoad
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    
    self.navigationItem.title=@"Settings";
    [super viewDidLoad];
    settingArray=[[NSMutableArray alloc]initWithObjects:@"User",@"Select City",@"Current Membership Plan",@"About Us",@"How BiteBC Works",@"How to use the APP",@"Terms and Conditions",nil];
    iconArray=[[NSMutableArray alloc]initWithObjects:@"usr.png",@"city-1.png",@"change.png",@"about.png",@"que.png",@"howToUse",@"terms.png",nil];

    
    self.tableView.separatorColor=[UIColor clearColor];
   // self.tableView.backgroundColor=[UIColor grayColor];
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return settingArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0, 2, 320, 43)];
        cellView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        iconsView = [[UIImageView alloc] initWithFrame:CGRectMake(7, 10, 16, 16)];
        iconsView.tag = IMAGE_TAG;
        iconsView.layer.masksToBounds=YES;
        iconsView.backgroundColor=[UIColor clearColor];
        iconsView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:iconsView];

        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,8,200,20)];
        nameLabel.tag = NAME_TAG;
        nameLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
        nameLabel.backgroundColor=[UIColor clearColor];
        nameLabel.textColor = [UIColor blackColor];
        nameLabel.numberOfLines=2;
        nameLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:nameLabel];
        
        arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(292,13,10,16)];
        arrowImageView.tag = PHOTO_TAG;
        arrowImageView.image=[UIImage imageNamed:@"arw.png"];
        arrowImageView.backgroundColor=[UIColor clearColor];
        arrowImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        arrowImageView.image=[UIImage imageNamed:@"arw.png"];
        [cellView addSubview:arrowImageView];


        
        
    }
    nameLabel.text=[settingArray objectAtIndex:indexPath.row];
    iconsView.image=[UIImage imageNamed:[iconArray objectAtIndex:indexPath.row]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==5) {
        SliderPageViewController * slideVC=[[SliderPageViewController alloc]init];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        [self.navigationController pushViewController:slideVC animated:NO];
    }else{
    DetailOfUserViewController* detail=[[DetailOfUserViewController alloc]init];
    detail.idString=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    detail.titleStr=[settingArray objectAtIndex:indexPath.row];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [self.navigationController pushViewController:detail animated:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath   *)indexPath
{
    return 45;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
