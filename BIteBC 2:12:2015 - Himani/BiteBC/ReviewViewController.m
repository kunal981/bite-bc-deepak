//
//  ReviewViewController.m
//  BiteBC
//
//  Created by brst on 7/3/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "ReviewViewController.h"
#import "PaymentViewController.h"
#import "RestaurantTableViewController.h"
#import "DetailPageTableViewController.h"
#import "SettingTableViewController.h"
#import "NearMeViewController.h"
#import "MBProgressHUD.h"
#import "TermsAndConditionViewController.h"
#import "HelperViewController.h"
#import "AppDelegate.h"
static int flag=0;
 int tickImageFlag = 0;
static int coupn_code=0;
@interface ReviewViewController ()
{
    
    UIColor* preColor;
    UIColor* textColor;
     UIColor* applyButtonBackColor;
    UITextField* emailT;
    NSString *str;
}

@end

@implementation ReviewViewController
@synthesize idString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%d",tickImageFlag);
    if (tickImageFlag == 0)
    {
        
    }
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
       
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
  
    flag=0;
}
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}



-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
}


- (void)viewDidLoad
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidekeyboard)];
    tapGesture.numberOfTapsRequired = 1;
    
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    

    NSLog(@"%@",self.planNameStr);
    
    preColor=[UIColor colorWithRed:1.0000 green:1.0000 blue:1.0000 alpha:1.0];
    textColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
     applyButtonBackColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];

   // @"Enter Promo Code:"
    
    
    str = @"";

    sixMonthsPlansArray=[[NSMutableArray alloc]init];
    
    
    
    [sixMonthsPlansArray addObject:self.planNameStr];
    [sixMonthsPlansArray addObject:self.priceString];
    //[sixMonthsPlansArray addObject:str];
    
    
    if ([self.timeStr isEqualToString:@"ms_30 Day Free Trial"])
    {
        NSLog(@"time = %@",self.timeStr);
        str = @"30 Day";
    }
    else if ([self.timeStr isEqualToString:@"ms_12 months membership plan"])
    {
        NSLog(@"time = %@",self.timeStr);
        str = @"12 Month";
    }
    else if ([self.timeStr isEqualToString:@"ms_monthly membership plan"])
    {
        NSLog(@"time = %@",self.timeStr);
        str = @"Monthly Plan";
    }
    else
    {
        NSLog(@"dfs");
    }
    
    
    [sixMonthsPlansArray addObject:str];
    [sixMonthsPlansArray addObject:self.subTotalString];
    

    
    
//    str = [str stringByReplacingOccurrencesOfString:self.timeStr
//                                         withString:@"Day"];
//    NSLog(@"str == %@",str);
    
    
    
    self.view.backgroundColor=[UIColor colorWithRed:0.9647 green:0.9686 blue:0.9765 alpha:1.0];
    
    if ([idString isEqualToString:@"3"] || [idString isEqualToString:@"209"])
    {
        
        nameOfPlansArray=[[NSMutableArray alloc]initWithObjects:@"Plan Name:",@"Price:",@"Time:",@"Subtotal:", nil];

            [self premiumViewMethod];
            
            self.navigationItem.title=@"Review Your Order";

    }
    else
    {
        nameOfPlansArray=[[NSMutableArray alloc]initWithObjects:@"Plan Name:",@"Price:",@"Time:",@"Subtotal:",@"Enter Promo Code:", nil];

        [self premiumViewMethod];
         self.title=@"Review Your Order";
    }
    
    
}

-(void)hidekeyboard
{
    [valueTextfield resignFirstResponder];
}


-(void)confirmView
{
    UIImageView  * logoImage =[[UIImageView alloc]initWithFrame:CGRectMake(55,30,215,110)];
    logoImage.image=[UIImage imageNamed:@"confirm-page.png"];
    logoImage.layer.masksToBounds=YES;
    [self.view addSubview:logoImage];
    UILabel * label;
    if (!IS_IPHONE_5)
    {
         label =[[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-300, self.view.frame.size.width, 80)];
    }
    else
    {
        label =[[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-360, self.view.frame.size.width, 80)];
    }
   
    
    label.backgroundColor=[UIColor clearColor];
    label.numberOfLines=3;
    label.textColor=[UIColor whiteColor];
    label.textAlignment=NSTextAlignmentCenter;
    label.text=@"To activate your 3 months free membership please click on the share with facebook button below";
    [self.view addSubview:label];
    
    UIView* terms_View=[[UIView alloc]initWithFrame:CGRectMake(5,self.view.frame.size.height-220,self.view.frame.size.width-10, 50)];
    terms_View.backgroundColor=[UIColor clearColor];
    [self.view addSubview:terms_View];

    //************* THREE BUTTONS ON TERMS-VIEW *****************//
    
    UIButton * buttonAgree=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonAgree.frame=CGRectMake(3, 13, 26, 26);
    UIImage* img=[UIImage imageNamed:@"NotSelected"];
    [buttonAgree setImage:img forState:UIControlStateNormal];
    buttonAgree.tag=1;
    [buttonAgree addTarget:self action:@selector(agreementButton_pressed:) forControlEvents:UIControlEventTouchUpInside];
    //    buttonAgree.layer.borderColor=[UIColor colorWithRed:0.2118 green:0.4745 blue:0.6902 alpha:2.0].CGColor;
    //    buttonAgree.layer.borderWidth=1.0;
    [terms_View addSubview:buttonAgree];
    
    
    UILabel * labelAgree=[[UILabel alloc]initWithFrame:CGRectMake(32, 14,180,20)];
    labelAgree.text=@"I have read and agreed to";
    labelAgree.textColor=[UIColor whiteColor];
    labelAgree.backgroundColor=[UIColor clearColor];
    labelAgree.font=[UIFont systemFontOfSize:13];
    [terms_View addSubview:labelAgree];
    
    UIButton * termsConditions=[UIButton buttonWithType:UIButtonTypeCustom];
    termsConditions.frame=CGRectMake(183, 12, 140, 26);
    [termsConditions setTitle:@"Terms And Conditions" forState:UIControlStateNormal];
    [termsConditions setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    termsConditions.titleLabel.font=[UIFont systemFontOfSize:12];
    [termsConditions addTarget:self action:@selector(termsButton_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [terms_View addSubview:termsConditions];
 
//    //************* FACEBOOK-VIEW HERE AND FACEBOOK SHARE BUTTON ON THIS *****************//
//  
//    UIView* fb_View=[[UIView alloc]initWithFrame:CGRectMake(5,self.view.frame.size.height-170,self.view.frame.size.width-10, 120)];
//    fb_View.backgroundColor=[UIColor colorWithRed:0.0980 green:0.0980 blue:0.1059 alpha:1.0];
//    fb_View.layer.borderWidth=9.0;
//    fb_View.layer.borderColor=[UIColor colorWithRed:0.1255 green:0.1255 blue:0.1333 alpha:1.0].CGColor;
//    [self.view addSubview:fb_View];
//    
//   UIButton* fbButton=[UIButton buttonWithType: UIButtonTypeCustom];
//    UIImage * image=[UIImage imageNamed:@"fb-share.png"];
//    [fbButton setImage:image forState:UIControlStateNormal];
//    fbButton.layer.masksToBounds=YES;
//    //[fbButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    //fbButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3608 blue:0.3490 alpha:1.0];
//    fbButton.frame=CGRectMake(80,44,150,35);
//    [fbButton addTarget:self action:@selector(fb_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [fb_View addSubview:fbButton];
//
    
    
}
-(void)premiumViewMethod
{
    int yPos=4;
    int h=10;
 
      for (int i=0; i<nameOfPlansArray.count; i++)
      {
        
        premiumView=[[UIView alloc]initWithFrame:CGRectMake(0,yPos, 320, 35)];
        premiumView.backgroundColor=preColor;
        premiumView.tag=i;
        yPos+=44;
        [self.view addSubview:premiumView];
        
        UILabel* label=[[UILabel alloc]initWithFrame:CGRectMake(10,h, 130, 15)];
        label.textAlignment=NSTextAlignmentLeft;
        label.font=[UIFont systemFontOfSize:14];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.text=[nameOfPlansArray objectAtIndex:i];
        h+=0;
        [premiumView addSubview:label];
        
        
        if (!(i==4))
        {
            UILabel* label2=[[UILabel alloc]initWithFrame:CGRectMake(140,h, 180, 15)];
            label2.textAlignment=NSTextAlignmentLeft;
            label2.font=[UIFont systemFontOfSize:14];
            label2.backgroundColor=[UIColor clearColor];
            label2.adjustsFontSizeToFitWidth=YES;
            label2.textColor=textColor;
            label2.text=[sixMonthsPlansArray objectAtIndex:i];
            h+=0;
            [premiumView addSubview:label2];
        }
            else
            {
                
                
                valueTextfield=[[UITextField alloc]initWithFrame:CGRectMake(140,h-4,90,25)];
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 28)];
                valueTextfield.leftView = paddingView;
                valueTextfield.leftViewMode = UITextFieldViewModeAlways;
                valueTextfield.layer.cornerRadius=3;
                valueTextfield.layer.borderWidth=1.0;
                valueTextfield.layer.borderColor=[UIColor blackColor].CGColor;
                valueTextfield.delegate=self;
                valueTextfield.borderStyle=UITextBorderStyleLine;
               // valueTextfield.backgroundColor=[UIColor whiteColor];
                valueTextfield.textAlignment=NSTextAlignmentNatural;
                valueTextfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                valueTextfield.font=[UIFont systemFontOfSize:13];
                //valueTextfield.placeholder=@"  Enter username";
                [premiumView addSubview:valueTextfield];

                
                applyButton = [UIButton buttonWithType:UIButtonTypeSystem];
                [applyButton setFrame:CGRectMake(228,h-4,60,25)];
              
                [applyButton addTarget:self action:@selector(applyButtonPressed) forControlEvents:UIControlEventTouchUpInside];
                [applyButton setTitle:@"Apply" forState:UIControlStateNormal];
                applyButton.titleLabel.font=[UIFont boldSystemFontOfSize:14];
                [applyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                applyButton.backgroundColor=applyButtonBackColor;
                
                [premiumView addSubview:applyButton];

                
            }
        }
    UIView* mainView;
     if ([idString isEqualToString:@"3"] || [idString isEqualToString:@"209"])
     {
          mainView=[[UIView alloc]initWithFrame:CGRectMake(0, 180, 320, 140)];
     }else
     {
          mainView=[[UIView alloc]initWithFrame:CGRectMake(0, 230, 320, 140)];
     }
    
    mainView.backgroundColor= [UIColor clearColor];
    [self.view addSubview:mainView];
    
         // TOTAL VIEW
        UIView* orderTotalView=[[UIView alloc]initWithFrame:CGRectMake(0,2, 320, 35)];
        orderTotalView.backgroundColor=preColor;
        [mainView addSubview:orderTotalView];
      
        UILabel* label=[[UILabel alloc]initWithFrame:CGRectMake(10,10, 150, 15)];
        label.textAlignment=NSTextAlignmentLeft;
        label.font=[UIFont boldSystemFontOfSize:14];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.text=@"Order Total:";
        
        [orderTotalView addSubview:label];
        
        order_Total=[[UILabel alloc]initWithFrame:CGRectMake(140,10, 160, 15)];
        order_Total.textAlignment=NSTextAlignmentLeft;
        order_Total.font=[UIFont boldSystemFontOfSize:14];
        order_Total.backgroundColor=[UIColor clearColor];
        order_Total.textColor=[UIColor blackColor];
        order_Total.text=self.orderTotalStr;
        [orderTotalView addSubview:order_Total];
    
        // USER-DETAIL VIEW
        name_mailView=[[UIView alloc]initWithFrame:CGRectMake(0,41,320, 55)];
        name_mailView.backgroundColor=preColor;
        [mainView addSubview:name_mailView];
        
        UIImageView* icons=[[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 15, 15)];
        icons.image=[UIImage imageNamed:@"user.png"];
        [name_mailView addSubview:icons];
        
        
        label=[[UILabel alloc]initWithFrame:CGRectMake(40,8, 150, 15)];
        label.textAlignment=NSTextAlignmentLeft;
        label.font=[UIFont systemFontOfSize:13];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"];
      
        [name_mailView addSubview:label];
        
        icons=[[UIImageView alloc]initWithFrame:CGRectMake(8,32,20, 13)];
        icons.image=[UIImage imageNamed:@"email.png"];
        [name_mailView addSubview:icons];

         emailT=[[UITextField alloc]initWithFrame:CGRectMake(40,30, 150, 15)];
        emailT.textAlignment=NSTextAlignmentLeft;
        emailT.font=[UIFont systemFontOfSize:13];
        emailT.delegate=self;
        emailT.enabled=NO;
         emailT.keyboardType=UIKeyboardTypeEmailAddress;
        emailT.backgroundColor=[UIColor clearColor];
        emailT.textColor=[UIColor blackColor];
       emailT.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"];
         [name_mailView addSubview:emailT];
    
    
        //PLACE-HOLDER BUTTON
        placeHoderButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [placeHoderButton setFrame:CGRectMake(2,380,316,30)];
        placeHoderButton.layer.borderWidth=1.0;
        placeHoderButton.layer.borderColor=[UIColor blackColor].CGColor;
        [placeHoderButton addTarget:self action:@selector(placeHoderButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [placeHoderButton setTitle:@"Place Order" forState:UIControlStateNormal];
        placeHoderButton.titleLabel.font=[UIFont boldSystemFontOfSize:17];
        [placeHoderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        placeHoderButton.backgroundColor=applyButtonBackColor;
        [self.view addSubview:placeHoderButton];
    
    UIButton * buttonAgree=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonAgree.frame=CGRectMake(4, 110, 26, 26);
    UIImage* img=[UIImage imageNamed:@"NotSelected.png"];
    [buttonAgree setImage:img forState:UIControlStateNormal];
    buttonAgree.tag=1;
    [buttonAgree addTarget:self action:@selector(agreementButton_pressed:) forControlEvents:UIControlEventTouchUpInside];
//    buttonAgree.layer.borderColor=[UIColor colorWithRed:0.2118 green:0.4745 blue:0.6902 alpha:2.0].CGColor;
//    buttonAgree.layer.borderWidth=1.0;
    [mainView addSubview:buttonAgree];
    
    
    UILabel * labelAgree=[[UILabel alloc]initWithFrame:CGRectMake(33, 110,180,20)];
    labelAgree.text=@"I have read and agreed to";
    labelAgree.textColor=[UIColor grayColor];
    labelAgree.backgroundColor=[UIColor clearColor];
    labelAgree.font=[UIFont systemFontOfSize:13];
    [mainView addSubview:labelAgree];
    
    UIButton * termsConditions=[UIButton buttonWithType:UIButtonTypeCustom];
    termsConditions.frame=CGRectMake(185, 107, 140, 26);
    [termsConditions setTitle:@"Terms And Conditions" forState:UIControlStateNormal];
    [termsConditions setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    termsConditions.titleLabel.font=[UIFont systemFontOfSize:12];
    [termsConditions addTarget:self action:@selector(termsButton_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:termsConditions];
     
}

 // A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs)
    {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

#pragma mark - Terms And Conditions
-(void)agreementButton_pressed:(UIButton *)sender
{
    
    if (tickImageFlag==0)
    {
        [sender setImage:[UIImage imageNamed:@"Selected.png"] forState:UIControlStateNormal];
        tickImageFlag=1;
    }
    else
    {
        [sender setImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
        tickImageFlag=0;

        
    }
    
   }
-(void)termsButton_Pressed
{
       TermsAndConditionViewController * terms=[[TermsAndConditionViewController alloc]init];
    [self presentViewController:terms animated:YES completion:nil];
    NSLog(@"button pressed");
    
}

-(void)fullDiscountMethod
{
    
    if ([[dataDict valueForKey:@"subtotal"] isEqualToString:@"$0.00"])
     
          coupn_code=-1;
        
        [self postUserPlan:@"test123"];
        return ;
        
    

}

#pragma mark - APPLY BUTTON PRESSED

-(void)applyButtonPressed
{
     [valueTextfield resignFirstResponder];
    
    if ([valueTextfield.text isEqualToString:@""])
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Please fill the coupon code." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else
    {
        [self coupon_Code_Method];
    }
    
    if (!IS_IPHONE_5)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=95;
        [self.view setFrame:frame1];
    }
    else
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=65;
        [self.view setFrame:frame1];
    }
}


    -(void)placeHoderButtonPressed
{
    if (tickImageFlag==1)
    {
        NSLog(@"%@",idString);
        if ([idString isEqualToString:@"3"] || [idString isEqualToString:@"209"] || [[dataDict valueForKey:@"subtotal"]isEqualToString:@"$0.00"])
        {
            
            [self showLoadingView];
            [self postUserPlan:@"test123"];
        }
        else
        {
            PaymentViewController* payment=[[PaymentViewController alloc]init];
            
            payment.planID=self.idString;
            payment.planMonths=self.planNameStr;
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"coupnCode"]);
            if (valueTextfield.text.length==0)
            {
                payment.coupn_Code=[NSString stringWithFormat:@""];
            }
            else
            {
                if ([[NSUserDefaults standardUserDefaults]boolForKey:@"coupnCode"])
                {
                    payment.coupn_Code=[NSString stringWithFormat:@"%@",valueTextfield.text];
                }
                else
                {
                    payment.coupn_Code=[NSString stringWithFormat:@""];
                    
                }

            }
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            
            [self.navigationController pushViewController:payment animated:YES];
            
        }
        

        
    }else
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Please Accept The Terms And Conditions" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
#pragma mark -  POST Method
-(void)postcustomerID
{
      flag=1;
     NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
  
    
}

-(void)postUserPlan:(NSString *)token
{
    
   
    
    flag=0;
    
     NSLog(@"%d",flag);
    
    coupn_code=-1;
    NSLog(@"token%@",token);
    NSLog(@"customerid%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    NSLog(@"planid%@",self.idString);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"planid=%@&customerid=%@&stripeToken=%@",self.idString,[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],token];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@payment.php?",post_url];
    
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    
    [request setTimeoutInterval:180];
    
    
    
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencode" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    //NSOperationQueue* qu=[[NSOperationQueue alloc]init];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
-(void)coupon_Code_Method
{
    flag=-1;
   coupn_code=1;
    [self showLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    
    NSString *post = [NSString stringWithFormat:@"planid=%@&coupon=%@",self.idString,valueTextfield.text];
  
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@check_coupon.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
   
    
}

#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"flag value is = %d coupncode value is = %d",flag,coupn_code);
   
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"data is = \n %@",dataDict);
    if (coupn_code==1)
    {
        [self hideLoadingView];
        NSString* status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
        
                if ([status isEqualToString:@"1"])
        {
            
            [valueTextfield resignFirstResponder];
            
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[dataDict valueForKey:@"message"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"coupnCode"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                order_Total.text=[dataDict valueForKey:@"subtotal"];
                UILabel * lbl=[[[[self.view subviews]objectAtIndex:3]subviews]objectAtIndex:1];
                
                lbl.text=[dataDict valueForKey:@"subtotal"];

           
            
            
                       [self hideLoadingView];
            }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"coupnCode"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Oops!" message:[dataDict valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        }

    }
    
    NSLog(@"%d",flag);
    
    if (flag==1)
    {
        [self hideLoadingView];
        NSString* status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
        if ([status isEqualToString:@"1"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"daysleft"] forKey:@"daysleft"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"package_name"] forKey:@"pack"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:@"planid"] forKey:@"planid"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]);
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"]);
            
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"planid"]);
            
            [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self hideLoadingView];
            
             
            [self tabBarScreen];
        }
        
        
    }

    
    if (flag==0)
    {
        NSString* status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
         NSString* message=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"message"]];
        
        if ([status isEqualToString:@"1"])
        {
            
            
            [self postcustomerID];
            
        }
        else
        {
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        }
        
    }
    
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}



#pragma mark - UITabBarController Method
-(void)tabBarScreen
{
    self.navigationController.navigationBarHidden=YES;
    
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"Activity"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Restaurants";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"Activity"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"Near-me"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"Near-me"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"Search"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"Search"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"Setting"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"Setting"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
    
    
    
    
    //    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
    //    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    [self.navigationController pushViewController:self.tabBarController animated:YES];
    
    
}
#pragma mark - UITextField Delegate Method's
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!IS_IPHONE_5)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-65;
        [self.view setFrame:frame1];
    }
    else
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-40;
        [self.view setFrame:frame1];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!IS_IPHONE_5)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=95;
        [self.view setFrame:frame1];
    }
    else
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=65;
        [self.view setFrame:frame1];
    }

    
//    CGRect frame1=self.view.frame;
//    frame1.origin.y=20;
//    [self.view setFrame:frame1];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
