//
//  DetailPageTableViewController.h
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface DetailPageTableViewController : UITableViewController<UISearchBarDelegate,NSURLConnectionDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UISearchControllerDelegate,UIGestureRecognizerDelegate>
{
    
    NSMutableArray *hotelNameArray,*allDataArr,*dataArray;
    
    NSURLConnection *connection;
    
    UIView *cellView;
    
    UIImageView *restImageView,*discountImage;
    
    NSMutableDictionary *dataDict;
    NSMutableData *data;
    
   
    NSString *city_ID;
    
    NSString *searchString;
    
    
    UILabel *hotelName_Label,*cusine_label,*savedLabel,*disAmountLabel;
    
    ASStarRatingView *staticStarRatingView;
    NSMutableArray *cityArray;
    NSMutableArray *cityIdArray;
    UIPickerView *pickerViewData;
    UITextField *txtCity;
    UIToolbar *toolBarForPeriodPicker;
    NSString *cityName;
    NSString *cityid;
    BOOL selectedCity;
}

@property(strong,nonatomic)NSString* idString;
@property(strong,nonatomic)NSString* pageTitle;

@property(strong,nonatomic)UISearchBar* search;
@end
