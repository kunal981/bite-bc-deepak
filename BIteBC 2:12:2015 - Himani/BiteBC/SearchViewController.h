//
//  SearchViewController.h
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UISearchBarDelegate,UISearchDisplayDelegate,NSURLConnectionDelegate>
{
    UIView* searchView;
    
    NSURLConnection *connection;
    
    NSMutableDictionary* searchDict,*detailDict,*datalDict;
   
    
    NSMutableData* data;
    
    NSString* searchString;
    
     NSString *offerAvlStr ,*noOfPeopleStr,*cuisineStr;

}
@property(strong,nonatomic)UISearchBar* search;
@end
