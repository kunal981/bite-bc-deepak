//
//  HelperViewController.m
//  BiteBC
//
//  Created by brst on 9/13/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "HelperViewController.h"

@interface HelperViewController ()
{
    UIScrollView * scrolView;
}
@end

@implementation HelperViewController
-(void)viewWillAppear:(BOOL)animated
{
    
    NSString * message=[NSString stringWithFormat:@"Thank you for purchasing a %@ membership to Bite BC",self.planTime];
    
    UIAlertView * thnxAlert= [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [thnxAlert show];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    imagesArry=[[NSMutableArray alloc]initWithObjects:@"img1.png",@"img2.png" ,nil];
    
    self.navigationItem.title=@"How it works";
    self.navigationItem.hidesBackButton=YES;
    
    skipButton=[UIButton buttonWithType: UIButtonTypeCustom];
    [skipButton setTitle:@"Next" forState:UIControlStateNormal];
    [skipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    skipButton.backgroundColor=[UIColor colorWithRed:0.8980 green:0.9059 blue:0.9176 alpha:1.0];
    skipButton.frame=CGRectMake(140, 5,60,20);
    skipButton.layer.cornerRadius=4;
    skipButton.layer.borderWidth=1.0;
    skipButton.layer.borderColor=[UIColor whiteColor].CGColor;
    skipButton.titleLabel.font=[UIFont boldSystemFontOfSize:13];
    [skipButton addTarget:self action:@selector(skipButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    barButton=[[UIBarButtonItem alloc]initWithCustomView:skipButton];
    self.navigationItem.rightBarButtonItem=barButton;

    
    
    self.navigationController.navigationBar.hidden=NO;

    
   scrolView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrolView.delegate=self;
    scrolView.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3373 alpha:1.0];

    [self.view addSubview:scrolView];
    
    //self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Newletter3.jpg"]];
    
    
    
    [self imagesView];
    
}

-(void)imagesView
{
    UIView * view;
    
     int h = 0;
    
    for (int i=0; i<4; i++)
    {
        
        if (i==0)
        {
            
            
            UIImage * img =[UIImage imageNamed:@"share_one.png"];
           
            view =[[UIView alloc]initWithFrame:CGRectMake(0, h, self.view.frame.size.width,img.size.height-195)];
            view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3373 alpha:1.0];
            
//            UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(0, 5, 320, 18)];
//            label.text=@"HOW IT WORKS";
//            label.textAlignment=NSTextAlignmentCenter;
//            label.textColor=[UIColor whiteColor];
//            label.font=[UIFont boldSystemFontOfSize:15];
//            [view addSubview:label];

            imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 0,300,img.size.height-200)];
            imageView.image=img;
            imageView.layer.masksToBounds=YES;
            h=h+view.frame.size.height;
            [view addSubview:imageView];

        }else if (i==1){
            
            
            
        UIImage * img =[UIImage imageNamed:@"share_third.png"];
            
    view =[[UIView alloc]initWithFrame:CGRectMake(0, h, self.view.frame.size.width,img.size.height-200)];
    view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3373 alpha:1.0];
           
            UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(0,0, 320, 18)];
            label.text=@"OFFERS";
            label.textAlignment=NSTextAlignmentCenter;
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont boldSystemFontOfSize:15];
            [view addSubview:label];

        imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 25,300,img.size.height-230)];
        imageView.image=img;
        imageView.layer.masksToBounds=YES;
         h=h+view.frame.size.height;
        [view addSubview:imageView];
       
        }
        
        else if (i==2){
            
            
            UIImage * img =[UIImage imageNamed:@"fourthH.png"];
           
            view =[[UIView alloc]initWithFrame:CGRectMake(0, h, self.view.frame.size.width,img.size.height-155)];
            view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3373 alpha:1.0];
           
            UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(0,0, 320, 18)];
            label.text=@"CHECKIN";
            label.textAlignment=NSTextAlignmentCenter;
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont boldSystemFontOfSize:15];
            [view addSubview:label];
            

            imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20,300,img.size.height-180)];
            imageView.image=img;
            imageView.layer.masksToBounds=YES;
             h=h+view.frame.size.height;
            [view addSubview:imageView];
        }
        else if (i==3){
            UIImage * img =[UIImage imageNamed:@"sixth.png"];
            
            view =[[UIView alloc]initWithFrame:CGRectMake(0, h, self.view.frame.size.width,img.size.height)];
            view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3373 alpha:1.0];
            
            UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(0,0, 320, 18)];
            label.text=@"MEMBERSHIP";
            label.textAlignment=NSTextAlignmentCenter;
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont boldSystemFontOfSize:15];
            [view addSubview:label];

            imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20,300,img.size.height-190)];
            imageView.image=img;
            imageView.layer.masksToBounds=YES;
            h=h+img.size.height-20;

            [view addSubview:imageView];
        }
        

        
        [scrolView addSubview:view];
        scrolView.contentSize=CGSizeMake(320, h+10);

    }
    
    
    
}

-(void)skipButtonPressed
{
    [self tabBarScreen];
}
#pragma mark - UITabBarController Method
-(void)tabBarScreen
{
    self.navigationController.navigationBarHidden=YES;
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"Activity"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Restaurants";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"Activity"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"Near-me"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"Near-me"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"Search"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"Search"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"Setting"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"Setting"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
    
    
    
    
    //    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
    //    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    [self.navigationController pushViewController:self.tabBarController animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
