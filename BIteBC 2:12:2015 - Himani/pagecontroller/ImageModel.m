//
//  ImageModel.m
//  PageController
//
//  Created by brst on 11/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "ImageModel.h"

@implementation ImageModel
- (id)initWithImageName:(NSString *)imageName
{
    self = [super init];
    if (self)
    {
        _imageName = imageName;
        _rating = 0;
    }
    
    return self;
}
@end
