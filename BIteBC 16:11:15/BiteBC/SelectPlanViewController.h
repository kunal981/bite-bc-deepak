//
//  SelectPlanViewController.h
//  BiteBC
//
//  Created by brst on 7/1/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectPlanViewController : UIViewController<NSURLConnectionDelegate>
{
    
    UIView *freeView,*oneMonthView,*twelveMonthView;
    
    
    UIButton *freeButton,*oneMonthButton,*yearlyButton;
    
    NSURLConnection *connection;
    
    
    NSMutableDictionary* dict,*detail;
    
    NSMutableData* data;
    
    NSMutableArray* arrayOfData,*arrData;
    
    
    NSString *nameOfPlan,*priceOfPlan,*subTotalofPlan,*idOfPlan;
    
}

@end
