//
//  PaymentViewController.h
//  BiteBC
//
//  Created by brst on 7/3/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PaymentViewController : UIViewController<UITextFieldDelegate,UITabBarControllerDelegate,NSURLConnectionDataDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    
    UIView *cardDetailView;
    
    NSMutableArray* cardNameArray,*smartCardsArr,*monthArr,*yearArr,*showMonthArr;
    
    UITextField *cardNo_Textfield,*cardType_Textfield;
    
    UITextField *expire_Textfield,*start_Textfield,*issueTextfield,*securityTextfield,*expYearTextField,*expMonthTextField;
    
    UIButton *expire_listButton,*start_listButton,*listButton,*submit_Button;
    
    NSMutableData* data;

    NSMutableDictionary * dataDict;
    
    
    NSURLConnection * connection;
    
    
    UITableView *optionTableView;
    UIButton *cardType_Btn,*expYear_Btn,*expMonth_Btn;
    
    UIPickerView *pickerViewData;
}
 

- (void)onLocationSelection;
@property(nonatomic,strong) UITabBarController* tabBarController;

@property(nonatomic,strong) NSString *planID;
@property(nonatomic,strong) NSString *planMonths;
@property(nonatomic,strong) NSString *coupn_Code;
-(void)postUserPlan:(NSString *)token;

-(void)tabBar;
@end
