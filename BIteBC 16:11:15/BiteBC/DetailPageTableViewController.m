//
//  DetailPageTableViewController.m
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "DetailPageTableViewController.h"
#import "DetailOfRestaurantViewController.h"
#import "AppDelegate.h"
#import "DataClass.h"
#import "MBProgressHUD.h"

#define NAME_TAG 1
#define IMAGE_TAG 2
#define FEATURE_TAG 3

@interface DetailPageTableViewController ()
{
    NSMutableDictionary *restaurantDetail;
    NSMutableDictionary *detail;
    
}

@end

@implementation DetailPageTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.tableView animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"SelctedCity"]) {
        NSString *city = [[NSUserDefaults standardUserDefaults]valueForKey:@"newCity"];
        txtCity.text = city;
        city_ID = [[NSUserDefaults standardUserDefaults]valueForKey:@"CITYID"];

 
    }else{
        city_ID = [[NSUserDefaults standardUserDefaults]valueForKey:@"CITYID"];
        NSString *city = [[NSUserDefaults standardUserDefaults]valueForKey:@"city"];
        txtCity.text = city;
    }
    
    
    
}

 - (void)viewDidLoad
{
    [super viewDidLoad];

    cityArray = [[NSMutableArray alloc]init];
    cityIdArray = [[NSMutableArray alloc]init];
    [self getCities];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    
    self.navigationItem.title=@"Search";

    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    
    txtCity = [[UITextField alloc]init];
    txtCity.frame = CGRectMake(0, 0, 120, 35);
    txtCity.placeholder = @"City";
    txtCity.backgroundColor =[UIColor whiteColor];
    txtCity.textColor = [UIColor blackColor];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtCity.leftView = paddingView2;
    txtCity.leftViewMode = UITextFieldViewModeAlways;
    txtCity.adjustsFontSizeToFitWidth = YES;
    txtCity.delegate = self;
    
       [self.tableView addSubview:txtCity];
    UIImageView *imgView = [[UIImageView alloc]init];
    
    imgView.frame =  CGRectMake(txtCity.frame.size.width-20, 12, 12, 12);
    
    imgView.image = [UIImage imageNamed:@"small.png"];
   
        //SEARCHBAR HERE
    self.search=[[UISearchBar alloc]initWithFrame:CGRectMake(txtCity.frame.size.width+1, 0, self.view.frame.size.width- txtCity.frame.size.width, 35)];
    self.search.searchBarStyle=UISearchBarStyleMinimal;
    self.search.placeholder=@"search";
    // self.search.tintColor=[UIColor whiteColor];
    //self.search.backgroundColor=[UIColor  colorWithRed:0.2863 green:0.3216 blue:0.3490 alpha:1];
    self.search.backgroundColor=[UIColor whiteColor];
    self.search.delegate=self;
   
//        toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 35)];
//        
//        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
//        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
//        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
//        
//        
//        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
//        
//        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
//        
//         self.search.inputAccessoryView = toolBarForPeriodPicker;
//        [self.view addSubview:toolBarForPeriodPicker];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           //[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    numberToolbar.barTintColor = [UIColor whiteColor];
    [numberToolbar sizeToFit];
    self.search.inputAccessoryView = numberToolbar;
    
    
    
    for (UIView *subView in self.search.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UIColor * color=[UIColor lightGrayColor];
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //set font color here
                searchBarTextField.textColor = [UIColor blackColor];
                
                searchBarTextField.attributedPlaceholder=[[NSAttributedString alloc] initWithString:@"Enter restaurant name" attributes:@{NSForegroundColorAttributeName: color}];
                
                break;
            }
        }
    }
    
    self.view.userInteractionEnabled = YES;
    
    [self.tableView addSubview:imgView];
    [self.tableView addSubview:self.search];
    
   
    
   // [self.tableView addGestureRecognizer:tap];
   
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint tapLocation = [gestureRecognizer locationInView:self.tableView];
    
    // Query tableview and get location of cell
    NSIndexPath *idp = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    // Now get the actual cell
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idp];
    NSLog(@"%@",cell);
    if (dataArray.count !=0) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}
- (void)doneWithNumberPad
{
//    CGPoint touchLocation = [sender locationOfTouch:0 inView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchLocation];
    
    [self.search resignFirstResponder];
    
}
#pragma mark -Post Methods
-(void)postData
{
    if (txtCity.text.length ==0 || txtCity.text == nil) {
       
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    } else if (searchString.length == 0 || searchString == nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter text to search" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {

    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSLog(@"%@",city_ID);
    
    NSString *post = [NSString stringWithFormat:@"datasearch=%@&city=%@",searchString,city_ID];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@product.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    
    NSLog(@"%@",postLength);
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencode" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSLog(@"request=%@",request);
    //NSOperationQueue* qu=[[NSOperationQueue alloc]init];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];

    }
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=%@",dataDict);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
    if ([status isEqualToString:@"1"])
    {
        
        
        dataArray=[[NSMutableArray alloc]init];
        allDataArr=[[NSMutableArray alloc]init];
        dataArray =[dataDict valueForKey:@"products"];

        for (int i=0; i<dataArray.count; i++)
        {
            
            restaurantDetail=[dataArray objectAtIndex:i];
            
            detail=[restaurantDetail valueForKey:@"detail"];
            
            
            NSLog(@"%@",restaurantDetail);
           
            DataClass* dataIS=[DataClass new];
            
            
            
            
            
            dataIS.hotelCuisine=[detail valueForKey:@"cuisine"];
            dataIS.hotelAdress= [detail valueForKey:@"address"];
            dataIS.offerType=[detail valueForKey:@"offer_available"];
             dataIS.hotelName= [detail valueForKey:@"name"];
            dataIS.hotelPhoto= [detail valueForKey:@"image_url"];
            dataIS.hotel_id= [detail valueForKey:@"entity_id"];
            dataIS.hotelrating= [NSString stringWithFormat:@"%@",[restaurantDetail valueForKey:@"rating"]];
            NSLog(@"%@",dataIS.hotelrating);
            [allDataArr addObject:dataIS];
            dataIS=nil;

        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"No Restaurant Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        allDataArr=nil;    }
    [self.tableView reloadData];

      
}

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allDataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,2, 320,78)];
        cellView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];
        
        restImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10, 58,58)];
        restImageView.tag = IMAGE_TAG;
        restImageView.layer.cornerRadius=8;
        restImageView.layer.masksToBounds=YES;
        //restImageView.backgroundColor=[UIColor  colorWithPatternImage:[UIImage imageNamed:@"ic_launcher.png"]];
        restImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:restImageView];
        
        
        discountImage = [[UIImageView alloc] initWithFrame:CGRectMake(248,23,70,45)];
        discountImage.tag = IMAGE_TAG;
        //discountImage.layer.cornerRadius=8;
        discountImage.layer.masksToBounds=YES;
        discountImage.backgroundColor=[UIColor clearColor];
        discountImage.image=[UIImage imageNamed:@"listing.png"];
        discountImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:discountImage];
        
        disAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(26,6,40,15)];
        disAmountLabel.tag = NAME_TAG;
        disAmountLabel.font = [UIFont boldSystemFontOfSize:18];
        disAmountLabel.backgroundColor=[UIColor clearColor];
        disAmountLabel.textColor = [UIColor whiteColor];
        
        disAmountLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [discountImage addSubview:disAmountLabel];
        
        
        savedLabel = [[UILabel alloc] initWithFrame:CGRectMake(9,5,57,35)];
        savedLabel.tag = NAME_TAG;
        savedLabel.font = [UIFont boldSystemFontOfSize:13];
        savedLabel.numberOfLines=2;
        savedLabel.textAlignment=NSTextAlignmentCenter;
        savedLabel.backgroundColor=[UIColor clearColor];
        savedLabel.textColor = [UIColor whiteColor];
        //savedLabel.text=@"50% off food bill";
        savedLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [discountImage addSubview:savedLabel];
        
        
        hotelName_Label = [[UILabel alloc] initWithFrame:CGRectMake(70,14,170,20)];
        hotelName_Label.tag = NAME_TAG;
        hotelName_Label.font = [UIFont boldSystemFontOfSize:15];
        hotelName_Label.backgroundColor=[UIColor clearColor];
        hotelName_Label.adjustsFontSizeToFitWidth=YES;
        hotelName_Label.textColor = [UIColor darkGrayColor];
        hotelName_Label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:hotelName_Label];
        
        cusine_label = [[UILabel alloc] initWithFrame:CGRectMake(70,35,170,30)];
        cusine_label.tag = FEATURE_TAG;
        cusine_label.numberOfLines=2;
        cusine_label.font = [UIFont systemFontOfSize:12];
        cusine_label.backgroundColor=[UIColor clearColor];
        cusine_label.textColor = [UIColor darkGrayColor];
        cusine_label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:cusine_label];
        
        staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(245,0,80, 27)];
        staticStarRatingView.canEdit = NO;
        staticStarRatingView.maxRating = 5;
        [cellView addSubview:staticStarRatingView];
        
        
     }
    
    DataClass* allData;
    allData = [allDataArr objectAtIndex:indexPath.row];
    
    
    hotelName_Label.text=[allData.hotelName uppercaseString];
    //restImageView.image=[UIImage imageNamed:[photoArray objectAtIndex:indexPath.row]];
    
    if ([allData.hotelPhoto isEqual:[NSNull null]])
        restImageView.image=[UIImage imageNamed:@" "];
    else
        
        [self imageDownloader:allData.hotelPhoto imageView:restImageView];
    
    
    if ([allData.offerType isEqual:[NSNull null]])
    {
        savedLabel.text=@"    N.A";
    }
    else
    {
        
        if ([allData.offerType isEqualToString:@"31"])
        {
            savedLabel.text=@"2 for 1 meals";
        }
        else if([allData.offerType isEqualToString:@"32"])
        {
            savedLabel.text=@"50% off food bill";
        }
        else if([allData.offerType isEqualToString:@"100"])
        {
            savedLabel.text=@"2 for 1";
        }
        else if([allData.offerType isEqualToString:@"101"])
        {
            savedLabel.text=@"50% off";
        }
        else if([allData.offerType isEqualToString:@"102"])
        {
            savedLabel.text=@"40% off";
        }
        else if([allData.offerType isEqualToString:@"103"])
        {
            savedLabel.text=@"30% off ";
        }
        
        else if([allData.offerType isEqualToString:@"104"])
        {
            savedLabel.text=@"40% off food bill ";
        }
        else if([allData.offerType isEqualToString:@"105"])
        {
            savedLabel.text=@"30% off food bill ";
        }

        
        
        
    }
    
    
    if ([allData.hotelCuisine isEqual:[NSNull null]])
    {
        cusine_label.text=@"";
        
    }
    else
    {
        cusine_label.text=allData.hotelCuisine;
    }
  
    id value=allData.hotelrating;
    int rate=[value intValue];
    staticStarRatingView.rating =rate;

    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath   *)indexPath
{
    return 80;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
 

    
//    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
//    UIView *v=[[cell.contentView subviews]objectAtIndex:0];
//    UIImageView *m=(UIImageView *)[[v subviews]objectAtIndex:0];
    
    DataClass *detailData=[allDataArr objectAtIndex:indexPath.row];
    
    NSLog(@"%@",detailData.hotel_id);
    DetailOfRestaurantViewController* detailOfRes=[[DetailOfRestaurantViewController alloc]init];
    detailOfRes.nameOfpRestaureant_String=detailData.hotelName;
    if ([detailData.hotelAdress isEqual:[NSNull null]])
    {
        
    }
    else
    {
        detailOfRes.adressOfRestaurant_String=detailData.hotelAdress;
    }
 //   detailOfRes.imageOf_hotel=m.image;
//    // detailOfRes.imageOf_hotel=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:detailData.hotelPhoto]]];
     detailOfRes.imageOfHotel_string=detailData.hotelPhoto;
    detailOfRes.rating_hotel=detailData.hotelrating;
    detailOfRes.product_id=detailData.hotel_id;
    if ([detailData.hotelCuisine isEqual:[NSNull null]])
    {
        
    }
    else
    {
        detailOfRes.cuisine_String=detailData.hotelCuisine;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController pushViewController:detailOfRes animated:YES];
    
   
         
    }
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        
    }
}
#pragma mark -UISearchBar Delegates

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if ([searchText length] == 0)
    {
        
       [searchBar performSelector:@selector(resignFirstResponder)withObject:nil afterDelay:0];
        
        
        
    }
    
    if (![searchText isEqualToString:@""]) // here you check that the search is not null, if you want add another check to avoid searches when the characters are less than 3
    {
        
        
        searchString=searchText;
        
        NSLog(@"%@",searchString);
        
    }
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
       if (txtCity.text.length ==0 || txtCity.text == nil) {
           [searchBar resignFirstResponder];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
      

        
    }else {

        [searchBar resignFirstResponder];
        [self postData];
    }

    //160022
    
}



#pragma mark - ImageDownloader Method
-(void)imageDownloader:(NSString *)urlStr imageView:(UIImageView *)imageView
{
    dispatch_queue_t imageQueue=dispatch_queue_create("imageDownloader", nil);
    dispatch_async(imageQueue, ^{
        NSURL *imageurl=[NSURL URLWithString:urlStr];
        NSData *imageData=[NSData dataWithContentsOfURL:imageurl];
        UIImage *image=[UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            imageView.image=image;
        });
        
    });
}

#pragma mark - UITextField Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == txtCity) {
        [textField resignFirstResponder];
        
        [toolBarForPeriodPicker removeFromSuperview];
        [pickerViewData removeFromSuperview];
      
        toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [self.view addSubview:toolBarForPeriodPicker];
        
        pickerViewData = [[UIPickerView alloc]init];
        pickerViewData.backgroundColor=[UIColor whiteColor];
        pickerViewData.dataSource=self;
        pickerViewData.delegate=self;
        
        CGRect viewFrame = self.view.frame;
        CGFloat pickerHeight = pickerViewData.frame.size.height;
        // assume you have an outlet called picker
        CGRect pickerFrame = CGRectMake(0.0,txtCity.frame.origin.y+txtCity.frame.size.height+44,
                                        viewFrame.size.width, pickerHeight);
        pickerViewData.frame = pickerFrame;
        
        [self.view addSubview:pickerViewData];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelBtnPressedPeriod
{
   
    
    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
   
}
-(void)doneBtnPressedPeriod
{
  //  [self postData];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"SelctedCity"];
    
    [[NSUserDefaults standardUserDefaults]setValue:txtCity.text forKey:@"newCity"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
    
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [cityArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return cityArray.count;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (row ==0) {
        txtCity.text = @"";
    }else{
    txtCity.text = [cityArray objectAtIndex:row];
    NSLog(@"%@",txtCity.text);
    }
    city_ID = [cityIdArray objectAtIndex:row];
}

-(void)getCities
{
    
    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/city.php"];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSLog(@"About to send req %@",req.URL);
    NSData *dataCity = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if (dataCity ==nil|| dataCity==(id)[NSNull null]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        NSArray *result = [NSJSONSerialization JSONObjectWithData:dataCity options:kNilOptions error:&error];
        NSLog(@"%@",result);
        
        cityName = @"Select city";
        [cityArray addObject:cityName];
        cityid = @" ";
        [cityIdArray addObject:cityid];
        for (int i =0 ; i<result.count; i++) {
            
            cityName = [[result objectAtIndex:i] valueForKey:@"name"];
            cityid = [[result objectAtIndex:i] valueForKey:@"id"];
            [cityIdArray addObject:cityid];
            [cityArray addObject:cityName];
        }
        
        NSLog(@"%@",cityArray);
        
        
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
