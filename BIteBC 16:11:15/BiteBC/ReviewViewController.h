//
//  ReviewViewController.h
//  BiteBC
//
//  Created by brst on 7/3/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
extern int  tickImageFlag;
@interface ReviewViewController : UIViewController<UITextFieldDelegate,UITabBarControllerDelegate>
{
    
     UIView *premiumView;
    
    NSMutableArray* nameOfPlansArray;
    NSMutableArray* sixMonthsPlansArray;
    NSMutableArray* tweleveMonthsPlansArray;
    NSMutableArray* oneMonthsPlansArray;
    
    UIButton *applyButton,*placeHoderButton;
    UITextField* valueTextfield;
    
    UIView* name_mailView;
    
    NSURLConnection * connection;
    NSMutableData * data;
    NSMutableDictionary * dataDict;
    UILabel * sub_Total,*order_Total;
    NSString * coupn_Value,*subTotalValue;
    
    
}

@property(strong,nonatomic)NSString* planNameStr;
@property(strong,nonatomic)NSString* priceString;
@property(strong,nonatomic)NSString* subTotalString;
@property(strong,nonatomic)NSString* orderTotalStr;


@property(strong,nonatomic)NSString* idString;

@property(nonatomic,strong) UITabBarController* tabBarController;


@end
