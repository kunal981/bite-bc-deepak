//
//  NearMeViewController.h
//  BiteBC
//
//  Created by brst on 7/10/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface NearMeViewController : UIViewController<UISearchBarDelegate,UISearchDisplayDelegate,CLLocationManagerDelegate,MKMapViewDelegate,NSURLConnectionDelegate>
{
    NSMutableArray * locationsArr;
    
    float staticLatitude;
    float staticLongitude;

    NSURLConnection *connectionIS;
    
    NSMutableData *dataIS;
    NSMutableDictionary *dataDict;
    
    NSString *searchString;
    
    NSMutableArray *allDataArr,*arrayOfproductArr;
    
    NSMutableDictionary *restaurantDetail,*detail;
    UIButton *infoButton;
    NSString *proID;
    
}
@property(strong,nonatomic)UISearchBar* search;

@property (nonatomic, retain) MKPolyline *routeLine;
@property (nonatomic, retain) MKPolylineView *routeLineView;
//for location in site button
@property(strong,nonatomic)MKMapView* mapView;

@property(strong,nonatomic)CLLocationManager* locationManager;
@property(nonatomic)NSInteger tag;

@end
