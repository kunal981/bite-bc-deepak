//
//  LoginViewController.h
//  BiteBC
//
//  Created by brst on 10/16/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface Register1ViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIButton *loginButton;
    NSURLConnection* connection;
    NSMutableDictionary* userArray;
    
    NSMutableDictionary* dict;
    UIImagePickerController* pickerimage;
    NSMutableData* data;
    
    NSString* base64String;
    UIImage *img22;
    NSMutableArray *cityArray;
    NSMutableArray *cityIDArray;
    UIPickerView *pickerViewData;
    
   // UIScrollView * scroll_View;
    UIToolbar *toolBarForPeriodPicker;
    UIView *viewForPeriodPicker;
    UIView *viewPicker;
    NSString *cityName;
    NSString *cityId;

}
 @property(strong,nonatomic)UITextField *txtFirstname;
@property(strong,nonatomic)UITextField *txtLastName;
@property(strong,nonatomic)UITextField *txtEmail;
@property(strong,nonatomic)UITextField *txtPswrd;
@property(strong,nonatomic)UITextField *txtConfirm_Email;
@property(strong,nonatomic)UITextField *txt_Cell_Number;
@property(strong,nonatomic)UITextField *txtConfirmPswrd;
@property(strong,nonatomic)UITextField *txtSelectCity;
@property(strong,nonatomic)UITextField *txtVerificationField;

@property(strong,nonatomic)NSString *udid;
 @end
