//
//  NewCheckInViewController.m
//  BiteBC
//
//  Created by brst on 10/17/15.
//  Copyright © 2015 Karan Bharara. All rights reserved.
//

#import "NewCheckInViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"

#define kOFFSET_FOR_KEYBOARD 80.0
@interface NewCheckInViewController (){
    double totalBill;
    double discount;
}


@end

@implementation NewCheckInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    flag = 0;
    
    self.lbl_Name.adjustsFontSizeToFitWidth = YES;
    self.lbl_monthlyMemberShip.adjustsFontSizeToFitWidth = YES;
    self.lbl_MemberID.adjustsFontSizeToFitWidth = YES;
    self.text_bill.delegate = self;
    self.text_discount.delegate = self;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _text_bill.leftView = paddingView2;
    _text_bill.leftViewMode = UITextFieldViewModeAlways;
    _text_bill.layer.borderWidth=1.0;
    _text_bill.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _text_discount.leftView = paddingView3;
    _text_discount.leftViewMode = UITextFieldViewModeAlways;
    _text_discount.layer.borderWidth=1.0;
    _text_discount.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    self.view.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
    
    //self.backVIew.backgroundColor = [UIColor colorWithRed:49.0/255 green:124.0/255 blue:119.0/255 alpha:1.0];

    
    [super viewDidLoad];
    totalBill =0;
    discount = 0;
       
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+100);
    self.lbl_Name.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"];
    NSString* urlString=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [[NSUserDefaults standardUserDefaults]valueForKey:@"password"]];
    NSLog(@"%@",urlString);
    
    self.imgView_Profile.clipsToBounds = YES;
    self.imgView_Profile.layer.borderWidth = 2.0;
    self.imgView_Profile.backgroundColor = [UIColor clearColor];
    self.imgView_Profile.layer.cornerRadius = 4.0;
    self.imgView_Profile.layer.borderColor = [UIColor colorWithRed:219.0f/255 green:219.0f/255 blue:219.0f/255 alpha:1].CGColor;
    self.imgView_Profile.layer.masksToBounds = YES;
    
    
    
    self.monthlyMemberShipPlanView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
   // _btnConfirm.layer.borderWidth = 1.0;
    //_btnConfirm.layer.borderColor = [UIColor blackColor].CGColor;
    _btnConfirm.layer.masksToBounds = YES;
    _btnConfirm.layer.cornerRadius = 15.0;
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"] isEqualToString:@"fbMode"])
    {
        [self.imgView_Profile sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        
    }else{
        NSLog(@"%@",self.profileImage_String);
        [self.imgView_Profile sd_setImageWithURL:[NSURL URLWithString:self.profileImage_String] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        //profileImage.image=[UIImage imageNamed:@"ic_launcher.png"];
    }
    
    self.lbl_dayLeft.text= [NSString stringWithFormat:@"%@ Days left",[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]];
    
    self.lbl_MemberID.text = [NSString stringWithFormat:@"Member ID Number : %@",self.cust_id];
    self.lbl_monthlyMemberShip.text = [NSString stringWithFormat:@"%@",self.memberShip];
                              
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    totalBill =0;
    discount = 0;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Identification Card";
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    
}

#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDictionary=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dataDictionary);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"success"]];
    NSString *check_status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"status"]];
    NSString *message = [NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"message"]];
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
        
    {
        
        NSLog(@"%@",check_status);
        
        
        if ([check_status isEqualToString:@"1"])
        {
            _btnConfirm.hidden=YES;
            //[checkinBtn removeFromSuperview];
            
            //NSString * message=[NSString stringWithFormat:@"%@ Bite membership confirmed. Thank you.",[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"]];
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            _text_bill.text = @"";
            _text_discount.text = @"";
           
            [_text_bill resignFirstResponder];
            [_text_discount resignFirstResponder];
            
            
        }
        else
        {
            
            
        }
        
        
        
    }
    else
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
      
        [self hideLoadingView];
    }
    
    
    
}

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark UITextField

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    UIWindow *window  = [[[UIApplication sharedApplication]delegate]window];
    if (!window.isKeyWindow) {
        [window makeKeyAndVisible];
    }
    
    if (textField == self.text_bill) {
        
        if (textField.text.length  == 0)
        {
            textField.text = @"$";
        }

         [toolBar removeFromSuperview];
        
       
        toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        toolBar.barStyle = UIBarStyleBlackOpaque;
        toolBar.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        UIBarButtonItem *okBtn = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStyleBordered target:self action:@selector(DoneBtnPressedPeriod)];
        
        
        [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,okBtn, nil]];
        [toolBar sizeToFit];
        self.text_bill.inputAccessoryView = toolBar;
        //[txtTotalbill layoutIfNeeded];
        
        flag = 1;
    
        
        
    }
    else if (textField == self.text_discount){
         [toolBar removeFromSuperview];
        
        if (textField.text.length  == 0)
        {
            textField.text = @"$";
        }
        

        flag = 2;

        toolBar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        toolBar1.barStyle = UIBarStyleBlackOpaque;
        toolBar1.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
         UIBarButtonItem *okBtn = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStyleBordered target:self action:@selector(DoneBtnPressedPeriod)];
        
        [toolBar1 setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,okBtn ,nil]];
        [toolBar1 sizeToFit];
        self.text_discount.inputAccessoryView = toolBar1;
    }
   
}



-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    if (textField == self.text_bill) {
        totalBill =0;
        
        bill = self.text_bill.text;
        bill = [bill stringByReplacingOccurrencesOfString:@"$" withString:@""];
        NSLog(@"%@",bill);
        
        totalBill =[bill doubleValue];
    }
    if (textField == self.text_discount) {
        discount =0;
       discountstr = self.text_discount.text;
         discountstr = [discountstr stringByReplacingOccurrencesOfString:@"$" withString:@""];
       
      
        discount = [discountstr doubleValue];
    }
    
    if (keyboardVisible) {
        
        [self setViewMovedUp:NO];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    // Make sure that the currency symbol is always at the beginning of the string:
    if (![newText hasPrefix:@"$"])
    {
        return NO;
    }
    
    // Default:
    return YES;
}

-(void)cancelBtnPressedPeriod{
    [self.text_bill resignFirstResponder];
    [self.text_discount resignFirstResponder];
   
}

-(void)DoneBtnPressedPeriod{
    if (flag ==1) {
         [self.text_discount becomeFirstResponder];
    }
    else if (flag ==2){
        [self.text_discount resignFirstResponder];
    }
   
}

#pragma mark Keyboard

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)keyboardWillShow {
    keyboardVisible = true;
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    keyboardVisible = false;
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_confirmPressed:(id)sender {
    [self sendidForReastaurant];
}

#pragma mark -  sendidForReastaurant
-(void)sendidForReastaurant
{
   
    
    totalBill =[bill doubleValue];
    
    //totalBill =[self.text_bill.text doubleValue];
    
    
    
    discount = [discountstr doubleValue];
    NSLog(@"%f %f",totalBill, discount);
    if (bill==nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter total bill." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (discountstr == nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter bite discount." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
        else if(discount>totalBill){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Bite discount should not greater than to total bill." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        _text_discount.text = @"";
        [_text_discount becomeFirstResponder];
    }
    else{
        
        NSLog(@"%@",self.restid);
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
        
        
        [self showLoadingView];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        
        
        NSString *post = [NSString stringWithFormat:@"customer_id=%@&rest_id=%@&total_amount=%@&discount=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],self.restid,bill,discountstr];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        //NSString *urlString=[NSString stringWithFormat:@"%@check_in.php?",post_url];
        NSString *urlString=[NSString stringWithFormat:@"%@report.php?",post_url];
        NSLog(@"%@",urlString);
        
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        
        NSLog(@"request=  %@",request);
        [request setTimeoutInterval:180];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
        
    }
}
@end
