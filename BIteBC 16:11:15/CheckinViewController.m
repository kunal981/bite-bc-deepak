//
//  CheckinViewController.m
//  BiteBC
//
//  Created by brst on 7/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "CheckinViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"

#define kOFFSET_FOR_KEYBOARD 180.0
#define kOFFSET_FOR_KEYBOARD1 200.0

@interface CheckinViewController (){
    double totalBill;
    double discount;
}

@end

@implementation CheckinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    totalBill =0;
    discount = 0;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Identification Card";
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    

    
}

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
   
    self.view.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];

    [super viewDidLoad];
    totalBill =0;
    discount = 0;
    [self.view removeConstraints:self.view.constraints];
    // Do any additional setup after loading the view.
    
    [self newView];
   // [self profileView];
   // [self checkPlan];
}
#pragma mark -  sendidForReastaurant
-(void)sendidForReastaurant
{
    totalBill =[txtTotalbill.text doubleValue];
    discount = [txtBiteDiscount.text doubleValue];
    NSLog(@"%f %f",totalBill, discount);
    if (txtTotalbill.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter total bill." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (txtBiteDiscount.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter bite discount." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (txtRestaurantCode.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter restaurant code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if(discount>totalBill){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Bite discount should not greater than to total bill." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        txtBiteDiscount.text = @"";
        [txtBiteDiscount becomeFirstResponder];
    }
    else{
    
    NSLog(@"%@",self.restid);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    
    
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"customer_id=%@&rest_id=%@&total_amount=%@&discount=%@&code=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],self.restid,txtTotalbill.text,txtBiteDiscount.text,txtRestaurantCode.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //NSString *urlString=[NSString stringWithFormat:@"%@check_in.php?",post_url];
    NSString *urlString=[NSString stringWithFormat:@"%@report.php?",post_url];
        NSLog(@"%@",urlString);
        
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];

    }
}

-(void)newView{
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+100);
    self.lbl_name.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"];
    NSString* urlString=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [[NSUserDefaults standardUserDefaults]valueForKey:@"password"]];
    NSLog(@"%@",urlString);
    
   
   
   
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"] isEqualToString:@"fbMode"])
    {
        [self.imgView_profile sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        
    }else{
        [self.imgView_profile sd_setImageWithURL:[NSURL URLWithString:self.profileImage_String] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        //profileImage.image=[UIImage imageNamed:@"ic_launcher.png"];
    }
    
    self.lbl_dayleft.text= [NSString stringWithFormat:@"%@ Days left",[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]];
    
}

-(void)profileView
{
    scrollBack = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    scrollBack.backgroundColor =[UIColor clearColor];
    scrollBack.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+100);
    
     
    UIView *whiteView;
    UILabel *label;
    UILabel *noteLabel;
    UILabel *notelabel1;
    if (!IS_IPHONE_5)
    {
        whiteView=[[UIView alloc]initWithFrame:CGRectMake(10, 30, 300, 200)];
        label=[[UILabel alloc]initWithFrame:CGRectMake(5,235,self.view.frame.size.width-10,20)];
           noteLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,21)];
        notelabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0,80,self.view.frame.size.width,40)];

    }
    else
    {
         whiteView=[[UIView alloc]initWithFrame:CGRectMake(10, 45, 300, 200)];
         label=[[UILabel alloc]initWithFrame:CGRectMake(5,250,self.view.frame.size.width-10,20)];
           noteLabel=[[UILabel alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,21)];
         notelabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0,80,self.view.frame.size.width,40)];
    }
    whiteView.backgroundColor=[UIColor whiteColor];
    whiteView.layer.cornerRadius=3;
    whiteView.layer.borderWidth=1.0;
    whiteView.layer.borderColor=[UIColor blackColor].CGColor;
    [self.view addSubview:scrollBack];
    [scrollBack addSubview:whiteView];
    
    UIImageView *imageV=[[UIImageView alloc]initWithFrame:CGRectMake(110,-5,60,60)];
    imageV.backgroundColor=[UIColor clearColor];
    imageV.image=[UIImage imageNamed:@"ic_launcher.png"];
    [whiteView addSubview:imageV];
    
    
    UIView * greenView=[[UIView alloc]initWithFrame:CGRectMake(0, 40 ,300 ,120)];
    greenView.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [whiteView addSubview:greenView];
    
    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(70, 3, 150,20)];
    nameLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"];
    nameLabel.textAlignment=NSTextAlignmentCenter;
    nameLabel.adjustsFontSizeToFitWidth=YES;
    nameLabel.textColor=[UIColor whiteColor];
    nameLabel.font=[UIFont boldSystemFontOfSize:16];
    [greenView addSubview:nameLabel];
    
    
    NSString* urlString=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [[NSUserDefaults standardUserDefaults]valueForKey:@"password"]];
    NSLog(@"%@",urlString);
   
    UIImageView *profileImage=[[UIImageView alloc]initWithFrame:CGRectMake(100,30,90,80)];
    profileImage.layer.masksToBounds=YES;
    profileImage.backgroundColor=[UIColor clearColor];
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"] isEqualToString:@"fbMode"])
    {
        [profileImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];

    }else{
         [profileImage sd_setImageWithURL:[NSURL URLWithString:self.profileImage_String] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        //profileImage.image=[UIImage imageNamed:@"ic_launcher.png"];
    }
    
    
    [greenView addSubview:profileImage];

    UILabel *planName=[[UILabel alloc]initWithFrame:CGRectMake(20,170,250,20)];
    planName.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"]);
    planName.textAlignment=NSTextAlignmentCenter;
    planName.textColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    planName.font=[UIFont boldSystemFontOfSize:18];
    [whiteView addSubview:planName];

   
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor redColor];
    label.textAlignment=NSTextAlignmentCenter;
    label.numberOfLines=2;
    label.adjustsFontSizeToFitWidth=YES;
    label.text= [NSString stringWithFormat:@"Membership expires on - %@ (Days left %@)",self.expiry_Date,[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]];
    label.font=[UIFont boldSystemFontOfSize:15];
    [scrollBack addSubview:label];
    
 
    noteLabel.backgroundColor=[UIColor clearColor];
    noteLabel.textColor=[UIColor grayColor];
    noteLabel.textAlignment=NSTextAlignmentCenter;
    noteLabel.adjustsFontSizeToFitWidth=YES;
    noteLabel.text=@"Please show this screen to the server.";
    noteLabel.numberOfLines=1;
    noteLabel.font=[UIFont boldSystemFontOfSize:13];
    [scrollBack addSubview:noteLabel];
    
    notelabel1.backgroundColor=[UIColor clearColor];
    notelabel1.textColor=[UIColor grayColor];
    notelabel1.textAlignment=NSTextAlignmentLeft;
    notelabel1.adjustsFontSizeToFitWidth=YES;
    notelabel1.text=@"The server must fill in the total bill and Bite discount amounts then click the confirm button below.";
    notelabel1.numberOfLines=2;
    notelabel1.font=[UIFont boldSystemFontOfSize:13];
    [scrollBack addSubview:noteLabel];
    
    if (!IS_IPHONE_5)
    { txtTotalbill=[[UITextField alloc]initWithFrame:CGRectMake(10,330,300,30)];

    }else{
        txtTotalbill=[[UITextField alloc]initWithFrame:CGRectMake(10,340,300,30)];
    }

    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtTotalbill.leftView = paddingView;
    txtTotalbill.leftViewMode = UITextFieldViewModeAlways;
    txtTotalbill.layer.borderWidth=1.0;
    txtTotalbill.layer.borderColor=[UIColor lightGrayColor].CGColor;
    txtTotalbill.delegate=self;
    txtTotalbill.backgroundColor=[UIColor whiteColor];
    txtTotalbill.keyboardType = UIKeyboardTypeDecimalPad;
    txtTotalbill.font=[UIFont systemFontOfSize:13];
    txtTotalbill.placeholder=@"Total bill";
    [scrollBack addSubview:txtTotalbill];
    
    if (!IS_IPHONE_5)
    { txtBiteDiscount=[[UITextField alloc]initWithFrame:CGRectMake(10,370,300,30)];
        
    }else{
        txtBiteDiscount=[[UITextField alloc]initWithFrame:CGRectMake(10,380,300,30)];
    }
    
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtBiteDiscount.leftView = paddingView1;
    txtBiteDiscount.leftViewMode = UITextFieldViewModeAlways;
    txtBiteDiscount.layer.borderWidth=1.0;
    txtBiteDiscount.layer.borderColor=[UIColor lightGrayColor].CGColor;
    txtBiteDiscount.delegate=self;
    txtBiteDiscount.backgroundColor=[UIColor whiteColor];
    txtBiteDiscount.keyboardType = UIKeyboardTypeDecimalPad;
    txtBiteDiscount.font=[UIFont systemFontOfSize:13];
    txtBiteDiscount.placeholder=@"Bite discount";
    [scrollBack addSubview:txtBiteDiscount];
    
    if (!IS_IPHONE_5)
    {
        txtRestaurantCode=[[UITextField alloc]initWithFrame:CGRectMake(10,410,300,30)];
        
    }else{
        txtRestaurantCode=[[UITextField alloc]initWithFrame:CGRectMake(10,420,300,30)];
    }
    
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txtRestaurantCode.leftView = paddingView2;
    txtRestaurantCode.leftViewMode = UITextFieldViewModeAlways;
    txtRestaurantCode.layer.borderWidth=1.0;
    txtRestaurantCode.layer.borderColor=[UIColor lightGrayColor].CGColor;
    txtRestaurantCode.delegate=self;
    txtRestaurantCode.backgroundColor=[UIColor whiteColor];
    txtRestaurantCode.keyboardType = UIKeyboardTypeDecimalPad;
    txtRestaurantCode.font=[UIFont systemFontOfSize:13];
    txtRestaurantCode.placeholder=@"Restaurant code";
    [scrollBack addSubview:txtRestaurantCode];

    //PLACE-HOLDER BUTTON
    checkinBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    if (!IS_IPHONE_5)
    {
        [checkinBtn setFrame:CGRectMake(10,450,300,30)];
    }else
    {
        [checkinBtn setFrame:CGRectMake(10,460,300,30)];
    }
    
    checkinBtn.layer.borderWidth=1.0;
    checkinBtn.layer.borderColor=[UIColor blackColor].CGColor;
    [checkinBtn addTarget:self action:@selector(sendidForReastaurant) forControlEvents:UIControlEventTouchUpInside];
    [checkinBtn setTitle:@"Confirm" forState:UIControlStateNormal];
    checkinBtn.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [checkinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    checkinBtn.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
   
   [scrollBack addSubview:checkinBtn];

    
}



// #pragma mark -  CheckMembership Plan
//-(void)checkPlan
//{
//    
//    [self showLoadingView];
//    
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
//    
//    
//    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
//    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
//    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//    
//    
//    NSLog(@"request=  %@",request);
//    [request setTimeoutInterval:180];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setHTTPBody:postData];
//    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
//    
//    
//}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDictionary=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dataDictionary);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"success"]];
      NSString *check_status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"status"]];
    NSString *message = [NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"message"]];
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
        
    {
        
        NSLog(@"%@",check_status);
        
        
        if ([check_status isEqualToString:@"1"])
        {
            checkinBtn.hidden=YES;
            //[checkinBtn removeFromSuperview];
          
            //NSString * message=[NSString stringWithFormat:@"%@ Bite membership confirmed. Thank you.",[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"]];
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            txtRestaurantCode.text = @"";
            txtBiteDiscount.text = @"";
            txtTotalbill.text = @"";
            [txtBiteDiscount resignFirstResponder];
            [txtRestaurantCode resignFirstResponder];
            [txtTotalbill resignFirstResponder];
            
        }
        else
        {
           
 
        }

  
        
    }
    else
    {
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        txtRestaurantCode.text = @"";
        [txtRestaurantCode becomeFirstResponder];
        [self hideLoadingView];
    }
    
   
    
}

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextField

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
        if (textField == txtTotalbill) {
       // [toolBar removeFromSuperview];
        
        
        toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        toolBar.barStyle = UIBarStyleBlackOpaque;
        toolBar.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
            
       
        [toolBar setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft, nil]];
        [toolBar sizeToFit];
        txtTotalbill.inputAccessoryView = toolBar;
            //[txtTotalbill layoutIfNeeded];
        
  
        
    }
    else if (textField == txtBiteDiscount){
       // [toolBar removeFromSuperview];
        
        

        toolBar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        toolBar1.barStyle = UIBarStyleBlackOpaque;
        toolBar1.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        [toolBar1 setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft, nil]];
        [toolBar1 sizeToFit];
        txtBiteDiscount.inputAccessoryView = toolBar1;
}
    else if (textField == txtRestaurantCode){
        //[toolBar1 removeFromSuperview];
        
        
        toolBar2 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        toolBar2.barStyle = UIBarStyleBlackOpaque;
        toolBar2.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        [toolBar2 setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft, nil]];
        [toolBar2 sizeToFit];
        txtRestaurantCode.inputAccessoryView = toolBar2;
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == txtTotalbill) {
        totalBill =0;
        totalBill =[txtTotalbill.text doubleValue];
    }
    if (textField == txtBiteDiscount) {
        discount =0;
        discount = [txtBiteDiscount.text doubleValue];
    }
    
    if (keyboardVisible) {
        
        [self setViewMovedUp:NO];
    }
}

-(void)cancelBtnPressedPeriod{
    [txtTotalbill resignFirstResponder];
    [txtBiteDiscount resignFirstResponder];
    [txtRestaurantCode resignFirstResponder];
}

#pragma mark Keyboard

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        if ([UIScreen mainScreen].bounds.size.height == 568) {
            rect.origin.y -= kOFFSET_FOR_KEYBOARD;
            rect.size.height += kOFFSET_FOR_KEYBOARD;
        }else{
            rect.origin.y -= kOFFSET_FOR_KEYBOARD1;
            rect.size.height += kOFFSET_FOR_KEYBOARD1;
        }
       
    }
    else
    {
         if ([UIScreen mainScreen].bounds.size.height == 568) {
             rect.origin.y += kOFFSET_FOR_KEYBOARD;
             rect.size.height -= kOFFSET_FOR_KEYBOARD;
         }else{
             rect.origin.y += kOFFSET_FOR_KEYBOARD1;
             rect.size.height -= kOFFSET_FOR_KEYBOARD1;
         }
        // revert back to the normal state.
        
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)keyboardWillShow {
    keyboardVisible = true;
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    keyboardVisible = false;
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


- (IBAction)btn_confirmPress:(id)sender {
}
@end
