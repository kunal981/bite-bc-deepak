//
//  CheckinViewController.h
//  BiteBC
//
//  Created by brst on 7/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckinViewController : UIViewController<NSURLConnectionDataDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
    
    
    NSURLConnection *connection;
    
    NSMutableData *data;
    
    NSMutableDictionary *dataDictionary;
    
    NSString *daysleft,*packName,*plan_id;
    
    UIButton * checkinBtn;
    UITextField *txtTotalbill;
    UITextField *txtBiteDiscount;
    UITextField *txtRestaurantCode;
    UIScrollView *scrollBack;
    UIToolbar *toolBar;
    UIToolbar *toolBar1;
    UIToolbar *toolBar2;
    BOOL keyboardVisible;

}

@property(nonatomic,strong)NSString *restid;
@property(nonatomic,strong)NSString * expiry_Date;
@property(nonatomic,strong)NSString *hotelNAME;
@property(nonatomic,strong)NSString *profileImage_String;
@property (strong, nonatomic) IBOutlet UIImageView *imgView_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UILabel *lb_memberID;
@property (strong, nonatomic) IBOutlet UILabel *lbl_monthyMembership;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dayleft;
- (IBAction)btn_confirmPress:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *textField_totalBill;
@property (strong, nonatomic) IBOutlet UITextField *textField_biteDiscount;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@end
