//
//  RestaurantTableViewController.m
//  BiteBC
//
//  Created by brst on 7/4/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "RestaurantTableViewController.h"
#import "DetailOfRestaurantViewController.h"
#import "MBProgressHUD.h"
#import "HelperViewController.h"
#import "AppDelegate.h"
#import "FilterViewController.h"
#import "ASStarRatingView.h"
#import "UIImageView+WebCache.h"
#import "DataClass.h"


#define NAME_TAG 1
#define IMAGE_TAG 2
#define FEATURE_TAG 3

@interface RestaurantTableViewController ()
{
    NSMutableDictionary *detail;
    NSMutableDictionary *restaurantDetail;
    NSMutableDictionary *cacheImages,*cachedImages;
    UIView *back_view;
   }

@end

@implementation RestaurantTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
   
    NSString *city = [[NSUserDefaults standardUserDefaults]valueForKey:@"city"];
    if (city == nil || city.length == 0) {
         self.navigationItem.title=@"Restaurant Listing";
    }
    else{
        self.navigationItem.title = city;
    }
 
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:17],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
 
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"filter"]isEqualToString:@"true"])
    {
        [self postData_Filter];
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"Refresh"]) {
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"Refresh"];
        
        [self refreshTask];
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
    [[NSUserDefaults standardUserDefaults]synchronize];
//    [back_view removeFromSuperview];
//    [blurView removeFromSuperview];

}
- (void)viewDidLoad
{
   
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"filter"]);
    
    [super viewDidLoad];
    
    NSString *city = [[NSUserDefaults standardUserDefaults]valueForKey:@"city"];
    if (city == nil || city.length == 0) {
        cityArray = [[NSMutableArray alloc]init];
        city_idArray = [[NSMutableArray alloc]init];
        [self getCities];
       // self.view.userInteractionEnabled = NO;
              back_view = [[UIView alloc]init];
        back_view.frame =[self.tableView bounds];
        
        self.navigationController.navigationBar.userInteractionEnabled =  NO;
        self.tabBarController.tabBar.hidden  = YES;
        
     
       // self.tableView.scrollEnabled = NO;
        
        back_view.backgroundColor = [UIColor blackColor];
        back_view.alpha = 0.7;
       
        [self.tableView addSubview:back_view];
        blurView = [[UIView alloc]init];
        blurView.backgroundColor = [UIColor whiteColor];
        blurView.layer.cornerRadius = 7.0;
        blurView.layer.borderColor = [UIColor blackColor].CGColor;
        
        blurView.frame = CGRectMake(20, self.view.frame.size.height/2 - 150, self.view.frame.size.width-40, 200);
        [self.tableView addSubview:blurView];
        
        lblAlert = [[UILabel alloc]initWithFrame:CGRectMake(0,  20, blurView.frame.size.width, 22)];
        lblAlert.textColor = [UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        lblAlert.font = [UIFont boldSystemFontOfSize:16];
        lblAlert.textAlignment = NSTextAlignmentCenter;
        lblAlert.text = @"Select Your City Below.";
        
        [blurView addSubview:lblAlert];
        
        txtCity = [[UITextField alloc]initWithFrame:CGRectMake(10, lblAlert.frame.origin.y + lblAlert.frame.size.height +30, blurView.frame.size.width-20, 30)];
        txtCity.delegate =self;
      
        
        txtCity.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Select city"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:14]
                                                     }
         ];
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.frame =  CGRectMake(blurView.frame.size.width-30, txtCity.frame.origin.y+10, 12, 12);
        
        imgView.image = [UIImage imageNamed:@"small.png"];
        
        
        [blurView addSubview:imgView];
        txtCity.layer.borderColor=[UIColor darkGrayColor].CGColor;
       // txtCity.layer.borderWidth = 0.90;
        
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        txtCity.leftView = paddingView2;
        txtCity.leftViewMode = UITextFieldViewModeAlways;
        txtCity.borderStyle = UITextBorderStyleLine;
        [blurView addSubview:txtCity];
        
           UIButton *submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
            [submit_Button setFrame:CGRectMake(10,txtCity.frame.origin.y+txtCity.frame.size.height+20,blurView.frame.size.width-20,35)];
            submit_Button.layer.borderWidth=1.0;
            submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
            [submit_Button addTarget:self action:@selector(submit_city) forControlEvents:UIControlEventTouchUpInside];
            [submit_Button setTitle:@"SUBMIT" forState:UIControlStateNormal];
            submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:17];
            [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
            [blurView addSubview:submit_Button];
       
        
        
    }
   
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    self.tableView.separatorColor=[UIColor clearColor];
//    hotelList=[[NSMutableArray alloc]initWithObjects:@"GBK-MANCHESTER",@"Aspirine-3 Star",@"The Oberoi",@"The Taj Mahal Palace",@"Amanbagh Resort",@"The Leela Palace Kempinski ",@"Sarvpriya",@"The Imperial",@"GBK-MANCHESTER",@" Taj Falaknuma Palace",@"The 4 Seasons", nil];
    discountList=[[NSMutableArray alloc]initWithObjects:@"50% off food bill",@"50% off food bill",@"2 for 1 meals",@"50% off food bill",@"50% off food bill",@"2 for 1 meals",@"2 for 1 meals", nil];
    photoArray=[[NSMutableArray alloc]initWithObjects:@"hotel1.jpeg",@"hyd.jpg",@"hotel3.jpeg",@"hotel4.jpeg",@"hotel5.jpeg",@"hotel6.jpeg",@"hotel6.jpeg", nil];
    
    restaurantDetail=[[NSMutableDictionary alloc]init];
    detail=[[NSMutableDictionary alloc]init];
    filterButton=[UIButton buttonWithType: UIButtonTypeCustom];
    [filterButton setTitle:@"FILTER" forState:UIControlStateNormal];
    [filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    filterButton.backgroundColor=[UIColor colorWithRed:0.8980 green:0.9059 blue:0.9176 alpha:1.0];
    filterButton.frame=CGRectMake(140, 5,60,20);
    filterButton.layer.cornerRadius=4;
    filterButton.layer.borderWidth=1.0;
    filterButton.layer.borderColor=[UIColor whiteColor].CGColor;
    filterButton.titleLabel.font=[UIFont boldSystemFontOfSize:13];
    [filterButton addTarget:self action:@selector(filterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
   
    barButton=[[UIBarButtonItem alloc]initWithCustomView:filterButton];
    self.navigationItem.rightBarButtonItem=barButton;
 
    // UIBARBUTTON HERE
    refreshButton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshTask)];
    [refreshButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem=refreshButton;

    [self fetchData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}
-(void)refreshTask
{
    checkCity = 0;
    
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self fetchData];
}

-(void)postData_Filter
{
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cuisine"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"noofpeople"]);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"availble"]);
    
   [self showLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    NSString *post = [NSString stringWithFormat:@"cuisine_types[]=%@&offer_type[]=%@&people[]=%@&availability[]=%@&customerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cuisine"],[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype"],[[NSUserDefaults standardUserDefaults]valueForKey:@"noofpeople"],[[NSUserDefaults standardUserDefaults]valueForKey:@"availble"],[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@filter.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}

-(void)fetchData
{
    
    [self showLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    NSString *post = [NSString stringWithFormat:@"customerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@products.php?",post_url];
    NSLog(@"%@",urlString);
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
//    [self showLoadingView];
//   
//    NSString* strURL=[NSString stringWithFormat:@"%@products.php",post_url];
//    NSURL *url3=[NSURL URLWithString:strURL];
//    NSURLRequest *request=[NSURLRequest requestWithURL:url3];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
//    connection=[[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    
    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
  
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dict);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
    
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
   
    {
        if ([checkCity isEqualToString:@"1"]) {
            [back_view removeFromSuperview];
            [blurView removeFromSuperview];
           
           
           NSString *msg=[NSString stringWithFormat:@"%@",[dict valueForKey:@"message"]];
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self refreshTask];
 
        }
        dataArray=[[NSMutableArray alloc]init];
        arrayOfAllData=[[NSMutableArray alloc]init];
        dataArray =[dict valueForKey:@"products"];
        
        
        for (int i=0; i<dataArray.count; i++)
        {
            
            restaurantDetail=[dataArray objectAtIndex:i];
            
            detail=[restaurantDetail valueForKey:@"detail"];
            DataClass* globalData=[DataClass new];
            
            
            globalData.hotelName= [[detail valueForKey:@"name"]uppercaseString];
            globalData.hotelCuisine=[detail valueForKey:@"cuisine"];
              globalData.hotelPhoto= [detail valueForKey:@"image_url"];
            globalData.hotel_id= [detail valueForKey:@"entity_id"];
             globalData.hotelAdress= [detail valueForKey:@"address"];
            globalData.offerType=[detail valueForKey:@"offer_available"];
            globalData.weekly_Str=[detail valueForKey:@"weekly"];
            globalData.monthly_Str=[detail valueForKey:@"monthly"];
         
            globalData.hotelrating= [NSString stringWithFormat:@"%@",[restaurantDetail valueForKey:@"rating"]];

            NSLog(@"%@",globalData.weekly_Str);
            NSLog(@"%@",globalData.monthly_Str);
           [arrayOfAllData addObject:globalData];
            globalData=nil;
            
            
        }
        [self imageDownloader2];
        
    }
    else
    {
        checkCity = 0;
        
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"message"] message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        arrayOfAllData=nil;
       
    }
    [self.tableView reloadData];
 
    [self hideLoadingView];
    
    }

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    return arrayOfAllData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellidentifier=nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
      NSString *identifier = [NSString stringWithFormat:@"Cell%d",(int)indexPath.row];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        cell.backgroundColor=[UIColor clearColor];
        
        cellView=[[UIView alloc]initWithFrame:CGRectMake(0,2, 320,78)];
        cellView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
        cellView.tag=indexPath.row;
        [cell.contentView addSubview:cellView];

        profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10, 58,58)];
        profileImageView.tag = IMAGE_TAG;
        profileImageView.layer.cornerRadius=8;
        profileImageView.layer.masksToBounds=YES;
        profileImageView.backgroundColor=[UIColor lightGrayColor];
        profileImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:profileImageView];
        
        
        discountImage = [[UIImageView alloc] initWithFrame:CGRectMake(248,23,70,45)];
        discountImage.tag = IMAGE_TAG;
        //discountImage.layer.cornerRadius=8;
        discountImage.layer.masksToBounds=YES;
        discountImage.backgroundColor=[UIColor clearColor];
        discountImage.image=[UIImage imageNamed:@"listing.png"];
        discountImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:discountImage];
        
        disAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(26,6,40,15)];
        disAmountLabel.tag = NAME_TAG;
        disAmountLabel.font = [UIFont boldSystemFontOfSize:18];
        disAmountLabel.backgroundColor=[UIColor clearColor];
        disAmountLabel.textColor = [UIColor whiteColor];
       
        disAmountLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [discountImage addSubview:disAmountLabel];

        
        savedLabel = [[UILabel alloc] initWithFrame:CGRectMake(9,5,57,35)];
        savedLabel.tag = NAME_TAG;
        savedLabel.font = [UIFont boldSystemFontOfSize:13];
        savedLabel.numberOfLines=2;
        savedLabel.textAlignment=NSTextAlignmentCenter;
        savedLabel.backgroundColor=[UIColor clearColor];
        savedLabel.textColor = [UIColor whiteColor];
        //savedLabel.text=@"50% off food bill";
        savedLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [discountImage addSubview:savedLabel];

        
        hotelName_Label = [[UILabel alloc] initWithFrame:CGRectMake(70,14,170,20)];
        hotelName_Label.tag = NAME_TAG;
        hotelName_Label.font = [UIFont boldSystemFontOfSize:15];
        hotelName_Label.backgroundColor=[UIColor clearColor];
        hotelName_Label.textColor = [UIColor darkGrayColor];
        hotelName_Label.adjustsFontSizeToFitWidth=YES;
        hotelName_Label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:hotelName_Label];
        
        cusine_label = [[UILabel alloc] initWithFrame:CGRectMake(70,35,170,30)];
        cusine_label.tag = FEATURE_TAG;
         cusine_label.adjustsFontSizeToFitWidth=YES;
        cusine_label.numberOfLines=2;
        cusine_label.font = [UIFont systemFontOfSize:12];
        cusine_label.backgroundColor=[UIColor clearColor];
        cusine_label.textColor = [UIColor darkGrayColor];
        cusine_label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [cellView addSubview:cusine_label];
        
        
      
        staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(245,0,80, 27)];
        staticStarRatingView.canEdit = NO;
        staticStarRatingView.maxRating = 5;
        [cellView addSubview:staticStarRatingView];
    
     }
    
    DataClass* allData;
    allData = [arrayOfAllData objectAtIndex:indexPath.row];

    
    hotelName_Label.text=allData.hotelName;
    //profileImageView.image=[UIImage imageNamed:[photoArray objectAtIndex:indexPath.row]];
   
    if([cacheImages objectForKey:identifier] != nil){
        profileImageView.image = [cacheImages valueForKey:identifier];
        
    }
    else if ([allData.hotelPhoto isEqual:[NSNull null]])
        profileImageView.image=[UIImage imageNamed:@" "];
    else
   // [profileImageView setImageWithURL:[NSURL URLWithString:allData.hotelPhoto] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        [profileImageView sd_setImageWithURL:[NSURL URLWithString:allData.hotelPhoto] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
//        
//    [self imageDownloader:allData.hotelPhoto imageView:profileImageView];
//    [cacheImages setValue:profileImageView.image forKey:identifier];
    
   // profileImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:allData.hotelPhoto]]];
 
    
    
    if ([allData.offerType isEqual:[NSNull null]])
    {
        savedLabel.text=@"    N.A";
    }
    else
    {
        
        NSLog(@"%@",allData.offerType);
        if ([allData.offerType isEqualToString:@"31"])
        {
            savedLabel.text=@"2 for 1 meals";
        }
        else if([allData.offerType isEqualToString:@"32"])
        {
             savedLabel.text=@"50% off food bill";
        }
        else if([allData.offerType isEqualToString:@"100"])
        {
            savedLabel.text=@"2 for 1";
        }
        else if([allData.offerType isEqualToString:@"101"])
        {
            savedLabel.text=@"50% off";
        }
        else if([allData.offerType isEqualToString:@"102"])
        {
            savedLabel.text=@"40% off";
        }
        else if([allData.offerType isEqualToString:@"103"])
        {
            savedLabel.text=@"30% off ";
        }
        
        else if([allData.offerType isEqualToString:@"104"])
        {
            savedLabel.text=@"40% off food bill ";
        }
        else if([allData.offerType isEqualToString:@"105"])
        {
            savedLabel.text=@"30% off food bill ";
        }
        


        
        
    }
   

    if ([allData.hotelCuisine isEqual:[NSNull null]])
    {
        cusine_label.text=@"";

    }
    else
    {
    cusine_label.text=allData.hotelCuisine;
    }

    id value=allData.hotelrating;
    int rate=[value intValue];
    staticStarRatingView.rating =rate;
   cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
        
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
   // UIView *v=[[cell.contentView subviews]objectAtIndex:0];
   // UIImageView *m=(UIImageView *)[[v subviews]objectAtIndex:0];
   
    DataClass *detailData=[arrayOfAllData objectAtIndex:indexPath.row];
    DetailOfRestaurantViewController* detailOfRes=[[DetailOfRestaurantViewController alloc]init];
    detailOfRes.nameOfpRestaureant_String=detailData.hotelName;
     if ([detailData.hotelAdress isEqual:[NSNull null]])
     {
         
     }
    else
    {
        detailOfRes.adressOfRestaurant_String=detailData.hotelAdress;
    }
    detailOfRes.imageOfHotel_string=detailData.hotelPhoto;
   // detailOfRes.imageOf_hotel=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:detailData.hotelPhoto]]];
   //detailOfRes.imageOfHotel_string=detailData.hotelPhoto;
    detailOfRes.rating_hotel=detailData.hotelrating;
    detailOfRes.product_id=detailData.hotel_id;
//    detailOfRes.weekly_str=detailData.weekly_Str;
//    detailOfRes.monthly_str=detailData.monthly_Str;
    
    if ([detailData.hotelCuisine isEqual:[NSNull null]])
    {
        
    }
    else
    {
         detailOfRes.cuisine_String=detailData.hotelCuisine;
    }
   
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController pushViewController:detailOfRes animated:YES];
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath   *)indexPath
{
    return 80;
}


-(void)filterButtonPressed
{
//    HelperViewController* filterView=[[HelperViewController alloc]init];
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    FilterViewController* filterView=[[FilterViewController alloc]init];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
   
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"offertype"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"noofpeople"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"cuisine"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"availble"];
    [[NSUserDefaults standardUserDefaults]synchronize];
 
   [self.navigationController pushViewController:filterView animated:YES];
    
    
}
#pragma mark - ImageDownloader Method
-(void)imageDownloader:(NSString *)urlStr imageView:(UIImageView *)imageView
{
    dispatch_queue_t imageQueue=dispatch_queue_create("imageDownloader", nil);
    dispatch_async(imageQueue, ^{
        NSURL *imageurl=[NSURL URLWithString:urlStr];
        NSData *imageData=[NSData dataWithContentsOfURL:imageurl];
        UIImage *image=[UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            imageView.image=image;
        });
        
    });
}

-(void)imageDownloader2
{
    
    DataClass* imageData;
    for(int i=0;i<arrayOfAllData.count;i++)
    {
        imageData=[arrayOfAllData objectAtIndex:i];
        NSLog(@"%@",imageData.hotelPhoto);
        
        NSString *identifier = [NSString stringWithFormat:@"Cell%d",
                                i];
        id st=imageData.hotelPhoto;
        if(![st isEqual:[NSNull null]])
        {
            dispatch_queue_t imageQueue=dispatch_queue_create("imageDownloader", nil);
            dispatch_async(imageQueue, ^{
                NSURL *imageurl=[NSURL URLWithString:imageData.hotelPhoto];
                NSData *imageData=[NSData dataWithContentsOfURL:imageurl];
                UIImage *image=[UIImage imageWithData:imageData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cacheImages setValue:image forKey:identifier];
                    // imageView.image=image;
                });

            });

        }
        else{
            UIImage *image=[UIImage imageNamed: @"default.png"];
            [cacheImages setValue:image forKey:identifier];

        }
    }

    [self.tableView reloadData];
    NSLog(@"cache count=%lu",(unsigned long)cachedImages.count);

}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"string entered=%@",txtCity.text);
    
    
}



#pragma mark - UITextField Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == txtCity) {
        [textField resignFirstResponder];
        CGRect viewFrame = self.view.frame;
              
        toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0,blurView.frame.origin.y+blurView.frame.size.height-90,viewFrame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [self.view addSubview:toolBarForPeriodPicker];
        
        pickerViewData = [[UIPickerView alloc]init];
        pickerViewData.backgroundColor=[UIColor whiteColor];
        pickerViewData.dataSource=self;
        pickerViewData.delegate=self;
        
        
        CGFloat pickerHeight = pickerViewData.frame.size.height;  // assume you have an outlet called picker
        CGRect pickerFrame = CGRectMake(0.0, blurView.frame.origin.y+blurView.frame.size.height-90+35,
                                        viewFrame.size.width, pickerHeight);
        pickerViewData.frame = pickerFrame;
        
        [self.view addSubview:pickerViewData];

    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelBtnPressedPeriod
{
    
    
    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
    
}
-(void)doneBtnPressedPeriod
{
    //  [self postData];
    
    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
    
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [cityArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return cityArray.count;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (row == 0) {
        txtCity.text =@"";
    }else{
    txtCity.text = [cityArray objectAtIndex:row];
    
    cityID = [city_idArray objectAtIndex:row];
    }
    
}

-(void)getCities
{
    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/city.php"];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSLog(@"About to send req %@",req.URL);
    NSData *dataCity = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if (dataCity ==nil|| dataCity==(id)[NSNull null]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        NSArray *result = [NSJSONSerialization JSONObjectWithData:dataCity options:kNilOptions error:&error];
        NSLog(@"%@",result);
        cityName = @"Select city";
        cityid = @" ";
        [city_idArray addObject:cityid];
        [cityArray addObject:cityName];
        
        for (int i =0 ; i<result.count; i++) {
            
            cityName = [[result objectAtIndex:i] valueForKey:@"name"];
            
            [cityArray addObject:cityName];
            cityid = [[result objectAtIndex:i] valueForKey:@"id"];
            [city_idArray addObject:cityid];
        }
        
        NSLog(@"%@",cityArray);
        
        
    }
    
}

#pragma mark UIButton Action

-(void)submit_city{
    
    if (txtCity.text.length ==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please select city. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
         [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"SelctedCity"];
    
    [back_view removeFromSuperview];
    [blurView removeFromSuperview];
        
        self.navigationController.navigationBar.userInteractionEnabled =  YES;
        self.tabBarController.tabBar.hidden  = NO;
    
    checkCity = @"1";
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"customerid=%@&city=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],txtCity.text];
    
    [[NSUserDefaults standardUserDefaults]setValue:cityID forKey:@"CITYID"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [[NSUserDefaults standardUserDefaults]setValue:txtCity.text forKey:@"city"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    self.navigationItem.title = txtCity.text;
   
    NSLog(@"%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@customercity.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    }

}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
   
    back_view.frame = self.tableView.bounds;
}

@end
