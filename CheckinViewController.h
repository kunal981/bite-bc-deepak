//
//  CheckinViewController.h
//  BiteBC
//
//  Created by brst on 7/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckinViewController : UIViewController<NSURLConnectionDataDelegate,UIAlertViewDelegate>
{
    
    
    NSURLConnection *connection;
    
    NSMutableData *data;
    
    NSMutableDictionary *dataDictionary;
    
    NSString *daysleft,*packName,*plan_id;
    
    UIButton * checkinBtn;
    
        

}

@property(nonatomic,strong)NSString *restid;
@property(nonatomic,strong)NSString * expiry_Date;
@property(nonatomic,strong)NSString *hotelNAME;
@property(nonatomic,strong)NSString *profileImage_String;


@end
