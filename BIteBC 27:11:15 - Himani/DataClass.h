//
//  DataClass.h
//  BiteBC
//
//  Created by brst on 7/16/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataClass : NSObject

@property (nonatomic, strong) NSString *hotelName; // name of recipe
@property (nonatomic, strong) NSString *hotel_id;
@property (nonatomic, strong) NSString *hotelAdress; // preparation time
@property (nonatomic, strong) NSString *hotelPhoto; // preparation time
@property (nonatomic, strong) NSString *offerType;
@property (nonatomic, strong) NSString *hotelCuisine; // image filename of recipe
@property (nonatomic, strong) NSString *hotelrating;
@property (nonatomic, strong) NSString *latitude_Str;
@property (nonatomic, strong) NSString *longitude_Str;
@property (nonatomic, strong) NSString *weekly_Str;
@property (nonatomic, strong) NSString *monthly_Str;

@property (nonatomic, strong) NSArray *arrayForward; // ingredients of recipe




 //FOR SELECT PALN
@property (nonatomic, strong) NSString *nameOfPlan; // name of recipe
@property (nonatomic, strong) NSString *idOfPlan;
@property (nonatomic, strong) NSString *priceOfPlan; // preparation time
@property (nonatomic, strong) NSString *subTotalofPlan; // preparation time
@property (nonatomic, strong) NSString *timeOfPlan;

@end
