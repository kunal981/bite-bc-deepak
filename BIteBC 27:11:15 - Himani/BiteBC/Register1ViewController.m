//
//  LoginViewController.m
//  BiteBC
//
//  Created by brst on 10/16/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "Register1ViewController.h"
#import "LoginViewController.h"
#import "TwillioIntegrationViewController.h"
#define sms_url2 @"https://api.twilio.com/2010-04-01/Accounts/ACf656d70460e43f77cbbaa9bfaf535875/Messages.json"
//#define sms_url2 @"https://api.twilio.com/2010-04-01/Accounts/AC63d253662d2f1c2c8e496a5d5709ac71/Messages.json"

#import "AFHTTPRequestOperationManager.h"
@interface Register1ViewController ()
{
    UIImageView * profileImage;
    UILabel * uploadLbl;
    UITextField *currentTextfield;
}

@end

@implementation Register1ViewController


-(void)viewWillAppear:(BOOL)animated
{
//     uploadLbl.hidden=NO;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Register";
         //self.navigationController.navigationBarHidden=NO;
        
}

-(void)navBarView
{
    UIView * navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]];
    [self.view addSubview:navView];
    
    UILabel * tittleLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 64)];
    tittleLbl.text=@"Register";
    tittleLbl.textAlignment=NSTextAlignmentCenter;
    tittleLbl.textColor=[UIColor whiteColor];
    tittleLbl.font=[UIFont fontWithName:@"Helvetica Neue" size:19];
    [navView addSubview:tittleLbl];
    
    UIButton * doneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame=CGRectMake(5,30 ,50,25);
    [doneBtn setImage:[UIImage imageNamed:@"bck.png"] forState:UIControlStateNormal];
    //[doneBtn setTitle:@"Back" forState:UIControlStateNormal];
    //[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //doneBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15.5];
    [doneBtn addTarget:self action:@selector(back_Btn_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:doneBtn];

    
}
-(void)back_Btn_Pressed
{
   [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    [self navBarView];
    
    
    cityArray  =   [[NSMutableArray alloc]init];
    cityIDArray =  [[NSMutableArray alloc]init];
    
    //************ LOGIN VIEW ****************//
   
       [self loginView];
   
    
       
    pickerimage=[[UIImagePickerController alloc]init];
    pickerimage.delegate=self;
    pickerimage.allowsEditing=YES;
    [self getCities];

    // Do any additional setup after loading the view.
}
#pragma mark - LoginButton Method
 - (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You don't have a camera for this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [noCameraAlert show];
        }
        else
        {
            pickerimage.sourceType=UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:pickerimage animated:YES completion:nil];
        }
    }
    else if (buttonIndex==1)
    {         pickerimage.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:pickerimage animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
 
    
    //profilePhotoView.image=[info objectForKey:UIImagePickerControllerOriginalImage];
    img22=[info objectForKey:UIImagePickerControllerEditedImage];
    NSLog(@"%@",img22);
    CGFloat W =  img22.size.width;
    CGFloat H = img22.size.height;
    
    CGFloat width = W -90;
     CGFloat height = H -90;
   
   
    img22 = [self resizeImage:img22 newSize:CGSizeMake(W-width, H - height)];
    //[[NSUserDefaults standardUserDefaults]setObject:im forKey:"profile_pic"];
    //[[NSUserDefaults standardUserDefaults]objectForKey:@"profile_pic"];
    
    NSLog(@"%@",img22);
   
       profileImage.image=img22;

       base64String=[self encodeToBase64String:img22];
    
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height {
    NSLog(@"resizing");
    CGImageRef imageRef = [image CGImage];
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
    
    //if (alphaInfo == kCGImageAlphaNone)
    alphaInfo = kCGImageAlphaNoneSkipLast;
    
    CGContextRef bitmap = CGBitmapContextCreate(NULL, width, height, CGImageGetBitsPerComponent(imageRef),
                                                4 * width, CGImageGetColorSpace(imageRef), alphaInfo);
    CGContextDrawImage(bitmap, CGRectMake(0, 0, width, height), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage *result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return result;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    //DO WHATEVER WHEN YOU PRESS CANCEL BUTTON (PROBABLY DISMISSING THE POPOVER
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(5, 0, size.width,size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
- (NSString *)encodeToBase64String:(UIImage *)image
{
    NSData *data2 = UIImagePNGRepresentation(image);
    
    return base64String=[data2 base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
 }

-(void)profilePictureTap
{
    
     UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:@"Choose Option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery" ,nil];
    
    [sheet showInView:self.view];
    
}

#pragma mark - LoginButton Method
-(void)loginButtonPressed
{
   
     NSString* trimes = [self.txtPswrd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.txtPswrd.text=trimes;
    if (self.txtFirstname.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter first name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
     }
    else if(self.txtLastName.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter last name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if(self.txtEmail.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if(![self NSStringIsValidEmail:self.txtEmail.text]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if(self.txtConfirm_Email.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter confirm email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if(!([self.txtEmail.text caseInsensitiveCompare:self.txtConfirm_Email.text]==NSOrderedSame) )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Email does not match the confirm email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if(self.txt_Cell_Number.text.length<10)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid cell number " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }

    else if(self.txtPswrd.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }else if(self.txtPswrd.text.length<6)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter minimum six character's" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if(self.txtConfirmPswrd.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter confirm password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    else if(![self.txtPswrd.text isEqualToString:self.txtConfirmPswrd.text])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Password not matched" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }else if(base64String.length==0 || [base64String isEqualToString:@"nil"] || [base64String isEqual:(id)[NSNull null]])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please upload the image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

        
    }
    else if(self.txtSelectCity.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please select city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:self.txtEmail.text forKey:@"mail"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:self.txtFirstname.text forKey:@"first"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:self.txtLastName.text forKey:@"last"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:self.txtPswrd.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:self.txt_Cell_Number.text forKey:@"cell_number"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setObject:self.txtSelectCity.text forKey:@"city"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"]);
 
         [self generateRandomCode];
        
    }

    
    
//     TwillioIntegrationViewController * twilioVC=[[TwillioIntegrationViewController alloc]init];
//    twilioVC.profileImage=img22;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    [self.navigationController pushViewController:twilioVC animated:YES];
   
}
 -(void)postData
{
    NSLog(@"email =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
    NSLog(@"first name is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
    NSLog(@"password is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]);
    NSLog(@"lastname is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
    
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"uuid = %@",uid);

    //base64String=[base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];

    NSLog(@"%@",base64String);
    [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"base64"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
    [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&profileimg=%@&platform=%@&device_id=%@&cell_number=%@&section=%@&city=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"],[[NSUserDefaults standardUserDefaults]valueForKey:@"last"],[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"],base64String,@"ios",uid,self.txt_Cell_Number.text,@"email",self.txtSelectCity.text];
    NSLog(@"%@",post);
    NSLog(@"%@",self.txtSelectCity.text);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@registeration.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
 
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=  \n%@",dict);
    
     NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
//     NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
//    
//    NSString * imageis =[NSString stringWithFormat:@"%@",[dict valueForKey:@"profileimg_url"]];
     if  ([sucess isEqualToString:@"1"])
  
    {
        NSString * thnxMessage=[NSString stringWithFormat:@"Please check your email and click on the link to activate your account in the message we just sent to %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]];
     
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thanks For Signing Up!" message:thnxMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];

        [self back_Btn_Pressed];
        
       }
        else   {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
       [self hideLoadingView];
             
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
   [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

-(void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
  
    if (textField.tag==0)
    {
        [self.txtLastName becomeFirstResponder];
    }
    else if(textField.tag==1)
    {
        [self.txtEmail becomeFirstResponder];
    }else if(textField.tag==2)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-40;
        [self.view setFrame:frame1];

        [self.txtConfirm_Email becomeFirstResponder];
    }
    else if(textField.tag==3)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-90;
        [self.view setFrame:frame1];

        [self.txt_Cell_Number becomeFirstResponder];
    } else if(textField.tag==4)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-120;
        [self.view setFrame:frame1];

        [self.txtPswrd becomeFirstResponder];
    }else if(textField.tag==5)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-140;
        [self.view setFrame:frame1];

        [self.txtConfirmPswrd becomeFirstResponder];
        

    }
    else if(textField.tag==6)
    {
       
        CGRect frame1=self.view.frame;
        frame1.origin.y=0;
        [self.view setFrame:frame1];
        
        [textField resignFirstResponder];
        
    }
    else
    {
       
    }




return YES;
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.txtEmail resignFirstResponder];
//    [self.txtConfirm_Email resignFirstResponder];
//    [self.txtFirstname resignFirstResponder];
//     [self.txtPswrd resignFirstResponder];
//
//    [self.txtLastName resignFirstResponder];
//    [self.txtConfirmPswrd resignFirstResponder];
//    [self.txt_Cell_Number resignFirstResponder];
//
//}

-(void)getCities
{
    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/city.php"];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSLog(@"About to send req %@",req.URL);
    NSData *dataCity = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if (dataCity ==nil|| dataCity==(id)[NSNull null]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        NSArray *result = [NSJSONSerialization JSONObjectWithData:dataCity options:kNilOptions error:&error];
        NSLog(@"%@",result);
        cityName = @"Select city";
        cityId = @" ";
        [cityArray addObject:cityName];
        [cityIDArray addObject:cityId];
        for (int i =0 ; i<result.count; i++) {
            
            cityName = [[result objectAtIndex:i] valueForKey:@"name"];
            cityId = [[result objectAtIndex:i] valueForKey:@"id"];
            [cityArray addObject:cityName];
            [cityIDArray addObject:cityId];
            
        }
        
        NSLog(@"%@",cityArray);
        
        
    }
    
}



-(void)loginView
{
    
   UIScrollView *scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0,64, self.view.frame.size.width, self.view.frame.size.height-45)];
   scrollView.backgroundColor=[UIColor clearColor];
    scrollView.userInteractionEnabled=YES;
    scrollView.scrollEnabled=YES;
    [self.view addSubview:scrollView];
    int height;
    if (!IS_IPHONE_5)
    {
           height=10;
    }else
    {
         height=15;
    }
    
    if ([UIScreen  mainScreen].bounds.size.height == 480) {
        
        profileImage=[[UIImageView alloc]initWithFrame:CGRectMake(12, 4, 90, 90)];
        profileImage.backgroundColor=[UIColor clearColor];
        profileImage.image=[UIImage imageNamed:@"pup.png"];
        profileImage.layer.masksToBounds=YES;
        profileImage.layer.borderColor=[UIColor whiteColor].CGColor;
        profileImage.layer.borderWidth=1.0;
        profileImage.layer.cornerRadius=5.0;
        profileImage.contentMode = UIViewContentModeScaleAspectFit;
        [scrollView addSubview:profileImage];
        
        
        uploadLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, profileImage.frame.size.height+profileImage.frame.origin.y+3,self.view.frame.size.width-10, 20)];
        uploadLbl.text=@"Please use an image that clearly shows your face";
        uploadLbl.textColor=[UIColor whiteColor];
        //uploadLbl.numberOfLines=2;
        uploadLbl.adjustsFontSizeToFitWidth=YES;
        uploadLbl.textAlignment=NSTextAlignmentLeft;
        uploadLbl.backgroundColor=[UIColor clearColor];
        uploadLbl.font=[UIFont boldSystemFontOfSize:12];
        uploadLbl.textAlignment=NSTextAlignmentCenter;
        [scrollView addSubview:uploadLbl];
        
        UITapGestureRecognizer *tapForPicture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePictureTap)];
        tapForPicture.numberOfTapsRequired = 1;
        tapForPicture.numberOfTouchesRequired=1;
        profileImage.userInteractionEnabled=YES;
        [profileImage addGestureRecognizer:tapForPicture];
        
        
        
        
        self.txtFirstname=[[UITextField alloc]initWithFrame:CGRectMake(profileImage.frame.size.width+20,4, 200, 35)];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        self.txtFirstname.leftView = paddingView;
        self.txtFirstname.leftViewMode = UITextFieldViewModeAlways;
        self.txtFirstname.layer.cornerRadius=6;
        self.txtFirstname.borderStyle=UITextBorderStyleNone;
        self.txtFirstname.delegate=self;
        self.txtFirstname.tag=0;
        self.txtFirstname.returnKeyType=UIReturnKeyNext;
        self.txtFirstname.layer.borderWidth=1.0;
        self.txtFirstname.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.txtFirstname.backgroundColor=[UIColor whiteColor];
        self.txtFirstname.textAlignment=NSTextAlignmentNatural;
        self.txtFirstname.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtFirstname.keyboardType=UIKeyboardTypeEmailAddress;
        self.txtFirstname.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"First Name"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        [scrollView addSubview:self.txtFirstname];
        
        self.txtLastName=[[UITextField alloc]initWithFrame:CGRectMake(profileImage.frame.size.width+20,  self.txtFirstname.frame.origin.y + self.txtFirstname.frame.size.height+height, 200, 35)];
        UIView *viewPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtLastName.leftView = viewPadding;
        self.txtLastName.tag=1;
        self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
        self.txtLastName.layer.cornerRadius=6;
        self.txtLastName.borderStyle=UITextBorderStyleNone;
        self.txtLastName.backgroundColor=[UIColor whiteColor];
        self.txtLastName.returnKeyType=UIReturnKeyNext;
        self.txtLastName.layer.borderWidth=1.0;
        self.txtLastName.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtLastName.textAlignment=NSTextAlignmentLeft;
        self.txtLastName.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtLastName.delegate=self;
        self.txtLastName.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Last Name"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        [scrollView addSubview:self.txtLastName];
        
        
        self.txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(12,  uploadLbl.frame.origin.y + uploadLbl.frame.size.height+height, self.view.frame.size.width-24, 35)];
        UIView *viewPadding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtEmail.leftView = viewPadding2;
        self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
        self.txtEmail.layer.cornerRadius=6;
        self.txtEmail.tag=2;
        self.txtEmail.returnKeyType=UIReturnKeyNext;
        self.txtEmail.borderStyle=UITextBorderStyleNone;
        self.txtEmail.backgroundColor=[UIColor whiteColor];
        self.txtEmail.layer.borderWidth=1.0;
        self.txtEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtEmail.textAlignment=NSTextAlignmentLeft;
        self.txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtEmail.keyboardType=UIKeyboardTypeEmailAddress;
        self.txtEmail.delegate=self;
        self.txtEmail.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Email"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        [scrollView addSubview:self.txtEmail];
        
        self.txtConfirm_Email=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height+height, self.view.frame.size.width-24, 35)];
        self.txtConfirm_Email.tag=3;
        UIView *viewPadding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtConfirm_Email.leftView = viewPadding7;
        self.txtConfirm_Email.leftViewMode = UITextFieldViewModeAlways;
        self.txtConfirm_Email.layer.cornerRadius=6;
        self.txtConfirm_Email.borderStyle=UITextBorderStyleNone;
        self.txtConfirm_Email.backgroundColor=[UIColor whiteColor];
        self.txtConfirm_Email.layer.borderWidth=1.0;
        self.txtConfirm_Email.returnKeyType=UIReturnKeyNext;
        self.txtConfirm_Email.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtEmail.textAlignment=NSTextAlignmentLeft;
        self.txtConfirm_Email.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtConfirm_Email.keyboardType=UIKeyboardTypeEmailAddress;
        self.txtConfirm_Email.delegate=self;
        self.txtConfirm_Email.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Confirm Email"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        [scrollView addSubview:self.txtConfirm_Email];
        
        
        self.txt_Cell_Number=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtConfirm_Email.frame.origin.y + self.txtConfirm_Email.frame.size.height+height , self.view.frame.size.width-24, 35)];
        UIView *viewPadding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txt_Cell_Number.leftView = viewPadding6;
        self.txt_Cell_Number.leftViewMode = UITextFieldViewModeAlways;
        self.txt_Cell_Number.layer.cornerRadius=6;
        self.txt_Cell_Number.returnKeyType=UIReturnKeyNext;
        //    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        //    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        //    numberToolbar.items = [NSArray arrayWithObjects:
        //
        //                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
        //                           [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
        //                           nil];
        //    [numberToolbar sizeToFit];
        self.txt_Cell_Number.tag=4;
        //    self.txt_Cell_Number.inputAccessoryView = numberToolbar;
        self.txt_Cell_Number.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
        self.txt_Cell_Number.borderStyle=UITextBorderStyleNone;
        self.txt_Cell_Number.backgroundColor=[UIColor whiteColor];
        self.txt_Cell_Number.layer.borderWidth=1.0;
        self.txt_Cell_Number.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.txt_Cell_Number.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"612 123 1234"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        self.txt_Cell_Number.delegate=self;
        
        [scrollView addSubview:self.txt_Cell_Number];
        
        
        
        
        self.txtPswrd=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txt_Cell_Number.frame.origin.y + self.txt_Cell_Number.frame.size.height+height, self.view.frame.size.width-24, 35)];
        UIView *viewPadding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtPswrd.leftView = viewPadding3;
        self.txtPswrd.leftViewMode = UITextFieldViewModeAlways;
        self.txtPswrd.layer.cornerRadius=6;
        self.txtPswrd.tag=5;
        self.txtPswrd.returnKeyType=UIReturnKeyNext;
        self.txtPswrd.borderStyle=UITextBorderStyleNone;
        self.txtPswrd.backgroundColor=[UIColor whiteColor];
        self.txtPswrd.layer.borderWidth=1.0;
        self.txtPswrd.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtPswrd.textAlignment=NSTextAlignmentLeft;
        self.txtPswrd.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtPswrd.keyboardType=UIKeyboardTypeEmailAddress;
        self.txtPswrd.placeholder=@"Password";
        self.txtPswrd.delegate=self;
        self.txtPswrd.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Password"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        self.txtPswrd.secureTextEntry=YES;
        [scrollView addSubview:self.txtPswrd];
        
        self.txtConfirmPswrd=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtPswrd.frame.origin.y + self.txtPswrd.frame.size.height+height, self.view.frame.size.width-24, 35)];
        UIView *viewPadding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtConfirmPswrd.leftView = viewPadding4;
        self.txtConfirmPswrd.leftViewMode = UITextFieldViewModeAlways;
        self.txtConfirmPswrd.layer.cornerRadius=6;
        self.txtConfirmPswrd.tag=6;
        self.txtConfirmPswrd.returnKeyType=UIReturnKeyDefault;
        self.txtConfirmPswrd.borderStyle=UITextBorderStyleNone;
        self.txtConfirmPswrd.backgroundColor=[UIColor whiteColor];
        self.txtConfirmPswrd.layer.borderWidth=1.0;
        self.txtConfirmPswrd.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtPswrd.textAlignment=NSTextAlignmentLeft;
        self.txtConfirmPswrd.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtConfirmPswrd.keyboardType=UIKeyboardTypeEmailAddress;
        self.txtConfirmPswrd.placeholder=@"Confirm Password";
        self.txtConfirmPswrd.delegate=self;
        self.txtConfirmPswrd.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Confirm Password"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        
        self.txtConfirmPswrd.secureTextEntry=YES;
        [scrollView addSubview:self.txtConfirmPswrd];
        
        
        self.txtSelectCity=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtConfirmPswrd.frame.origin.y + self.txtConfirmPswrd.frame.size.height+height, self.view.frame.size.width-24, 35)];
        UIView *viewPadding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
        self.txtSelectCity.leftView = viewPadding5;
        self.txtSelectCity.leftViewMode = UITextFieldViewModeAlways;
        self.txtSelectCity.layer.cornerRadius=6;
        self.txtSelectCity.tag=7;
        
        self.txtSelectCity.borderStyle=UITextBorderStyleNone;
        self.txtSelectCity.backgroundColor=[UIColor whiteColor];
        self.txtSelectCity.layer.borderWidth=1.0;
        self.txtSelectCity.layer.borderColor=[UIColor lightGrayColor].CGColor;
        //self.txtEmail.textAlignment=NSTextAlignmentLeft;
        self.txtSelectCity.autocapitalizationType = UITextAutocapitalizationTypeNone;
        
        self.txtSelectCity.delegate=self;
        self.txtSelectCity.attributedPlaceholder =
        [[NSAttributedString alloc] initWithString:@"Select City"
                                        attributes:@{
                                                     NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                     }
         ];
        
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.frame =  CGRectMake(self.txtSelectCity.frame.size.width-14, self.txtSelectCity.frame.origin.y+12, 12, 12);
        
        imgView.image = [UIImage imageNamed:@"small.png"];
        
        [scrollView addSubview:self.txtSelectCity];
        [scrollView addSubview:imgView];
        
        
        
        //PLACE-HOLDER BUTTON
        loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [loginButton setFrame:CGRectMake(18,  self.txtSelectCity.frame.origin.y + self.txtSelectCity.frame.size.height+height, 290, 35)];
        loginButton.layer.borderWidth=1.0;
        loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
        [loginButton addTarget:self action:@selector(loginButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [loginButton setTitle:@"Register" forState:UIControlStateNormal];
        loginButton.titleLabel.font=[UIFont boldSystemFontOfSize:17];
        [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        loginButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];;
        [scrollView addSubview:loginButton];
        
            scrollView.contentSize=CGSizeMake(self.view.frame.size.width,700);
            
            
        

        
    }
    else{
    
    
    profileImage=[[UIImageView alloc]initWithFrame:CGRectMake(12, 68, 90, 90)];
    profileImage.backgroundColor=[UIColor redColor];
    profileImage.image=[UIImage imageNamed:@"pup.png"];
    profileImage.layer.masksToBounds=YES;
    profileImage.layer.borderColor=[UIColor whiteColor].CGColor;
    profileImage.layer.borderWidth=1.0;
    profileImage.layer.cornerRadius=5.0;
         profileImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:profileImage];
    
    uploadLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, profileImage.frame.size.height+profileImage.frame.origin.y+3,self.view.frame.size.width-10, 20)];
    uploadLbl.text=@"Please use an image that clearly shows your face";
    uploadLbl.textColor=[UIColor whiteColor];
    //uploadLbl.numberOfLines=2;
    uploadLbl.adjustsFontSizeToFitWidth=YES;
    uploadLbl.textAlignment=NSTextAlignmentLeft;
    uploadLbl.backgroundColor=[UIColor clearColor];
    uploadLbl.font=[UIFont boldSystemFontOfSize:12];
    uploadLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:uploadLbl];
    
    UITapGestureRecognizer *tapForPicture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePictureTap)];
    tapForPicture.numberOfTapsRequired = 1;
    tapForPicture.numberOfTouchesRequired=1;
    profileImage.userInteractionEnabled=YES;
    [profileImage addGestureRecognizer:tapForPicture];
    

   
    
    self.txtFirstname=[[UITextField alloc]initWithFrame:CGRectMake(profileImage.frame.size.width+20,68, 200, 35)];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtFirstname.leftView = paddingView;
    self.txtFirstname.leftViewMode = UITextFieldViewModeAlways;
    self.txtFirstname.layer.cornerRadius=6;
    self.txtFirstname.borderStyle=UITextBorderStyleNone;
    self.txtFirstname.delegate=self;
    self.txtFirstname.tag=0;
    self.txtFirstname.returnKeyType=UIReturnKeyNext;
    self.txtFirstname.layer.borderWidth=1.0;
    self.txtFirstname.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.txtFirstname.backgroundColor=[UIColor whiteColor];
    self.txtFirstname.textAlignment=NSTextAlignmentNatural;
    self.txtFirstname.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtFirstname.keyboardType=UIKeyboardTypeEmailAddress;
    self.txtFirstname.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"First Name"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    [self.view addSubview:self.txtFirstname];
    
    self.txtLastName=[[UITextField alloc]initWithFrame:CGRectMake(profileImage.frame.size.width+20,  self.txtFirstname.frame.origin.y + self.txtFirstname.frame.size.height+height, 200, 35)];
    UIView *viewPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtLastName.leftView = viewPadding;
    self.txtLastName.tag=1;
    self.txtLastName.leftViewMode = UITextFieldViewModeAlways;
    self.txtLastName.layer.cornerRadius=6;
    self.txtLastName.borderStyle=UITextBorderStyleNone;
    self.txtLastName.backgroundColor=[UIColor whiteColor];
    self.txtLastName.returnKeyType=UIReturnKeyNext;
    self.txtLastName.layer.borderWidth=1.0;
    self.txtLastName.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtLastName.textAlignment=NSTextAlignmentLeft;
    self.txtLastName.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtLastName.delegate=self;
    self.txtLastName.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Last Name"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    [self.view addSubview:self.txtLastName];

    
    self.txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(12,  uploadLbl.frame.origin.y + uploadLbl.frame.size.height+height, self.view.frame.size.width-24, 35)];
    UIView *viewPadding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtEmail.leftView = viewPadding2;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    self.txtEmail.layer.cornerRadius=6;
    self.txtEmail.tag=2;
    self.txtEmail.returnKeyType=UIReturnKeyNext;
    self.txtEmail.borderStyle=UITextBorderStyleNone;
    self.txtEmail.backgroundColor=[UIColor whiteColor];
    self.txtEmail.layer.borderWidth=1.0;
    self.txtEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtEmail.textAlignment=NSTextAlignmentLeft;
    self.txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtEmail.keyboardType=UIKeyboardTypeEmailAddress;
    self.txtEmail.delegate=self;
    self.txtEmail.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Email"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    [self.view addSubview:self.txtEmail];
    
    self.txtConfirm_Email=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height+height, self.view.frame.size.width-24, 35)];
     self.txtConfirm_Email.tag=3;
    UIView *viewPadding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtConfirm_Email.leftView = viewPadding7;
    self.txtConfirm_Email.leftViewMode = UITextFieldViewModeAlways;
    self.txtConfirm_Email.layer.cornerRadius=6;
    self.txtConfirm_Email.borderStyle=UITextBorderStyleNone;
    self.txtConfirm_Email.backgroundColor=[UIColor whiteColor];
    self.txtConfirm_Email.layer.borderWidth=1.0;
    self.txtConfirm_Email.returnKeyType=UIReturnKeyNext;
    self.txtConfirm_Email.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtEmail.textAlignment=NSTextAlignmentLeft;
    self.txtConfirm_Email.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtConfirm_Email.keyboardType=UIKeyboardTypeEmailAddress;
    self.txtConfirm_Email.delegate=self;
    self.txtConfirm_Email.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Confirm Email"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    [self.view addSubview:self.txtConfirm_Email];

    
    self.txt_Cell_Number=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtConfirm_Email.frame.origin.y + self.txtConfirm_Email.frame.size.height+height , self.view.frame.size.width-24, 35)];
    UIView *viewPadding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txt_Cell_Number.leftView = viewPadding6;
    self.txt_Cell_Number.leftViewMode = UITextFieldViewModeAlways;
    self.txt_Cell_Number.layer.cornerRadius=6;
     self.txt_Cell_Number.returnKeyType=UIReturnKeyNext;
//    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
//    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
//    numberToolbar.items = [NSArray arrayWithObjects:
//                           
//                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                           [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
//                           nil];
//    [numberToolbar sizeToFit];
     self.txt_Cell_Number.tag=4;
//    self.txt_Cell_Number.inputAccessoryView = numberToolbar;
    self.txt_Cell_Number.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    self.txt_Cell_Number.borderStyle=UITextBorderStyleNone;
    self.txt_Cell_Number.backgroundColor=[UIColor whiteColor];
    self.txt_Cell_Number.layer.borderWidth=1.0;
    self.txt_Cell_Number.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.txt_Cell_Number.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"612 123 1234"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    self.txt_Cell_Number.delegate=self;
    
    [self.view addSubview:self.txt_Cell_Number];

    
    
    
    self.txtPswrd=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txt_Cell_Number.frame.origin.y + self.txt_Cell_Number.frame.size.height+height, self.view.frame.size.width-24, 35)];
    UIView *viewPadding3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtPswrd.leftView = viewPadding3;
    self.txtPswrd.leftViewMode = UITextFieldViewModeAlways;
    self.txtPswrd.layer.cornerRadius=6;
    self.txtPswrd.tag=5;
    self.txtPswrd.returnKeyType=UIReturnKeyNext;
    self.txtPswrd.borderStyle=UITextBorderStyleNone;
    self.txtPswrd.backgroundColor=[UIColor whiteColor];
    self.txtPswrd.layer.borderWidth=1.0;
    self.txtPswrd.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtPswrd.textAlignment=NSTextAlignmentLeft;
    self.txtPswrd.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtPswrd.keyboardType=UIKeyboardTypeEmailAddress;
    self.txtPswrd.placeholder=@"Password";
    self.txtPswrd.delegate=self;
    self.txtPswrd.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Password"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    self.txtPswrd.secureTextEntry=YES;
    [self.view addSubview:self.txtPswrd];
    
    self.txtConfirmPswrd=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtPswrd.frame.origin.y + self.txtPswrd.frame.size.height+height, self.view.frame.size.width-24, 35)];
    UIView *viewPadding4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtConfirmPswrd.leftView = viewPadding4;
    self.txtConfirmPswrd.leftViewMode = UITextFieldViewModeAlways;
    self.txtConfirmPswrd.layer.cornerRadius=6;
    self.txtConfirmPswrd.tag=6;
    self.txtConfirmPswrd.returnKeyType=UIReturnKeyDefault;
    self.txtConfirmPswrd.borderStyle=UITextBorderStyleNone;
    self.txtConfirmPswrd.backgroundColor=[UIColor whiteColor];
    self.txtConfirmPswrd.layer.borderWidth=1.0;
    self.txtConfirmPswrd.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtPswrd.textAlignment=NSTextAlignmentLeft;
    self.txtConfirmPswrd.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtConfirmPswrd.keyboardType=UIKeyboardTypeEmailAddress;
    self.txtConfirmPswrd.placeholder=@"Confirm Password";
    self.txtConfirmPswrd.delegate=self;
    self.txtConfirmPswrd.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Confirm Password"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    
    self.txtConfirmPswrd.secureTextEntry=YES;
    [self.view addSubview:self.txtConfirmPswrd];
    
    
    self.txtSelectCity=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txtConfirmPswrd.frame.origin.y + self.txtConfirmPswrd.frame.size.height+height, self.view.frame.size.width-24, 35)];
    UIView *viewPadding5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtSelectCity.leftView = viewPadding5;
    self.txtSelectCity.leftViewMode = UITextFieldViewModeAlways;
    self.txtSelectCity.layer.cornerRadius=6;
    self.txtSelectCity.tag=7;
   
    self.txtSelectCity.borderStyle=UITextBorderStyleNone;
    self.txtSelectCity.backgroundColor=[UIColor whiteColor];
    self.txtSelectCity.layer.borderWidth=1.0;
    self.txtSelectCity.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtEmail.textAlignment=NSTextAlignmentLeft;
    self.txtSelectCity.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.txtSelectCity.delegate=self;
    self.txtSelectCity.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Select City"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    UIImageView *imgView = [[UIImageView alloc]init];
    imgView.frame =  CGRectMake(self.txtSelectCity.frame.size.width-14, self.txtSelectCity.frame.origin.y+12, 12, 12);
    
    imgView.image = [UIImage imageNamed:@"small.png"];
    
    [self.view addSubview:self.txtSelectCity];
     [self.view addSubview:imgView];
    
    
    
    //PLACE-HOLDER BUTTON
    loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setFrame:CGRectMake(18,  self.txtSelectCity.frame.origin.y + self.txtSelectCity.frame.size.height+height, 290, 35)];
    loginButton.layer.borderWidth=1.0;
    loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [loginButton addTarget:self action:@selector(loginButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"Register" forState:UIControlStateNormal];
    loginButton.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];;
    [self.view addSubview:loginButton];
    if (!IS_IPHONE_5)
    {
        
       // self.view.contentSize=CGSizeMake(self.view.frame.size.width, loginButton.frame.origin.y + loginButton.frame.size.height+height+220);

    }else
    {
        //self.view.contentSize=CGSizeMake(self.view.frame.size.width, loginButton.frame.origin.y + loginButton.frame.size.height+height+210);
        

    }
    }
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField.tag==7)
    {
        [self addPickerView];
        return NO;
    }
    if(textField.tag ==6){
        CGRect frame1=self.view.frame;
        frame1.origin.y= -130 ;
        [self.view setFrame:frame1];
        
    }
    if(textField.tag ==5){
        CGRect frame1=self.view.frame;
        NSLog(@"%@",NSStringFromCGRect(frame1));
        if(frame1.origin.y<= -120){
        frame1.origin.y= -100 ;
        }
        [self.view setFrame:frame1];
        
    }
    if(textField.tag ==4){
        CGRect frame1=self.view.frame;
        NSLog(@"%@",NSStringFromCGRect(frame1));
        if(frame1.origin.y >= -120){
            frame1.origin.y= -80 ;
        }
        [self.view setFrame:frame1];
        
    }
    if(textField.tag ==3){
        CGRect frame1=self.view.frame;
        NSLog(@"%@",NSStringFromCGRect(frame1));
        if(frame1.origin.y>= -120){
            frame1.origin.y= -60 ;
        }
        
        [self.view setFrame:frame1];
        
    }
    if(textField.tag ==2){
        CGRect frame1=self.view.frame;
        NSLog(@"%@",NSStringFromCGRect(frame1));
        if(frame1.origin.y>= -120){
            frame1.origin.y= -20 ;
        }
        
        [self.view setFrame:frame1];
        
    }
    
    
    currentTextfield=textField;
    return YES;
}
-(void)addPickerView
{
    
    [currentTextfield resignFirstResponder];
    
    
//    viewPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    
//    viewPicker.backgroundColor  = [UIColor clearColor];
//    
//    self.view
    
   CGRect frame2=self.view.frame;
    NSLog(@"%@",NSStringFromCGRect(frame2));
  frame2.origin.y= 0;
 [self.view setFrame:frame2];
    
    viewForPeriodPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-210 ,self.view.frame.size.width,210)];
    viewForPeriodPicker.backgroundColor=[UIColor clearColor];
    
    
    toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
    toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
    
    
    UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
    
    [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
    [viewForPeriodPicker addSubview:toolBarForPeriodPicker];
    
    
    
    pickerViewData = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width , 180)];
    pickerViewData.backgroundColor=[UIColor whiteColor];
    pickerViewData.dataSource=self;
    pickerViewData.delegate=self;
    
    // CGRect viewFrame = self.view.frame;
    //       CGFloat pickerHeight = pickerViewData.frame.size.height;  // assume you have an outlet called picker
    //        CGRect pickerFrame = CGRectMake(0.0, viewFrame.size.height-pickerHeight,
    //                                        viewFrame.size.width, pickerHeight);
    //
    // pickerViewData.frame = pickerFrame;
    
    [viewForPeriodPicker addSubview:pickerViewData];
    
    [self.view addSubview:viewForPeriodPicker];
    
//    CGRect frame3=self.view.frame;
//    frame3.origin.y= -120 ;
//    [self.view setFrame:frame3];

}

/*-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextfield =textField;
    if (textField == self.txtSelectCity) {
       
       [textField resignFirstResponder];
        
        CGRect frame2=self.view.frame;
        frame2.origin.y= 64;
        [self.view setFrame:frame2];
        
        viewForPeriodPicker = [[UIView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-210 ,self.view.frame.size.width,210)];
        viewForPeriodPicker.backgroundColor=[UIColor clearColor];

        
        toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        [viewForPeriodPicker addSubview:toolBarForPeriodPicker];
        
        
       
        pickerViewData = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, self.view.frame.size.width , 180)];
        pickerViewData.backgroundColor=[UIColor whiteColor];
        pickerViewData.dataSource=self;
        pickerViewData.delegate=self;
        
       // CGRect viewFrame = self.view.frame;
//       CGFloat pickerHeight = pickerViewData.frame.size.height;  // assume you have an outlet called picker
//        CGRect pickerFrame = CGRectMake(0.0, viewFrame.size.height-pickerHeight,
//                                        viewFrame.size.width, pickerHeight);
//        
        // pickerViewData.frame = pickerFrame;
        
        [viewForPeriodPicker addSubview:pickerViewData];
        
        [self.view addSubview:viewForPeriodPicker];
        
         CGRect frame1=self.view.frame;
        frame1.origin.y= -120 ;
        [self.view setFrame:frame1];
    }
}*/

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag==7) {
        
        CGRect frame1=self.view.frame;
        frame1.origin.y= 0 ;
        [self.view setFrame:frame1];
    }
}

-(void)cancelBtnPressedPeriod
{
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    
    [self.view setFrame:frame1];
    [pickerViewData removeFromSuperview];
    [viewForPeriodPicker removeFromSuperview];
}
-(void)doneBtnPressedPeriod
{
    
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    
    [self.view setFrame:frame1];
    
    
    //[tableViewAdding reloadData];
    [pickerViewData removeFromSuperview];
    [viewForPeriodPicker removeFromSuperview];
    
}

-(void)doneWithNumberPad
{
    [self.txtPswrd becomeFirstResponder];
    
}
-(void)doneWithVerification
{
    
    [self.txtVerificationField resignFirstResponder];
}

-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)generateRandomCode
{
    NSString * addString;
    [self showLoadingView];
    int min = 10000;
    int max = 99999;
    
    int randNum = rand() % (max - min) + min;
    
    NSString* randomCode=[NSString stringWithFormat:@"Bite Confirmation Code \n %d",randNum];
    
    NSString * mobileNumber=[NSString stringWithFormat:@"%@",self.txt_Cell_Number.text];
    NSLog(@"before = %@",mobileNumber);

   NSString * checkNumber=[mobileNumber substringToIndex:1];
    NSLog(@"check sign = %@",checkNumber);

    if ([checkNumber isEqualToString:@"+"])
    {
        
        addString=mobileNumber;
        
        NSLog(@"after = %@",addString);
        
    }else
    {
        addString=[@"+1" stringByAppendingString:self.txt_Cell_Number.text];

    }
      NSLog(@"checking = %@",addString);
    addString = [addString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [self sendCode:addString randomCode:randomCode];
    
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_Cell_Number.text forKey:@"cell_number"];
    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInt:randNum] forKey:@"codeIs"];
    [[NSUserDefaults standardUserDefaults]synchronize];

 
    NSLog(@"phone number is = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"]);
    NSLog(@"checking plus sign = %@ ",randomCode);
    
    

       
}
#pragma mark - Message sending Method

-(void)sendCode:(NSString *)toContact randomCode:(NSString *)randomCode

{
     NSMutableDictionary *dict_Value = [[NSMutableDictionary alloc]init];
    [dict_Value setObject:@"+17786553948" forKey:@"From"];
    [dict_Value setObject:toContact forKey:@"To"];
    [dict_Value setObject:randomCode forKey:@"Body"];
    
    NSLog(@"DICT=%@",dict_Value);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"ACf656d70460e43f77cbbaa9bfaf535875" password:@"218b1d1ff3fd6125ff5d113aca779dbc"];
    [manager POST:sms_url2 parameters:dict_Value success:
     ^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"%@",responseObject);
         
         id status=[responseObject valueForKey:@"status"];
         NSLog(@"status=%@",status);
         if ([status isEqualToString:@"queued"])
         {
             [self hideLoadingView];
             TwillioIntegrationViewController * twilioVC=[[TwillioIntegrationViewController alloc]init];
             twilioVC.profileImage=img22;
             self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
             self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
             [self.navigationController pushViewController:twilioVC animated:YES];
             NSLog(@"code sent");
         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please check your cell number once" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];

         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [self hideLoadingView];
         NSLog(@"error=%@",error);
         if (error) {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please check your cell number once" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];

         }
        
         
     }];

    
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [cityArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
        return cityArray.count;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   
    if(row ==0){
       self.txtSelectCity.text = @"";
    }else{
    self.txtSelectCity.text = [cityArray objectAtIndex:row];
    }
     NSLog(@"%@",self.txtSelectCity.text);
}




@end
