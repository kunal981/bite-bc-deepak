//
//  TwillioIntegrationViewController.h
//  BiteBC
//
//  Created by brst on 11/8/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "BiteBCMainViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "SelectPlanViewController.h"

#define sms_url2 @"https://api.twilio.com/2010-04-01/Accounts/ACf656d70460e43f77cbbaa9bfaf535875/Messages.json"

@interface TwillioIntegrationViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,UITabBarControllerDelegate,MFMailComposeViewControllerDelegate>
{
     NSURLConnection* connection;
      NSString* base64String;
    NSMutableDictionary* dict;
  
    NSMutableData* data;
    UIAlertView *alertTextfield;
    UITextField* textF;
    UIView * customView;
    UIButton*  resendBtn ;
    BiteBCMainViewController * biteBCMainVC;
   UIImage *profileImage;
}
-(void)tabBar;
-(void)postData;
@property(nonatomic,strong) UITabBarController* tabBarController;

@property(strong,nonatomic)UITextField *txtVerificationField;
@property(strong,nonatomic)UITextField *txt_Mobile_Number;
@property(strong,nonatomic)UIImage *profileImage;

@end
