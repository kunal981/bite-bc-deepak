//
//  DetailOfUserViewController.m
//  BiteBC
//
//  Created by brst on 7/11/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "DetailOfUserViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"

#import "BiteBCMainViewController.h"



@interface DetailOfUserViewController ()
{
    UIView *memberShipView;
    UIView *sixMonthsPlanView_thirtyDay;
    UIView *sixMonthsPlanView_monthlyPlan;
    UIView *sixMonthsPlanView_TwelveMonths;
    UIImageView *tickImageView_thirtyDay;
    UIImageView *tickImageView_monthlyPlan;
    UIImageView *tickImageView_TwelveMonths;
    
    
}

@end

@implementation DetailOfUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    checkString = @"0";
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    }
-(void)loadView
{
       UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}



- (void)viewDidLoad
{
    
    NSLog(@"%@",self.idString);
    self.view.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    
    self.navigationItem.title=self.titleStr;
    if ([self.idString isEqualToString:@"0"])
    {
        [self userFields];
   
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    }
    else if ([self.idString isEqualToString:@"2"])
    {
        
        //[self checkPlan];
        [self changeMemberShip];
        
    }
    else if ([self.idString isEqualToString:@"3"])
    {
        [self aboutUs];
    }
    else if ([self.idString isEqualToString:@"4"])
    {
        
        imagesArr=[[NSMutableArray alloc]initWithObjects:@"key1.png",@"key-img2.png",@"key-img3.png",@"key-img4.png",@"key-img5.png",@"key-img6.png",@"key-img7.png",nil];
        labelTextArr=[[NSMutableArray alloc]initWithObjects:@"The restaurant offers Bite members 50% OFF the total food bill",@"The restaurant offers Bite members 2 for 1 meals (across all courses ordered), 2 for 1 means one Bite membership applies for 2 people.",@"You must telephone book in advance and mention Bite members",@"The Bite members offer is not available Friday evenings from 6:30pm onwards.",@"The Bite members offer is not available Saturday evenings from 6:30pm onwards.",@"The Bite members offer is not available during December",@" Number denotes the maximum number of people per Bite booking",nil];
        [self biteBCWorkingView];

    }
    else if ([self.idString isEqualToString:@"6"])
    {
        [self termsAndConditions];
    }
    
    else if ([self.idString isEqualToString:@"1"])
    {
        cityArray = [[NSMutableArray alloc]init];
        cityIDArray = [[NSMutableArray alloc]init];
        [self cityFields];
        [self getCities];
    }
}

-(void)cityFields
{
    UIView *detail_View=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,360)];
    detail_View.backgroundColor=[UIColor clearColor];
    [self.view addSubview:detail_View];
    city=[[UITextField alloc]initWithFrame:CGRectMake(5,10,310, 35)];
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    city.leftView = paddingView3;
    city.leftViewMode = UITextFieldViewModeAlways;
    city.layer.borderWidth=1.0;
    city.layer.borderColor=[UIColor lightGrayColor].CGColor;
    city.delegate=self;
    city.enabled=YES;
    city.backgroundColor=[UIColor whiteColor];
    city.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"city"];
    city.returnKeyType=UIReturnKeyDone;
    
    city.font=[UIFont systemFontOfSize:13];
    city.placeholder=@"Select city";
    [detail_View addSubview:city];
    
    UIImageView *imgView = [[UIImageView alloc]init];
    imgView.frame =  CGRectMake(city.frame.size.width-20, city.frame.origin.y+10, 12, 12);
    
    imgView.image = [UIImage imageNamed:@"small.png"];
    
    
    [detail_View addSubview:imgView];
    
    
    UIButton *submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
    [submit_Button setFrame:CGRectMake(2,55,316,35)];
    submit_Button.layer.borderWidth=1.0;
    submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
    [submit_Button addTarget:self action:@selector(submit_Button) forControlEvents:UIControlEventTouchUpInside];
    [submit_Button setTitle:@"SUBMIT" forState:UIControlStateNormal];
    submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [detail_View addSubview:submit_Button];
    
    UITextView *txtView = [[UITextView alloc]initWithFrame:CGRectMake(2,90,316,100)];
    txtView.text = @"Select which city you are currently in to see our list of restaurants that are available in the selected city. These restaurants will now appear in your Restaurants tab.";
    txtView.scrollEnabled = NO;
    txtView.userInteractionEnabled = NO;
    txtView.textColor = [UIColor darkGrayColor];
    txtView.font = [UIFont systemFontOfSize:15];
    [detail_View addSubview:txtView];

}



 -(void)userFields

{
    UIView *detail_View=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,400)];
    detail_View.backgroundColor=[UIColor clearColor];
    [self.view addSubview:detail_View];
    
//    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(10,0,150,30)];
//    label.backgroundColor=[UIColor clearColor];
//    label.textColor=[UIColor lightGrayColor];
//    label.textAlignment=NSTextAlignmentLeft;
//    label.text=@"User";
//    label.numberOfLines=4;
//    label.font=[UIFont boldSystemFontOfSize:15];
//    [detail_View addSubview:label];

    
    
    UITextField *firstName=[[UITextField alloc]initWithFrame:CGRectMake(5,10,310, 35)];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    firstName.leftView = paddingView;
    firstName.leftViewMode = UITextFieldViewModeAlways;
    firstName.layer.borderWidth=1.0;
    firstName.layer.borderColor=[UIColor lightGrayColor].CGColor;
    firstName.delegate=self;
    firstName.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"first"];
    firstName.backgroundColor=[UIColor whiteColor];
    firstName.returnKeyType=UIReturnKeyDone;
    firstName.font=[UIFont systemFontOfSize:13];
    firstName.placeholder=@"Enter your first name";
    firstName.enabled=NO;
    [detail_View addSubview:firstName];
    
    UITextField *lastNametextField=[[UITextField alloc]initWithFrame:CGRectMake(5,50,310, 35)];
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    lastNametextField.leftView = paddingView1;
    lastNametextField.leftViewMode = UITextFieldViewModeAlways;
    lastNametextField.layer.borderWidth=1.0;
    lastNametextField.layer.borderColor=[UIColor lightGrayColor].CGColor;
    lastNametextField.delegate=self;
     lastNametextField.enabled=NO;
    lastNametextField.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"last"];
    lastNametextField.backgroundColor=[UIColor whiteColor];
    lastNametextField.returnKeyType=UIReturnKeyDone;
    lastNametextField.font=[UIFont systemFontOfSize:13];
    lastNametextField.placeholder=@"Enter your last name";
    [detail_View addSubview:lastNametextField];

    UITextField *emailAdrees=[[UITextField alloc]initWithFrame:CGRectMake(5,90,310, 35)];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    emailAdrees.leftView = paddingView2;
    emailAdrees.leftViewMode = UITextFieldViewModeAlways;
    emailAdrees.layer.borderWidth=1.0;
    emailAdrees.layer.borderColor=[UIColor lightGrayColor].CGColor;
    emailAdrees.delegate=self;
    emailAdrees.enabled=NO;
    emailAdrees.backgroundColor=[UIColor whiteColor];
    emailAdrees.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"];
    emailAdrees.returnKeyType=UIReturnKeyDone;
    
    emailAdrees.font=[UIFont systemFontOfSize:13];
    emailAdrees.placeholder=@"Enter your email adress";
    [detail_View addSubview:emailAdrees];
    
    
    
    
    
    
   

    
}

-(void)changeMemberShip
{
    memberShipView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,400)];
    memberShipView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:memberShipView];
    
    
    
    int h=10;
    
    
    for (int i=0; i<3; i++)
    {
        sixMonthsPlanView_thirtyDay=[[UIView alloc]initWithFrame:CGRectMake(5,h,310, 35)];
        sixMonthsPlanView_thirtyDay.backgroundColor=[UIColor whiteColor];
        sixMonthsPlanView_thirtyDay.layer.borderWidth=1.0;
        sixMonthsPlanView_thirtyDay.layer.borderColor=[UIColor lightGrayColor].CGColor;
        sixMonthsPlanView_thirtyDay.tag=i+3;
        sixMonthsPlanView_thirtyDay.userInteractionEnabled=YES;
        h+=40;
        [memberShipView addSubview:sixMonthsPlanView_thirtyDay];
        
       
        
//    sixMonthsPlanView_monthlyPlan=[[UIView alloc]initWithFrame:CGRectMake(5,50,310, 35)];
//    sixMonthsPlanView_monthlyPlan.backgroundColor=[UIColor whiteColor];
//    sixMonthsPlanView_monthlyPlan.layer.borderWidth=1.0;
//    sixMonthsPlanView_monthlyPlan.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    // sixMonthsPlanView_thirtyDay.tag=i+3;
//    sixMonthsPlanView_monthlyPlan.userInteractionEnabled=YES;
//    h+=40;
//    [memberShipView addSubview:sixMonthsPlanView_monthlyPlan];
//    
//
//    
//    sixMonthsPlanView_TwelveMonths=[[UIView alloc]initWithFrame:CGRectMake(5,90,310, 35)];
//    sixMonthsPlanView_TwelveMonths.backgroundColor=[UIColor whiteColor];
//    sixMonthsPlanView_TwelveMonths.layer.borderWidth=1.0;
//    sixMonthsPlanView_TwelveMonths.layer.borderColor=[UIColor lightGrayColor].CGColor;
//    // sixMonthsPlanView_thirtyDay.tag=i+3;
//    sixMonthsPlanView_TwelveMonths.userInteractionEnabled=YES;
//    h+=40;
//    [memberShipView addSubview:sixMonthsPlanView_TwelveMonths];
//    

    
    
        
        
        
        tickImageView_thirtyDay=[[UIImageView alloc]initWithFrame:CGRectMake(280, 8, 30, 20)];
        tickImageView_thirtyDay.backgroundColor=[UIColor clearColor];
        //tickImageView.tag=i+3;
              [sixMonthsPlanView_thirtyDay addSubview:tickImageView_thirtyDay];
    
    
    
//    
//    tickImageView_monthlyPlan=[[UIImageView alloc]initWithFrame:CGRectMake(280, 8, 30, 20)];
//    tickImageView_monthlyPlan.backgroundColor=[UIColor clearColor];
//    //tickImageView.tag=i+3;
//    [sixMonthsPlanView_monthlyPlan addSubview:tickImageView_monthlyPlan];
//
//    
//    
//    
//    
//    tickImageView_TwelveMonths=[[UIImageView alloc]initWithFrame:CGRectMake(280, 8, 30, 20)];
//    tickImageView_TwelveMonths.backgroundColor=[UIColor clearColor];
//    //tickImageView.tag=i+3;
//    [sixMonthsPlanView_TwelveMonths addSubview:tickImageView_TwelveMonths];
//
    
//    UILabel* lbl=[[UILabel alloc]initWithFrame:CGRectMake(10,8,210,20)];
//    lbl.textAlignment=NSTextAlignmentLeft;
//    lbl.text = @"30 Day Membership Plan";
    
    
    
        
        int p_id=[[[NSUserDefaults standardUserDefaults]valueForKey:@"planid"] intValue];
        
        if (sixMonthsPlanView_thirtyDay.tag == p_id)
        {
             tickImageView_thirtyDay.image=[UIImage imageNamed:@"tick1.png"];
        }
        
        
        UILabel* lbl=[[UILabel alloc]initWithFrame:CGRectMake(10,8,210,20)];
        lbl.textAlignment=NSTextAlignmentLeft;
        
        
        if (i==0)
        {
            //lbl.text=@"3 Months Free Trail";
         
            if (p_id == 209)
            {
                tickImageView_thirtyDay.image=[UIImage imageNamed:@"tick1.png"];
            }

            
            lbl.text=@"30 Day Free Trial";
            
        }
        else if (i==1)
        {
             lbl.text=@"Monthly Membership Plan";
 
        }
//        else if (i==3)
//        {
//            
//            lbl.text=@"30 Days Membership Plan";
//        }
        else
        {
           lbl.text=@"12 Month Membership Plan";
        }
        
        lbl.font=[UIFont systemFontOfSize:14];
        lbl.textColor=[UIColor blackColor];
        [sixMonthsPlanView_thirtyDay addSubview:lbl];
        
    }
    
    
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(10,130,300,30)];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor lightGrayColor];
        label.textAlignment=NSTextAlignmentLeft;
        label.text= [NSString stringWithFormat:@"Days to expire - %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]];
        label.numberOfLines=4;
        label.font=[UIFont boldSystemFontOfSize:15];
        [memberShipView addSubview:label];
    
    
    
    
    
    

//    UIButton *changePlanBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    [changePlanBtn setFrame:CGRectMake(2,170,316,35)];
//    changePlanBtn.layer.borderWidth=1.0;
//    changePlanBtn.layer.borderColor=[UIColor whiteColor].CGColor;
//    [changePlanBtn addTarget:self action:@selector(submit_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [changePlanBtn setTitle:@"Renew Now" forState:UIControlStateNormal];
//    changePlanBtn.titleLabel.font=[UIFont systemFontOfSize:17];
//    [changePlanBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    changePlanBtn.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
//    [memberShipView addSubview:changePlanBtn];
  }



-(void)aboutUs
{
    
//    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(10,0,210,30)];
//    label.backgroundColor=[UIColor clearColor];
//    label.textColor=[UIColor lightGrayColor];
//    label.textAlignment=NSTextAlignmentLeft;
//    label.text=@"About Us";
//    label.numberOfLines=4;
//    label.font=[UIFont boldSystemFontOfSize:15];
//    [self.view addSubview:label];
//
    
    NSString* myText=[NSString stringWithFormat:@"%@",@"Bite BC is a large scale diners club in the form of a mobile app providing members with discounts on eating out.\n\n\nEating out can be very expensive, which more often than not stops people from doing it as often as they would like to. With our intuitive app you will be able to locate all of our participating restaurants at a touch of a button and will save hundreds of dollars over the course of the year.\nAs a company we have two main aims; to help members by making eating out more affordable and to also help restaurants by encouraging people to eat out more often.\n\n\nAll of our restaurants will offer unlimited usage, meaning that our members are not restricted to just one visit per restaurant; you can go back as many times as you wish. With your Bite BC membership you get half price dining at all our restaurants (either 50% off or 2 for 1 across all courses"];
    
    UITextView *review_Textview;
    if (!IS_IPHONE_5)
    {
      review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,5,316,310)];
    }
    else
    {
    review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,5,316,440)];
    }
   
    review_Textview.backgroundColor=[UIColor whiteColor];
    review_Textview.layer.borderWidth=1.0;
    review_Textview.layer.masksToBounds=YES;
    review_Textview.layer.cornerRadius=4;
    review_Textview.text=myText;
    review_Textview.editable=NO;
    review_Textview.font=[UIFont systemFontOfSize:14];
    review_Textview.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
    
    
    [self.view addSubview:review_Textview];

    
}
-(void)biteBCWorkingView
{
    
//    UILabel *label2=[[UILabel alloc]initWithFrame:CGRectMake(10,0,270,30)];
//    label2.backgroundColor=[UIColor clearColor];
//    label2.textColor=[UIColor lightGrayColor];
//    label2.textAlignment=NSTextAlignmentLeft;
//    label2.text=@"How to use your BiteBC membership";
//    label2.numberOfLines=4;
//    label2.font=[UIFont boldSystemFontOfSize:15];
//    [self.view addSubview:label2];
//
    
    NSString *textIs=[NSString stringWithFormat:@"%@",@"A biteBC membership gives you 50% off the total food bill, or 2 for the price of 1 at all participating restaurants."];
    
    UIScrollView *detail_View=[[UIScrollView alloc]initWithFrame:CGRectMake(0,5,320,670)];
    detail_View.backgroundColor=[UIColor clearColor];
    [self.view addSubview:detail_View];

    
    UIView *biteBC_view=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,670)];
    biteBC_view.backgroundColor=[UIColor whiteColor];
    biteBC_view.layer.borderWidth=1.0;
    biteBC_view.layer.masksToBounds=YES;
    biteBC_view.layer.cornerRadius=4;
//    biteBC_Textview.text=textIs;
//    biteBC_Textview.editable=NO;
//    biteBC_Textview.font=[UIFont systemFontOfSize:13];
    biteBC_view.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
    [detail_View addSubview:biteBC_view];
    
   
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(5,0,316,60)];
    label.backgroundColor=[UIColor clearColor];
    //label.textColor=[UIColor blackColor];
    label.textAlignment=NSTextAlignmentLeft;
    label.text=textIs;
    label.numberOfLines=4;
    label.font=[UIFont systemFontOfSize:13];
    [biteBC_view addSubview:label];
   
    int x=7;
    
    for (int i=0; i<3; i++)
    {
        UIView *stepView=[[UIView alloc]initWithFrame:CGRectMake(x,58,100,80)];
        stepView.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        stepView.layer.cornerRadius=2;
        stepView.layer.borderWidth=.7;
        stepView.layer.borderColor=[UIColor blackColor].CGColor;
        x+=105;
        [biteBC_view addSubview:stepView];
        
        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(5,5,50,15)];
        label.backgroundColor=[UIColor clearColor];
        //label.textColor=[UIColor blackColor];
        label.textAlignment=NSTextAlignmentLeft;
        label.textColor=[UIColor whiteColor];
        label.tag=1;
        label.text=[NSString stringWithFormat:@"Step %d",i+1];
        label.font=[UIFont boldSystemFontOfSize:14];
        [stepView addSubview:label];
        
        label=[[UILabel alloc]initWithFrame:CGRectMake(5,25,93,50)];
        label.backgroundColor=[UIColor clearColor];
        //label.textColor=[UIColor blackColor];
        label.textAlignment=NSTextAlignmentLeft;
        label.tag=2;
        label.textColor=[UIColor whiteColor];
        label.numberOfLines=4;
        UIImageView* imageV=[[UIImageView alloc]initWithFrame:CGRectMake(60, 7, 20, 20)];
                [stepView addSubview:imageV];

        if (i==0)
        {
             label.text=@"Search for restaurants and choose where to dine!";
            imageV.image=[UIImage imageNamed:@"search_1.png"];

        }
        else if (i==1)
        {
             label.text=@"Book mentioning BiteBC and checkin";
            imageV.image=[UIImage imageNamed:@"search_2.png"];

        }
        else
        {
            label.text=@"Show your BiteBC checkin on your phone to the server";
            imageV.image=[UIImage imageNamed:@"search_3.png"];


        }
       
        label.font=[UIFont boldSystemFontOfSize:10];
        [stepView addSubview:label];
        
       
        
        
    }

    
   UILabel *biteBC_Textview=[[UILabel alloc]initWithFrame:CGRectMake(2,140,316,90)];
    
    biteBC_Textview.backgroundColor=[UIColor clearColor];
    biteBC_Textview.font=[UIFont systemFontOfSize:13];
    biteBC_Textview.textAlignment=NSTextAlignmentLeft;
    biteBC_Textview.numberOfLines=6;
    biteBC_Textview.layer.masksToBounds=YES;
    biteBC_Textview.text=@"Important: You must make an advance booking mentioning biteBc, unless the 'Call' icon is not shown. Please note 'The Full Key' icons to recognise the pre-booking facilities available for each restaurant.";
    [biteBC_view addSubview:biteBC_Textview];
    
    int h=240;
    for (int i=0; i<imagesArr.count; i++)
    {
//        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(5,h,60,30)];
//        label.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
//        label.layer.cornerRadius=2;
//        label.layer.borderWidth=.7;
//        label.textColor=[UIColor whiteColor];
//        label.textAlignment=NSTextAlignmentCenter;
//        label.font=[UIFont boldSystemFontOfSize:17];
//        label.layer.borderColor=[UIColor blackColor].CGColor;
//        if (i==0)
//        {
//            label.text=@"50%";
//        }
//        else
//        {
//            label.text=@"2 for 1";
//
//        }
//        
//        h+=125;
//        [biteBC_view addSubview:label];
        
        
        
        UIImageView *imageOfKey=[[UIImageView alloc]initWithFrame:CGRectMake(5, h, 50, 30)];
        imageOfKey.backgroundColor=[UIColor grayColor];
        imageOfKey.layer.masksToBounds=YES;
        imageOfKey.image=[UIImage imageNamed:[imagesArr objectAtIndex:i]];
       
        [biteBC_view addSubview:imageOfKey];
        
        UILabel *biteBC_Label;
        if (i==1)
        {
            biteBC_Label=[[UILabel alloc]initWithFrame:CGRectMake(65,h-5,250,60)];
            biteBC_Label.backgroundColor=[UIColor clearColor];
            biteBC_Label.font=[UIFont systemFontOfSize:12];
            // biteBC_Label.layer.masksToBounds=YES;
            biteBC_Label.numberOfLines=4;
            biteBC_Label.text=[labelTextArr objectAtIndex:i];

        }
        else
        {
           biteBC_Label=[[UILabel alloc]initWithFrame:CGRectMake(65,h-5,250,40)];
            biteBC_Label.backgroundColor=[UIColor clearColor];
            biteBC_Label.font=[UIFont systemFontOfSize:13];
            biteBC_Label.adjustsFontSizeToFitWidth=YES;
            // biteBC_Label.layer.masksToBounds=YES;
            biteBC_Label.numberOfLines=2;
            biteBC_Label.text=[labelTextArr objectAtIndex:i];
         }
        
        
        h+=imageOfKey.frame.size.height+33;
        
        [biteBC_view addSubview:biteBC_Label];

        
        
    }
    
//    UILabel *biteBC_Label=[[UILabel alloc]initWithFrame:CGRectMake(75,235,230,120)];
//   // biteBC_Textview.editable=NO;
//    biteBC_Label.backgroundColor=[UIColor clearColor];
//    biteBC_Label.font=[UIFont systemFontOfSize:12];
//   biteBC_Textview.layer.masksToBounds=YES;
//    biteBC_Label.numberOfLines=8;
//    biteBC_Label.text=@"50% OFF the total food bill means that all diners in a party can eat and receive a 50% discount on their food, even with just one biteBc membership member in the party. However, please do check with the individual restaurant for the maximum numbers permitted per biteBc membership.";
//    [biteBC_view addSubview:biteBC_Label];
//    
//    biteBC_Label=[[UILabel alloc]initWithFrame:CGRectMake(75,340,230,120)];
//    
//    biteBC_Label.backgroundColor=[UIColor clearColor];
//    biteBC_Label.font=[UIFont systemFontOfSize:12];
//    biteBC_Label.layer.masksToBounds=YES;
//     biteBC_Label.numberOfLines=7;
//    biteBC_Label.text=@"The restaurant offers Bite members 2 for 1 meals (across all courses ordered), 2 for 1 means one Bite membership applies for 2 people.";
//    [biteBC_view addSubview:biteBC_Label];
  
    
    
    if (!IS_IPHONE_5)
    {
        detail_View.contentSize=CGSizeMake(320,1100);

    }
    else
    {
        detail_View.contentSize=CGSizeMake(320,900);

    }
    
}

//-(void)fullKeyView
//{
//    UIView *detail_View=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320,400)];
//    detail_View.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:detail_View];
//
//    
//    
//    
//    UIImageView *imageView=[[UIImageView alloc]initWithFrame: CGRectMake(-1, 10, 320,400)];
//    imageView.image=[UIImage imageNamed:@"key3.png"];
//    imageView.layer.masksToBounds=YES;
//    imageView.backgroundColor=[UIColor clearColor];
//    [detail_View addSubview:imageView];
//    
//    
//}
//

-(void)howAppWork{
    
}
-(void)termsAndConditions
{
    NSString * myText=[NSString stringWithFormat:@"1.Introduction\n1.1  This page (together with the documents referred to on it) tells you the terms and conditions on which we supply Bite diner’s club membership. Further details regarding Bite diner’s club can be found on our website www.bitebc.ca (our website). Please read these terms and conditions carefully and make sure that you understand them, before ordering your membership for our app. You should understand that by ordering a Bite membership, you agree to be bound by these terms and conditions.\n\n1.2 You should print a copy of these terms and conditions for future reference.\n\n1.3 Please click on the button marked “I have read and agreed to terms and conditions” onthe Bite diner’s club purchase page on our app if you accept them. By doing so you also agree that you consent to us processing your data in accordance with our privacy policy. Please understand that if you refuse to accept these terms and conditions, you will not be able to purchase a membership with us nor become a member of the club.\n\n1.4  We reserve the right to amend these terms and conditions at any time by giving you notice by posting the amended terms and conditions on our site or within our app. However, please note that you will be subject to the terms and conditions in force at the time that you ordered a membership from us, unless any change to these terms and conditions is required to be made by law or governmental authority (in which case it will apply to orders previously placed by you).\n\n2. Information about us\n2.1 We operate our site and mobile apps. We are Bite Diners Club Ltd. a company registered in Canada with our registered office at 1055 West Hastings Street, Vancouver, BC, V6E 2E9\n\n3. Your status\n3.1 By placing an order for a Bite membership through our site or mobile app, you warrant that you are legally capable of entering into binding contracts and you are at least 18 years old.\n\n4. How the contract between you and us is formed \n4.1 After placing an order for a Bite membership, you will receive an e-mail from us acknowledging that we have received your order. Please note that this does not mean that your order has been accepted. Your order constitutes an offer to us to become a member of the club. All orders are subject to acceptance by us. The contract between us will only be formed when we send you a confirmation of acceptance of payment for membership.\n\n5. Term\n5.1 The club is an on-going subscription service so your membership is continuous and your membership is renewed automatically at the end of each membership period. If you do not want to renew your membership you should contact us by email to info@bitebc.ca at any point within your membership period and no later than 5pm on the working day preceding your renewal date. Your renewal date being the day prior to the expiry date stated on your membership plan.You are required to inform us if you change your correspondence email address. We will not be liable for any non-receipt of communication from us\n\n6.Consumer rights\n6.1 You have the right to cancel your initial registration of membership with us within 14 days of your original purchase. This does not apply to subsequent renewals.\n\n6.2 To cancel your membership, you must email us at info@bitebc.ca Monday to Friday 9.00am to 5.00pm. Upon cancellation you will receive a confirmation email; it is recommended that this is kept for your own personal records.\n\n6.3 If you cancel your membership within the cooling off period (14 days), you will be entitled to a refund of your membership, less our $10 administration charge for cancellations. Any cancellations after the cooling off period (14 days) are not entitled to any refund.\n\n7. Availability and delivery\n7.1 You will receive access to your membership immediately once payment has cleared and you receive confirmation email, unless there are any exceptional circumstances. In which case please contact us at info@bitebc.ca.\n\n8. Price and payment\n8.1 The price of membership of the club will be subject to change and as quoted on our site or mobile app from time to time, except in cases of obvious error.\n\n8.2 Prices include TAX.\n\n8.3 Prices are liable to change at any time, but changes will not affect orders in respect of which we have already sent you confirmation.\n\n8.4 Payment must be by credit or debit card, or by such other method as we may agree from time to time. We will charge your credit or debit card when your order is placed.\n\n9. Participating restaurants and use of your Bite Membership\n9.1 On presentation of your Bite membership, participating restaurants will offer either 50% off your total food bill (applying to everyone dining in the group; although, the relevant restaurant may place a limit on the maximum number of people per booking (please check our site for details) or 2 for 1 meals from the restaurants menu (the cheapest item from each course will be deducted from the bill).\n\n9.2 Participating restaurants may exclude some days of the week, all of December and public holidays. Please check our app for which restrictions apply for each restaurant. Mothers' Day, Fathers' Day and Valentines' Day are automatically excluded from the offer. Please refer to individual restaurant details page in our app as other exclusions may apply.\n\n9.3 If a participating restaurant has a telephone icon listed on its page on our app or site advance bookings must be made and by calling the restaurant and your use of Bite membership must be mentioned.\n\n9.4 Offers advertised on our site or app are only available to members who present their checkin on their mobile phone and their membership is confirmed by the server and is valid. Such offers are not available in conjunction with any other offers that participating restaurants may be running, which may include set menus or any other menus other than the standard a la carte menu.\n\n9.5 The expiry date of each Bite Membership will vary and will always be checked at each restaurant. Expired Bite memberships are not accepted by participating restaurants. Memberships are strictly non-transferable and can only be used by named members and their dining partners, up to the limited specified by participating restaurants on our site. Any attempted misuse of your Bite Membership may result in your membership being revoked.\n\n9.6 We will use reasonable endeavours to update our site and mobile app to show the particulars of participating restaurants and the terms of their availability for participation in the club. Participating restaurants may, however, be entitled to withdraw from the club or to change the terms and conditions of their availability after you have become a member and we shall have no liability for any such withdrawals or changes in terms and conditions or availability.\n\n9.7 Members will have the benefit of any additional restaurants which join the club at a later date and any increase in availability of participating restaurants.\n\n9.8 Our printed or online marketing material is intended as a guide of restaurants who are participating at the time of publication and, therefore, may not include all participating restaurants at any one time.\n\n10. Our liability\n10.1 Subject to clause 10.3, if we fail to comply with these terms and conditions, we shall only be liable for the membership fee.\n\n10.2 Subject to clause 10.3, we will not be liable for losses that result from our failure to comply with these terms and conditions that fall into the following categories:\n\n10.2.1 loss of income or revenue;\n\n10.2.2 loss of business;\n\n10.2.3 loss of profits; or\n\n10.2.4 loss of anticipated savings.\n\n10.3 Nothing in this agreement excludes or limits our liability for:\n\n10.3.1 death or personal injury caused by our negligence;\n\n10.3.2 fraud or fraudulent misrepresentation;\n\n10.4 Where you purchase food from any participating restaurant, any losses or liability arising out of, or in connection with, such food shall be the relevant participating restaurant's liability.We accept no liability for any bad experiences or bad food at any of the participating restaurants. We will not become involved in any dispute between you and any restaurant.\n\n10.5 We do not give any warranty for any goods or services accessed through, or displayed on, our site or mobile app.\n\n11.Written communication\n11.1 Applicable laws require that some of the information or communications we send to you should be in writing. When using our site, you accept that communication with us will be mainly electronic. We will contact you by e-mail or provide you with information by posting notices on our website or social media channels. For contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights.\n\n12. Notices\n12.1 All notices given by you to us must be given to The Operations Director at info@bitebc.ca. We may give notice to you at either the e-mail or telephone number you provide to us when placing an order, or in any of the ways specified in clause 11 above. Notice will be deemed received and properly served immediately when posted on our website, 24 hours after an e-mail is sent, or contact by phone, or three days after the date of posting of any letter. In proving the service of any notice, it will be sufficient to prove, in the case of a letter, that such letter was properly addressed, stamped and placed in the post and, in the case of an e-mail, that such e-mail was sent to the specified e-mail address of the addressee.\n\n13. Waiver\n13.1 Failure by us to enforce any of these terms and conditions will not prevent us from subsequently relying on, or enforcing, them.\n\n14. Severability\n14.1 If any court or competent authority decides that any of the provisions of these terms and conditions are invalid, unlawful or unenforceable to any extent, the term will, to that extent only, be severed from the remaining terms, which will continue to be valid to the fullest extent permitted by law.\n\n15. Third party rights\n15.1 A person who is not party to these terms and conditions shall not have any rights under or in connection with it under the Contracts\n\n16 Entire agreement\n16.1 These terms and conditions and any document expressly referred to in them constitute the whole agreement between us and supersede all previous discussions, correspondence, negotiations, previous arrangement, understanding or agreement between us relating to the subject matter of these terms and conditions. We each acknowledge that, in entering into these terms and conditions, neither of us relies on, or will have any remedies in respect of, any representation or warranty (whether made innocently or negligently) that is not set out in these terms and conditions or the documents referred to in them. Nothing in this clause limits or excludes any liability for fraud."];
    
    UITextView *review_Textview;
    if (!IS_IPHONE_5)
    {
        review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,2,316,362)];
    }
    else
    {
        review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(2,2,316,452)];
    }
    
    review_Textview.backgroundColor=[UIColor whiteColor];
    review_Textview.layer.borderWidth=1.0;
    review_Textview.layer.masksToBounds=YES;
    review_Textview.layer.cornerRadius=4;
    review_Textview.text=myText;
    review_Textview.editable=NO;
    review_Textview.font=[UIFont systemFontOfSize:14];
    review_Textview.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
    
    
    [self.view addSubview:review_Textview];

}

#pragma mark -  CheckMembership Plan
-(void)checkPlan
{
    
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];

    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    mutableData=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [mutableData appendData:theData];
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDictionary=[NSJSONSerialization JSONObjectWithData:mutableData options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dataDictionary);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"success"]];
    
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
        
    {
        if ([checkString isEqualToString:@"1"]) {
            NSString *msg=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"message"]];
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
           
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Refresh"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
        
        
        
         daysleft=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"daysleft"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary valueForKey:@"daysleft"] forKey:@"daysleft"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
         packName=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"package_name"]];
        
        plan_id=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"product_id"]];


        
            
        NSLog(@" %@",daysleft);
          NSLog(@" %@",packName);
        }
        
    }
    else
    {
        [self hideLoadingView];
    }
    
    //[self changeMemberShip];

}
//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}


#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
#pragma mark - UITextField Methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == city) {
        [textField resignFirstResponder];
        CGRect viewFrame = self.view.frame;
        toolBarForPeriodPicker = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0,55,viewFrame.size.width, 35)];
        toolBarForPeriodPicker.barStyle = UIBarStyleBlackOpaque;
        toolBarForPeriodPicker.tintColor=[UIColor whiteColor];
        UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressedPeriod)];
        
        
        UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressedPeriod)];
        
        [toolBarForPeriodPicker setItems:[NSArray arrayWithObjects:cancelBtn,flexibleSpaceLeft,btn1, nil]];
        
        [self.view addSubview:toolBarForPeriodPicker];
        pickerViewData = [[UIPickerView alloc]init];
        pickerViewData.backgroundColor=[UIColor whiteColor];
        pickerViewData.dataSource=self;
        pickerViewData.delegate=self;
        
        
        CGFloat pickerHeight = pickerViewData.frame.size.height;  // assume you have an outlet called picker
        CGRect pickerFrame = CGRectMake(0.0, 55+35,
                                        viewFrame.size.width, pickerHeight);
        pickerViewData.frame = pickerFrame;
        
        [self.view addSubview:pickerViewData];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelBtnPressedPeriod
{
    
    
    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
    
}
-(void)doneBtnPressedPeriod
{
    //  [self postData];
    
    [toolBarForPeriodPicker removeFromSuperview];
    [pickerViewData removeFromSuperview];
    
}

//-(void)selectPlanMethod:(UITapGestureRecognizer *)reco
//{
//   
//    for (int i=0; i<2; i++)
//    {
//        UIView* view=[[memberShipView subviews]objectAtIndex:i];
//        UIImageView *m=[[view subviews]objectAtIndex:0];
//        m.image=nil;
//        
//    }
//    UIImageView *m=[[reco.view subviews]objectAtIndex:0];
//    m.image=[UIImage imageNamed:@"tick1.png"];
////    if (reco.view.tag==0)
////    {
////        NSLog(@"0 tag");
////        
////    }
////    else
////    {
////          NSLog(@"1 tag");
////        
////    }
//   
//}
-(void)submit_ButtonPressed
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [cityArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return cityArray.count;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (row ==0) {
        city.text = @"";
    }
    else{
    city.text = [cityArray objectAtIndex:row];
    city_id = [cityIDArray objectAtIndex:row];
    }
    NSLog(@" city name %@",city.text);
    
    
}

//-(void)getCities
//{
//    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/city.php"];
//    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
//    NSURLResponse *response = nil;
//    NSError *error = nil;
//    NSLog(@"About to send req %@",req.URL);
//    NSData *dataCity = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
//    if (dataCity ==nil|| dataCity==(id)[NSNull null]) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    else {
//        NSArray *result = [NSJSONSerialization JSONObjectWithData:dataCity options:kNilOptions error:&error];
//        NSLog(@"%@",result);
//        cityName = @"Select city";
//        [cityArray addObject:cityName];
//        cityid = @" ";
//        [cityIDArray addObject:cityid];
//        for (int i =0 ; i<result.count; i++) {
//            
//            cityName = [[result objectAtIndex:i] valueForKey:@"name"];
//            
//            [cityArray addObject:cityName];
//            cityid = [[result objectAtIndex:i] valueForKey:@"id"];
//            [cityIDArray addObject:cityid];
//            
//        }
//        
//        NSLog(@"%@",cityArray);
//        
//        
//    }
//    
//}

-(void)getCities
{
    [self showLoadingView];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer  = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //[AFJSONResponseSerializer addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
      //manager.responseSerializer.acceptableContentTypes =
    
    [manager GET:@"http://bitebc.ca/api/api/city.php" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *result = responseObject;
        cityName = @"Select city";
        [cityArray addObject:cityName];
        cityid = @" ";
        [cityIDArray addObject:cityid];
        for (int i =0 ; i<result.count; i++) {
            
           cityName = [[result objectAtIndex:i] valueForKey:@"name"];
            
            [cityArray addObject:cityName];
            cityid = [[result objectAtIndex:i] valueForKey:@"id"];
            [cityIDArray addObject:cityid];
        }
        [self hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self hideLoadingView];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Server Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [self hideLoadingView];

    }];
    
     /*[self showLoadingView];
    NSString *str = [NSString stringWithFormat:@"http://bitebc.ca/api/api/city.php"];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
   
    NSLog(@"About to send req %@",req.URL);
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data ==nil|| data==(id)[NSNull null]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Server Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
        } else{
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
        NSLog(@"%@",result);
        for (int i =0 ; i<result.count; i++) {
            
            NSString *cityName = [[result objectAtIndex:i] valueForKey:@"name"];
            
            [cityArray addObject:cityName];
            NSString *cityid = [[result objectAtIndex:i] valueForKey:@"id"];
            [cityIDArray addObject:cityid];
        }
        
            [self hideLoadingView];

        }
        
    }];*/
   
    
}

-(void)submit_Button{
    
    if (city.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please select city." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    } else{
    [self showLoadingView];
         [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"SelctedCity"];
    checkString = @"1";
    
    NSString *post = [NSString stringWithFormat:@"customerid=%@&city=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],city.text];
    
    [[NSUserDefaults standardUserDefaults]setValue:city_id forKey:@"CITYID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [[NSUserDefaults standardUserDefaults]setValue:city.text forKey:@"city"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    NSLog(@"%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@customercity.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    }
}


@end
