//
//  TwillioIntegrationViewController.m
//  BiteBC
//
//  Created by brst on 11/8/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "TwillioIntegrationViewController.h"
#import "MBProgressHUD.h"
#import "RestaurantTableViewController.h"
#import "DetailPageTableViewController.h"
#import "SettingTableViewController.h"
#import "NearMeViewController.h"
#import "BiteBCMainViewController.h"

#import "AppDelegate.h"
#import "LoginViewController.h"

static int emailMode;
static int tag=0;
static int count=0;
@interface TwillioIntegrationViewController ()

@end

@implementation TwillioIntegrationViewController
@synthesize  profileImage;
#pragma mark - viewDidLoad Method

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"verify" forKey:@"verifyScreen"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"]);
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"] isEqualToString:@"fbMode"])
    {
        
    }
    else
    {
        profileImage=[self imageWithImage:profileImage convertToSize:CGSizeMake(250,150)];
        base64String=[self encodeToBase64String:profileImage];
        NSLog(@"%@",base64String);
        
    }
       [self verificationCodeMatch];
    // Do any additional setup after loading the view.
}


-(void)verificationCodeMatch
{
    UIView * transparentView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    transparentView.backgroundColor=[UIColor blackColor];
    transparentView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"login.png"]];
    
    transparentView.alpha=1.0;
    [self.view addSubview:transparentView];
    UILabel * tittleLbl;
    
    
      if (!IS_IPHONE_5)
    {
        tittleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 200, self.view.frame.size.width-20, 90)];
        tittleLbl.font=[UIFont boldSystemFontOfSize:15];
    }else
    {
        tittleLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 230, self.view.frame.size.width-40, 90)];
        tittleLbl.font=[UIFont boldSystemFontOfSize:15];
    }
    
    tittleLbl.text=@"We have sent a verification code to your mobile number. Please enter that code below to complete registration.";
    tittleLbl.textAlignment=NSTextAlignmentCenter;
    tittleLbl.numberOfLines=3;
    tittleLbl.adjustsFontSizeToFitWidth=YES;
    tittleLbl.textColor=[UIColor whiteColor];
    
    [transparentView addSubview:tittleLbl];
    
     if (!IS_IPHONE_5)
     {
         self.txtVerificationField=[[UITextField alloc]initWithFrame:CGRectMake(15,  tittleLbl.frame.origin.y + tittleLbl.frame.size.height+20 , self.view.frame.size.width-30, 35)];

     }
     else
     {
         self.txtVerificationField=[[UITextField alloc]initWithFrame:CGRectMake(15,  tittleLbl.frame.origin.y + tittleLbl.frame.size.height+40 , self.view.frame.size.width-30, 35)];

     }
    
    UIView *viewPadding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txtVerificationField.leftView = viewPadding6;
    self.txtVerificationField.leftViewMode = UITextFieldViewModeAlways;
    self.txtVerificationField.layer.cornerRadius=4;
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithVerification)],nil];
    [numberToolbar sizeToFit];
    self.txtVerificationField.inputAccessoryView = numberToolbar;
    self.txtVerificationField.keyboardType=UIKeyboardTypeNumberPad;
    self.txtVerificationField.borderStyle=UITextBorderStyleNone;
    self.txtVerificationField.backgroundColor=[UIColor whiteColor];
    self.txtVerificationField.layer.borderWidth=1.0;
    self.txtVerificationField.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.txtVerificationField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Enter verification code"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    self.txtVerificationField.delegate=self;
    
    [transparentView addSubview:self.txtVerificationField];
    
    UIButton * doneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame=CGRectMake(18, self.txtVerificationField.frame.origin.y + self.txtVerificationField.frame.size.height+15 , self.view.frame.size.width-36,32);
    [doneBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneBtn.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    doneBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15.5];
    doneBtn.layer.borderWidth=1.0;
    doneBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    
    [doneBtn addTarget:self action:@selector(submit_Btn_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [transparentView addSubview:doneBtn];
    
    UIButton*  report_Isuue_Btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [report_Isuue_Btn setFrame:CGRectMake(20,  doneBtn.frame.origin.y + doneBtn.frame.size.height+15, doneBtn.frame.size.width/2, 20)];
    [report_Isuue_Btn addTarget:self action:@selector(report_Isuue_BtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [report_Isuue_Btn setTitle:@"Report an Issue" forState:UIControlStateNormal];
    report_Isuue_Btn.titleLabel.font=[UIFont systemFontOfSize:14];
    report_Isuue_Btn.titleLabel.textAlignment=NSTextAlignmentRight;
    report_Isuue_Btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    report_Isuue_Btn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [report_Isuue_Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [transparentView addSubview:report_Isuue_Btn];
    
     NSString * value=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"count"]];
    NSLog(@"%@",value);
     //PLACE-HOLDER BUTTON
    if ([value isEqualToString:@"5"])
    {
        
    }else
    {
        resendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [resendBtn setFrame:CGRectMake(report_Isuue_Btn.frame.origin.x + report_Isuue_Btn.frame.size.width,  doneBtn.frame.origin.y + doneBtn.frame.size.height+15,doneBtn.frame.size.width/2, 20)];
        [resendBtn addTarget:self action:@selector(resendBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        [resendBtn setTitle:@"Resend Code" forState:UIControlStateNormal];
        resendBtn.titleLabel.font=[UIFont systemFontOfSize:14];
        resendBtn.titleLabel.textAlignment=NSTextAlignmentRight;
        resendBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
        resendBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
        [resendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [transparentView addSubview:resendBtn];

    }
    
    
//    UIButton * homeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//     if (!IS_IPHONE_5)
//     {
//         homeBtn.frame=CGRectMake((self.view.frame.size.width-35)/2,resendBtn.frame.origin.y + resendBtn.frame.size.height+10,35,35);
//
//     }else
//     {
//         homeBtn.frame=CGRectMake((self.view.frame.size.width-40)/2,resendBtn.frame.origin.y + resendBtn.frame.size.height+35 ,40,40);
//
//     }
//    [homeBtn setImage:[UIImage imageNamed:@"hm.png"] forState:UIControlStateNormal];
//    //[doneBtn setTitle:@"Back" forState:UIControlStateNormal];
//    //[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    //doneBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15.5];
//    [homeBtn addTarget:self action:@selector(home_Btn_Pressed) forControlEvents:UIControlEventTouchUpInside];
//    [transparentView addSubview:homeBtn];
//

    
}
#pragma mark - Home_Btn_Pressed Method

-(void)report_Isuue_BtnPressed
{
    NSLog(@"issue btn");
    MFMailComposeViewController*mailComposer;
    if ([MFMailComposeViewController canSendMail])
    {
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Problem in receiving SMS"];
        
        NSArray* rec=[[NSArray alloc]initWithObjects:@"info@bitebc.ca", nil];
        [mailComposer setToRecipients:rec];
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    if (result)
    {
        
        NSLog(@"%u",result);
//            UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Mail sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
    }
    
    else if (error)
    {
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Mail not send" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

//-(void)home_Btn_Pressed
//{
//    
//    NSLog(@"before = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
//    NSLog(@"before = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
//    NSLog(@"before = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
//    NSLog(@"before = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"]);
//    NSLog(@"before = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"]);
//   
//    [[NSUserDefaults standardUserDefaults]setValue:@"notverify" forKey:@"verifyScreen"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//
//            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"mail"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//    
//            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"first"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//    
//            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"last"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//    
//    
//    
//            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"password"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"cell_number"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//    
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"verifyScreen"]);
//
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
//    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"]);
//    
//    BiteBCMainViewController* biteBc=[[BiteBCMainViewController alloc]init];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    [self.navigationController pushViewController:biteBc animated:YES];
//
//
//}
#pragma mark - Submit_Btn Method
-(void)submit_Btn_Pressed
{
    
    
    NSLog(@"email is = %@  ",[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]);
    
    NSString * mynumber=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"codeIs"]];
     NSLog(@"phone number is = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"]);

       if ([self.txtVerificationField.text isEqualToString:mynumber])
    {
  
        [[NSUserDefaults standardUserDefaults]setValue:@"verified" forKey:@"verifyScreen"];
        [[NSUserDefaults standardUserDefaults]synchronize];
      
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]isEqualToString:@"fbMode"])
        {
            emailMode=1;
            [self postDataFacebook];
            
        }
        else
        {
            emailMode=0;
            [self postData];

        }
        
        
//        LoginViewController * login=[[LoginViewController alloc]init];
//        [self presentViewController:login animated:YES completion:nil];
        
     }else
     {
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter correct verification code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];

     }
    
}
-(void)resendBtnPressed
{
 //  [self dismissViewControllerAnimated:YES completion:nil];
    [self customAlert];
}
#pragma mark - CustomAlert Method
-(void)customAlert
{
    customView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height)];
    
    customView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    // self.view.alpha = 0.6;
    [self.view addSubview:customView];
    
    UIView * alertview=[[UIView alloc]initWithFrame:CGRectMake(15,  self.view.frame.size.height/2-90, self.view.frame.size.width-30,175)];
    alertview.layer.cornerRadius=8.0;
    alertview.layer.borderColor = [UIColor whiteColor].CGColor;
    alertview.layer.borderWidth = 3.0;
    alertview.backgroundColor=[UIColor colorWithRed:49.0/255 green:124.0/255 blue:119.0/255 alpha:1.0];
    
    [customView addSubview:alertview];
    
    UILabel* lbl_free=[[UILabel alloc]initWithFrame:CGRectMake(15, 10,alertview.frame.size.width-30, 60)];
    lbl_free.text=@"Please Enter Your Cell Phone Number Below To Receive Your Verification Code.";
    lbl_free.textColor=[UIColor whiteColor];
    lbl_free.adjustsFontSizeToFitWidth=YES;
    lbl_free.numberOfLines=3;
    lbl_free.textAlignment=NSTextAlignmentCenter;
    lbl_free.font=[UIFont fontWithName:@"Helvetica Neue Light" size:11];
    lbl_free.textAlignment=NSTextAlignmentCenter;
    [alertview addSubview:lbl_free];
    
    self.txt_Mobile_Number=[[UITextField alloc]initWithFrame:CGRectMake(15,  lbl_free.frame.origin.y + lbl_free.frame.size.height+15 , alertview.frame.size.width-30, 35)];
    UIView *viewPadding6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    self.txt_Mobile_Number.leftView = viewPadding6;
    self.txt_Mobile_Number.leftViewMode = UITextFieldViewModeAlways;
    self.txt_Mobile_Number.layer.cornerRadius=2;
    self.txt_Mobile_Number.tag=1;
    self.txt_Mobile_Number.returnKeyType=UIReturnKeyDone;
    self.txt_Mobile_Number.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    self.txt_Mobile_Number.borderStyle=UITextBorderStyleNone;
    self.txt_Mobile_Number.backgroundColor=[UIColor whiteColor];
    self.txt_Mobile_Number.layer.borderWidth=.8;
    self.txt_Mobile_Number.layer.borderColor=[UIColor blackColor].CGColor;
    self.txt_Mobile_Number.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Enter your phone number"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:11]
                                                 }
     ];
    self.txt_Mobile_Number.delegate=self;
    // self.txt_Mobile_Number.layer.masksToBounds = NO;
    
    self.txt_Mobile_Number.layer.borderColor = [UIColor colorWithRed:49.0/255 green:124.0/255 blue:119.0/255 alpha:1.0].CGColor;
    self.txt_Mobile_Number.layer.borderWidth = 1.0;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 3;
    border.borderColor =  [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0,  self.txt_Mobile_Number.frame.size.height - borderWidth,  self.txt_Mobile_Number.frame.size.width,  self.txt_Mobile_Number.frame.size.height);
    border.borderWidth = borderWidth;
    self.txt_Mobile_Number.layer.shadowOpacity = 1.0f;
    [self.txt_Mobile_Number.layer addSublayer:border];
    
    self.txt_Mobile_Number.layer.masksToBounds = YES;
    [alertview addSubview:self.txt_Mobile_Number];
    
    UIView * lineView=[[UIView alloc]initWithFrame:CGRectMake( 0,  self.txt_Mobile_Number.frame.origin.y + self.txt_Mobile_Number.frame.size.height+20, alertview.frame.size.width, .7)];
    lineView.backgroundColor=[UIColor lightGrayColor];
    // [alertview addSubview:lineView];
    
    UIButton*  okBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [okBtn setFrame:CGRectMake(alertview.frame.size.width/2 - 35,  self.txt_Mobile_Number.frame.origin.y + self.txt_Mobile_Number.frame.size.height+8, 70, 34)];
    [okBtn addTarget:self action:@selector(okBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [okBtn setTitle:@"OK" forState:UIControlStateNormal];
    okBtn.titleLabel.font=[UIFont boldSystemFontOfSize:19];
    okBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    okBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    okBtn.layer.borderWidth = 1.0;
    okBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    okBtn.layer.cornerRadius = 5.0;
    okBtn.backgroundColor = [UIColor clearColor];
    [alertview addSubview:okBtn];
}
-(void)okBtnPressed
{
    
    
    if (self.txt_Mobile_Number.text.length<10)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid cell number with country code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }else
    {
        
        count++;
        [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInt:count] forKey:@"count"];
        [[NSUserDefaults standardUserDefaults]synchronize];
     
        if (count==5)
        {
            resendBtn.hidden=YES;
        }
        [self showLoadingView];
         [customView removeFromSuperview];
        [[NSUserDefaults standardUserDefaults]setValue:self.txt_Mobile_Number.text forKey:@"cell_number"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self generateRandomCode];
    }
}
-(void)doneWithVerification
{
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    [self.view setFrame:frame1];
    

  [self.txtVerificationField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!IS_IPHONE_5)
    {
        CGRect frame1=self.view.frame;
        frame1.origin.y=-120;
        [self.view setFrame:frame1];
    }else
    {
    CGRect frame1=self.view.frame;
    frame1.origin.y=-90;
    [self.view setFrame:frame1];
    }
     
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    [self.view setFrame:frame1];
    

   
    [textField resignFirstResponder];
    return YES;
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txt_Mobile_Number resignFirstResponder];
    [self.txtVerificationField resignFirstResponder];
   
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    [self.view setFrame:frame1];
    

    [customView removeFromSuperview];
    
    
}
-(void)postDataFacebook
{
    NSLog(@"email =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
    NSLog(@"first name is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
    NSLog(@"password is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]);
    NSLog(@"lastname is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
    
    [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&platform=ios&device_id=%@&cell_number=%@&section=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"],[[NSUserDefaults standardUserDefaults]valueForKey:@"last"],[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"],[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],@"facebook" ];
    
    
//    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&profileimg=%@@platform=%@&device_id=%@&cell_number=%@&section=%@",@"Alex",@"Mei",@"alex.turney@yahoo.co.uk",@"alex.turney@yahoo.co.uk",[[NSUserDefaults standardUserDefaults]valueForKey:@"base64"],@"ios",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],@"facebook" ];
//
    
    NSLog(@"posting value is = %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@registeration.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}






-(void)postcustomerID
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
}


#pragma mark - PostData Method
-(void)postData
{
    NSLog(@"email =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
    NSLog(@"first name is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"]);
    NSLog(@"password is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]);
    NSLog(@"lastname is =%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]);
    
 
    [[NSUserDefaults standardUserDefaults]setObject:base64String forKey:@"base64"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&profileimg=%@&platform=%@&device_id=%@&cell_number=%@&section=%@&city=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"],[[NSUserDefaults standardUserDefaults]valueForKey:@"last"],[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"],base64String,@"ios",[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],[[NSUserDefaults standardUserDefaults]valueForKey:@"cell_number"],@"email",[[NSUserDefaults standardUserDefaults]valueForKey:@"city"]];
    NSLog(@"%@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *urlString=[NSString stringWithFormat:@"%@registeration.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(5, 0, size.width,size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
- (NSString *)encodeToBase64String:(UIImage *)image
{
    NSData *data2 = UIImagePNGRepresentation(image);
    
    return base64String=[data2 base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
}

 #pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (emailMode==0)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
        dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"JSON=  \n%@",dict);
        
        NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        //     NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
        //
        //    NSString * imageis =[NSString stringWithFormat:@"%@",[dict valueForKey:@"profileimg_url"]];
        if  ([sucess isEqualToString:@"1"])
            
        {
            NSString * thnxMessage=[NSString stringWithFormat:@"Your login details are - Email: %@ Password: %@. These have also been sent to your email.",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"],[[NSUserDefaults standardUserDefaults]valueForKey:@"password"]];
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thanks For Signing Up!" message:thnxMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            LoginViewController * login=[[LoginViewController alloc]init];
            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            [self.navigationController pushViewController:login animated:YES];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"city"] forKey:@"city"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"cityid"] forKey:@"CITYID"];

            
        }
        else   {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        [self hideLoadingView];

    }
    else if (emailMode==1)
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=  \n%@",dict);
    
    if (tag==1)
    {
        NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"status"]];
        
        NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
        
        NSString* image_profile=[NSString stringWithFormat:@"%@",[dict valueForKey:@"profileimg_url"]];
        NSLog(@"%@",image_profile);
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"daysleft"] forKey:@"daysleft"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"package_name"] forKey:@"pack"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"planid"] forKey:@"planid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if  ([sucess isEqualToString:@"0"] && [status isEqualToString:@"not purchased"])
        {
            SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
            [self.navigationController pushViewController:selectPlan animated:YES];
        }
        else if([daysleft isEqualToString:@"0"])
        {
            if ([[dict valueForKey:@"status"] isEqualToString:@"paused"])
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your membership plan has expired" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your Bite membership has expired please purchase a new membership plan." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
                [self.navigationController pushViewController:selectPlan animated:YES];
                
            }
            
        }
        
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self tabBar];
            [self hideLoadingView];
        }
        [self hideLoadingView];
        
    }
    
    
    if (tag==0)
    {
        NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        if ([status isEqualToString:@"1"])
        {
            tag=1;
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"sessionid"] forKey:@"sessionId"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"customerid"] forKey:@"customerid"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSLog(@"customerid = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
            
            [self postcustomerID];
        }
        else
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"You are already registered with this device." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
            
        }
    }
    }
    
    
    
}




- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

#pragma mark - Message sending Method
-(void)generateRandomCode
{
    int min = 10000;
    int max = 99999;
    
    int randNum = rand() % (max - min) + min;
  
    NSString* randomCode=[NSString stringWithFormat:@"Bite Confirmation Code \n %d",randNum];
    NSString * addString=[@"+1" stringByAppendingString:self.txt_Mobile_Number.text];
   addString = [addString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [self sendCode:addString randomCode:randomCode];
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_Mobile_Number.text forKey:@"cell_number"];
    [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithInt:randNum] forKey:@"codeIs"];
    [[NSUserDefaults standardUserDefaults]synchronize];
  
}

-(void)sendCode:(NSString *)toContact randomCode:(NSString *)randomCode
 {
   
    NSMutableDictionary *dict_Value = [[NSMutableDictionary alloc]init];
    [dict_Value setObject:@"+17786553948" forKey:@"From"];
    [dict_Value setObject:toContact forKey:@"To"];
    [dict_Value setObject:randomCode forKey:@"Body"];
    
    NSLog(@"DICT=%@",dict_Value);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"ACf656d70460e43f77cbbaa9bfaf535875" password:@"218b1d1ff3fd6125ff5d113aca779dbc"];
    [manager POST:sms_url2 parameters:dict_Value success:
     ^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"%@",responseObject);
         
         id status=[responseObject valueForKey:@"status"];
         NSLog(@"status=%@",status);
         if ([status isEqualToString:@"queued"])
         {
             [self hideLoadingView];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Code resent successfully !" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];

             NSLog(@"code sent");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [self hideLoadingView];
         NSLog(@"error=%@",error);
         
     }];
    
    
}
#pragma mark - UITabBarController Method
-(void)tabBar
{
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"Activity"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Restaurants";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"Activity"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"Near-me"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"Near-me"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"Search"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"Search"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"Setting"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"Setting"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
    
    
    
    
    //    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
    //    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    [self.navigationController pushViewController:self.tabBarController animated:YES];
    
    
}

#pragma mark - Loading Method
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
