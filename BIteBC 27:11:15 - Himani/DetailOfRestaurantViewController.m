//
//  DetailOfRestaurantViewController.m
//  BiteBC
//
//  Created by brst on 7/8/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "DetailOfRestaurantViewController.h"
#import "BiteBCMainViewController.h"
#import "MapViewViewController.h"
#import "NearMeViewController.h"
#import "CheckinViewController.h"
#import "NewCheckInViewController.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import <FBSDKShareKit/FBSDKShareKit.h>


static int isSubmit=0;
static int postCustmer=0;

@interface DetailOfRestaurantViewController ()
{
    NSMutableArray *arr;
    NSMutableArray *cu;
    UITextField *name_Textfield;
    UITextView *review_Textview;
    UIView* editView;
    UIView *thirdView;
    //UILabel  * avlLabel;
    
    UIButton *avlLabel;
    
}

@end

@implementation DetailOfRestaurantViewController
@synthesize imageOf_hotel;
@synthesize imageOfHotel_string;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
   
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
  //  [self detalButtonPressed];
 
}
-(void)viewWillDisappear:(BOOL)animated
{
    //[detail_View removeFromSuperview];
}
-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
    mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.backgroundColor =[UIColor clearColor];
    mainScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+160);
    mainScrollView.userInteractionEnabled  =  YES;
    mainScrollView.scrollEnabled = YES;
    
    [self.view addSubview:mainScrollView];
    
    NSLog(@"%@",self.product_id);
     weeklyArray=[[NSMutableArray alloc]init];
    avialableArray = [[NSMutableArray alloc]init];
    detailDict=[[NSMutableDictionary alloc]init];
    allRatingdict=[[NSMutableArray alloc]init];

    if (self.nameOfpRestaureant_String.length>20)
    {
        [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:13],NSFontAttributeName,nil]];
        self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];

    }else
    {
        [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:16],NSFontAttributeName,nil]];
        self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];

        
    }
        
    self.navigationItem.title=self.nameOfpRestaureant_String;
    
   
     arr=[[NSMutableArray alloc]initWithObjects:@"RESTAURANT INFORMATION",@"CUISINE",@"OFFER TYPE",@"NO. OF PEOPLE", nil];
    reviewNameArray=[[NSMutableArray alloc]initWithObjects:@"Name",@"Review",@"Ratings",nil];
    imagesArray=[[NSMutableArray alloc]init];
    
    
    
    mapButton=[UIButton buttonWithType: UIButtonTypeCustom];
        UIImage* img=[UIImage imageNamed:@"map1.png"];
    [mapButton setImage:img forState:UIControlStateNormal];
    mapButton.frame=CGRectMake(0, 0, 20, 25);
    mapButton.hidden=YES;
    
    [mapButton addTarget:self action:@selector(mapButton) forControlEvents:UIControlEventTouchUpInside];
   UIBarButtonItem * barButton=[[UIBarButtonItem alloc]initWithCustomView:mapButton];
    self.navigationItem.rightBarButtonItem=barButton;

 
    [self upperView];
    [self buttonsView];
    
    
     [self dataFetchByproductId];
    
     avlY = 125;
     avlX = 125;
}

#pragma mark -MapButton Method
-(void)mapButton
{
    
    MapViewViewController * mapView=[[MapViewViewController alloc]init];
    mapView.zipcode_str=zipcode_string;
    mapView.entityId_str=self.product_id;
     mapView.nameOfRest=self.nameOfpRestaureant_String;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
   
    [self.navigationController pushViewController:mapView animated:YES];
    
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


#pragma mark - DataFetch Meyhod

-(void)dataFetchByproductId
{
    
    isSubmit=0;
    [self showLoadingView];
 
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"productid=%@",self.product_id];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@products.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
  
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
 }
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    mapButton.hidden=NO;
   
    
    NSLog(@"product_id = %@",self.product_id);
    [self hideLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"detail data of res is  = \n %@",dict);
   
    if (postCustmer==1)
    {
        
        postCustmer=0;
        
        NSString* success=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];

        NSString* planStatus=[NSString stringWithFormat:@"%@",[dict valueForKey:@"status"]];
        
//        NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
        
        if ([success isEqualToString:@"0"] && [planStatus isEqualToString:@"paused"])
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your membership plan has expired" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"fbStatus"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            BiteBCMainViewController* biteBC=[[BiteBCMainViewController alloc]init];
            UINavigationController * navigate=[[UINavigationController alloc]initWithRootViewController:biteBC];
            [self.navigationController presentViewController:navigate animated:NO completion:nil];
 
        }
        else if  ([success isEqualToString:@"0"] && [planStatus isEqualToString:@"not purchased"])
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your membership has been expired. Please re-purchase your membership plan." message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
            
            UINavigationController *navigationController =
            [[UINavigationController alloc] initWithRootViewController:selectPlan];
                [self.navigationController presentViewController:navigationController animated:NO completion:nil];
            
        }
        else
        {
//            CheckinViewController *checkInView=[[CheckinViewController alloc]initWithNibName:@"CheckinViewController" bundle:nil];
//            checkInView.restid=self.product_id;
//            checkInView.expiry_Date=[dict valueForKey:@"end_date"];
//            checkInView.hotelNAME=self.nameOfpRestaureant_String;
//            checkInView.profileImage_String=[dict valueForKey:@"profileimg_url"];
//            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//            
//            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"daysleft"] forKey:@"daysleft"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            
//            self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//            [self.navigationController pushViewController:checkInView animated:YES];
            
            NewCheckInViewController *VCont = [[NewCheckInViewController alloc]init];
            VCont.restid  = self.product_id;
            VCont.expiry_Date = [dict valueForKey:@"end_date"];
            VCont.hotelNAME = self.nameOfpRestaureant_String;
            VCont.profileImage_String = [dict valueForKey:@"profileimg_url"];
            VCont.memberShip = [dict valueForKey:@"package_name"];
             VCont.cust_id = [dict valueForKey:@"customerid"];
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"daysleft"] forKey:@"daysleft"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
            
                        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                        [self.navigationController pushViewController:VCont animated:YES];
            
            
         }
        
    }

    if(isSubmit==0)
    {
        
        detailDict=[dict valueForKeyPath:@"product_detail.detail"];
        imagesArray=[dict valueForKeyPath:@"product_detail.images"];
        
        allRatingdict=[dict  valueForKeyPath:@"product_detail.allratings"];
        
        imageStr=[detailDict valueForKey:@"image_url"];
        offerAvl=[detailDict valueForKey:@"offer_available"];
        offerPeriod=[detailDict valueForKey:@"offer_period"];
        noOfPeople=[detailDict valueForKey:@"number_people"];
        websiteName=[detailDict valueForKey:@"website_link"];
        numPeople=[detailDict valueForKey:@"num_of_people"];
        desc_Str=[detailDict valueForKey:@"description"];
        phone_Num=[detailDict valueForKey:@"phone_number"];
        NSLog(@"%@",phone_Num);
         booking_NUM=[NSString stringWithFormat:@"%@",[detailDict valueForKey:@"phone_number"]];
        NSLog(@"%@",booking_NUM);
        weeklyArray=[detailDict valueForKey:@"non_available"];
        avialableArray = [detailDict valueForKey:@"offer_period"];
          self.monthly_str=[detailDict valueForKey:@"monthly"];
          zipcode_string=[detailDict valueForKey:@"zipcode"];
        calling_str=[detailDict valueForKey:@"cellphone"];
        
        NSLog(@"weeklyArray =\n %@",weeklyArray);
 
        NSArray* name=[self.cuisine_String  componentsSeparatedByString:@","];
        NSLog(@"cuisne =\n %@",name);
        
        cu=[[NSMutableArray alloc]init];
        
        
          [cu addObject:@[desc_Str]];
        
        
        if (name == nil || name == (id)[NSNull null])
        {
            [cu addObject:@[@" N.A"]];
        }
        else
        {
            
                [cu addObject:name];
        }
        
      
        //[cu addObject:@[name[0]]];
        // [cu addObject:@[@" Asian",@" French",@" British"]];
        
        if ([offerAvl isEqual:[NSNull null]])
        {
            [cu addObject:@[@"N.A"]];
        }
        else
        {
            if ([offerAvl isEqualToString:@"31"])
            {
                
                
                [cu addObject:@[@"2 for 1 meals"]];
            }
            else if([offerAvl isEqualToString:@"32"]){
                
                 [cu addObject:@[@"50% off food bill"]];
                
                
            }
            else if ([offerAvl isEqualToString:@"100"]){
                [cu addObject:@[@"2 for 1"]];
            }
            
            else if([offerAvl isEqualToString:@"102"])
            {
                [cu addObject:@[@"40% off"]];
            }
            else if([offerAvl isEqualToString:@"103"])
            {
                [cu addObject:@[@"30% off"]];
            }
            
            else if([offerAvl isEqualToString:@"104"])
            {
                [cu addObject:@[@"40% off food bill"]];
            }
            else if([offerAvl isEqualToString:@"105"])
            {
                [cu addObject:@[@"30% off food bill"]];
            }
            
            
            else
            {
               [cu addObject:@[@"50% off"]];
                
            }
            
            
            
        }
        
        if ([noOfPeople isEqual:[NSNull null]])
        {
            [cu addObject:@[@" 0"]];
        }else
        {
            
            [cu addObject:@[[NSString stringWithFormat:@"%@" ,noOfPeople]]];
        }
        
      
        
        
      
        [self detalButtonPressed];
       

    }
       isSubmit=1;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self hideLoadingView];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}



-(void)buttonsView
{
    
    UIView* buttonView=[[UIView alloc]initWithFrame:CGRectMake(0, 90, 320, 40)];
    buttonView.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3608 blue:0.3490 alpha:1.0];
    buttonView.layer.borderWidth=1.5;
    buttonView.layer.borderColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0].CGColor;
    //[self.view addSubview:buttonView];
    
    [mainScrollView addSubview:buttonView];
    
    
      detailButton=[UIButton buttonWithType: UIButtonTypeSystem];
        [detailButton setTitle:@"DETAILS" forState:UIControlStateNormal];
        [detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       // detailButton.backgroundColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
        detailButton.frame=CGRectMake(20,5,80,30);
        detailButton.titleLabel.font=[UIFont boldSystemFontOfSize:11];
 //        button.layer.cornerRadius=5;
//        button.layer.borderWidth=1.5;
    [detailButton addTarget:self action:@selector(detalButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [buttonView addSubview:detailButton];

    
    galleryButton=[UIButton buttonWithType: UIButtonTypeSystem];
    [galleryButton setTitle:@"GALLERY" forState:UIControlStateNormal];
    [galleryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //galleryButton.backgroundColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    galleryButton.frame=CGRectMake(120,5,80,30);
    galleryButton.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    [galleryButton addTarget:self action:@selector(galleryButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    //        button.layer.cornerRadius=5;
    //        button.layer.borderWidth=1.5;
    [buttonView addSubview:galleryButton];
    
    reviewsButton=[UIButton buttonWithType: UIButtonTypeSystem];
    [reviewsButton setTitle:@"REVIEWS" forState:UIControlStateNormal];
      reviewsButton.frame=CGRectMake(220,5,80,30);
    [reviewsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
     reviewsButton.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    [reviewsButton addTarget:self action:@selector(reviewsButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    //        button.layer.cornerRadius=5;
    //        button.layer.borderWidth=1.5;
    [buttonView addSubview:reviewsButton];
    
}

-(void)upperView
{
    UIView* headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 90)];
    headerView.backgroundColor=[UIColor clearColor];
   // [self.view addSubview:headerView];
    [mainScrollView addSubview:headerView];
    hotelImageView = [[UIImageView alloc] initWithFrame:CGRectMake(7,15, 60,60)];
    hotelImageView.layer.cornerRadius=8;
    hotelImageView.layer.borderWidth=1.0;
    hotelImageView.layer.borderColor=[UIColor whiteColor].CGColor;
    hotelImageView.layer.masksToBounds=YES;
    hotelImageView.backgroundColor=[UIColor lightGrayColor];
    NSLog(@"%@",self.imageOfHotel_string);
   
    
    [hotelImageView sd_setImageWithURL:[NSURL URLWithString:imageOfHotel_string] placeholderImage:[UIImage imageNamed:@"ic-launcher.png"] options:SDWebImageRefreshCached];
 
    hotelImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [headerView addSubview:hotelImageView];

    
    adressLabel = [[UILabel alloc] initWithFrame:CGRectMake(75,16,120,65)];
    adressLabel.font = [UIFont boldSystemFontOfSize:12.3];
    adressLabel.numberOfLines=4;
    adressLabel.backgroundColor=[UIColor clearColor];
    adressLabel.textColor = [UIColor whiteColor];
    adressLabel.text=self.adressOfRestaurant_String;
    adressLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [headerView addSubview:adressLabel];
    
    
    checkinButton=[UIButton buttonWithType: UIButtonTypeSystem];
    [checkinButton setTitle:@"CHECKIN" forState:UIControlStateNormal];
    [checkinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    checkinButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3608 blue:0.3490 alpha:1.0];
    checkinButton.frame=CGRectMake(210,30,100,30);
    checkinButton.layer.cornerRadius=5;
    checkinButton.layer.borderWidth=1.5;
    checkinButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    checkinButton.contentEdgeInsets=UIEdgeInsetsMake(2, 5, 0, 3);
    checkinButton.layer.borderColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0].CGColor;
    checkinButton.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    [checkinButton addTarget:self action:@selector(checkinButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:checkinButton];
    
    UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(75,7, 15,15)];
         imageView.layer.masksToBounds=YES;
    imageView.image=[UIImage imageNamed:@"checkin.png"];
    [checkinButton addSubview:imageView];

    
}
#pragma mark - Three-Main Views
-(void)detailView
{
    [mainScrollView setScrollEnabled:YES];
    detail_View=[[UIView alloc]initWithFrame:CGRectMake(0, 132, 320, 370)];
    
    detail_View.backgroundColor=[UIColor clearColor];
    [mainScrollView addSubview:detail_View];
    //[self.view addSubview:detail_View];
    
    UIView* subView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 155)];
    subView.backgroundColor=[UIColor clearColor];
    [detail_View addSubview:subView];
    
    
    fbButton=[UIButton buttonWithType: UIButtonTypeCustom];
    // [fbButton setTitle:@"Facebook Share" forState:UIControlStateNormal];
    UIImage * image=[UIImage imageNamed:@"fb.png"];
    
    [fbButton setImage:image forState:UIControlStateNormal];
    fbButton.layer.masksToBounds=YES;
    //[fbButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //fbButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3608 blue:0.3490 alpha:1.0];
    fbButton.frame=CGRectMake(274,108,37,37);
    //    fbButton.layer.cornerRadius=5;
    //    fbButton.layer.borderWidth=1.5;
    //    fbButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //fbButton.contentEdgeInsets=UIEdgeInsetsMake(2, 5, 0, 3);
    //   fbButton.layer.borderColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0].CGColor;
    //fbButton.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    [fbButton addTarget:self action:@selector(fbButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [subView addSubview:fbButton];

    
    UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 100, 75)];
    imageView.backgroundColor=[UIColor lightGrayColor];
    imageView.layer.borderWidth=1.0;
    imageView.layer.borderColor=[UIColor whiteColor].CGColor;
    imageView.layer.cornerRadius=2;
    
    imageView.layer.masksToBounds=YES;
     [imageView sd_setImageWithURL:[NSURL URLWithString:imageOfHotel_string] placeholderImage:[UIImage imageNamed:@"ic-launcher.png"] options:SDWebImageRefreshCached];
    //[self imageDownloader:imageOfHotel_string imageView:imageView];
   // imageView.image=self.imageOf_hotel;
    

    [subView addSubview:imageView];
    
    firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(125,10,100,18)];
    firstLabel.textAlignment=NSTextAlignmentCenter;
    firstLabel.font = [UIFont boldSystemFontOfSize:11.5];
    firstLabel.layer.borderWidth=1.0;
    firstLabel.layer.borderColor=[UIColor whiteColor].CGColor;
    firstLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    firstLabel.textColor = [UIColor whiteColor];
   
//    if ([offerAvl isEqualToString:@"31"])
//    {
//        firstLabel.text=@"2 For 1";
//    }
//    else
//    {
//        firstLabel.text=@"50%";
//    }
    
    
    if ([offerAvl isEqualToString:@"31"])
    {
        
        
        firstLabel.text = @"2 for 1 meals";
    }
    else if([offerAvl isEqualToString:@"32"]){
        
         firstLabel.text = @"50% off food bill";
        
        
    }
    else if ([offerAvl isEqualToString:@"100"]){
         firstLabel.text =@"2 for 1";
    }
    
    else if([offerAvl isEqualToString:@"102"])
    {
         firstLabel.text =@"40% off";
    }
    else if([offerAvl isEqualToString:@"103"])
    {
         firstLabel.text = @"30% off";
    }
    
    else if([offerAvl isEqualToString:@"104"])
    {
         firstLabel.text = @"40% off food bill";
    }
    else if([offerAvl isEqualToString:@"105"])
    {
        firstLabel.text =@"30% off food bill";
    }
    
    
    else
    {
         firstLabel.text = @"50% off";
        
    }


         //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [subView addSubview:firstLabel];
    if (numPeople==nil || numPeople==(id)[NSNull null])
    {
        
    }
    else
        
    {
        secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(230,10,60,18)];
        secondLabel.textAlignment=NSTextAlignmentCenter;
        secondLabel.layer.borderWidth=1.0;
        secondLabel.layer.borderColor=[UIColor whiteColor].CGColor;
        secondLabel.font = [UIFont boldSystemFontOfSize:12];
        secondLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        secondLabel.textColor = [UIColor whiteColor];
        UIImageView* imageV=[[UIImageView alloc]initWithFrame:CGRectMake(2.5, 4, 10.5, 10.5)];
        imageV.image=[UIImage imageNamed:@"pupit.png"];
        imageV.backgroundColor=[UIColor clearColor];

        [secondLabel addSubview:imageV];
        
        NSString * str=[NSString stringWithFormat:@"   X %@ ",numPeople];
        secondLabel.text=str;
        [subView addSubview:secondLabel];
        
    }

    
    firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    
    
    if (self.monthly_str==(id)[NSNull null] || self.monthly_str==nil)
    {
        
    }
    else
    {
        fourthLabel = [[UILabel alloc] initWithFrame:CGRectMake(125,33,40,18)];
        fourthLabel.textAlignment=NSTextAlignmentCenter;
        fourthLabel.font = [UIFont boldSystemFontOfSize:11];
        fourthLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
        fourthLabel.layer.borderWidth=1.0;
        fourthLabel.layer.borderColor=[UIColor whiteColor].CGColor;
        fourthLabel.textColor = [UIColor whiteColor];
        
        
        self.monthly_str= [self.monthly_str substringToIndex:3];
        NSString *new=[NSString stringWithFormat:@"X %@",self.monthly_str];
        
        fourthLabel.text=new;
        
        [subView addSubview:fourthLabel];
        
    }
    
    if (calling_str==(id)[NSNull null])
    {
        
    }
    else
    {
        if ([calling_str isEqualToString:@"Yes"])
        {
            if (fourthLabel.frame.origin.y==0)
            {
                cellImage=[[UIImageView alloc]initWithFrame:CGRectMake(125, 31.5, 40, 20)];
                cellImage.layer.masksToBounds=YES;
                cellImage.layer.borderWidth=1.1;
                cellImage.layer.borderColor=[UIColor whiteColor].CGColor;
                cellImage.image=[UIImage imageNamed:@"key-img3.png"];
                
            }
            else{
                
            
            cellImage=[[UIImageView alloc]initWithFrame:CGRectMake(170, 31.5, 40, 20)];
            cellImage.layer.masksToBounds=YES;
            cellImage.layer.borderWidth=1.1;
            cellImage.layer.borderColor=[UIColor whiteColor].CGColor;
            cellImage.image=[UIImage imageNamed:@"key-img3.png"];
        }
            [subView addSubview:cellImage];
            
        }
        else
        {
            
        }
        
    }
    
    NSLog(@"%@",NSStringFromCGRect(fourthLabel.frame));
    
    NSString *str1 = [avialableArray objectAtIndex:0];
    NSLog(@"%@",str1);
    
    if (avialableArray==(id)[NSNull null] || weeklyArray==nil || [[avialableArray objectAtIndex:0] isEqualToString:@"non_available"])
    {
        
    }else
        
    {
        int x=125;
        int xpos=125;
        NSLog(@"%@",avialableArray);
        for (int i=0; i<avialableArray.count; i++)
        {
            
            NSLog(@"%@",NSStringFromCGRect(avlLabel.frame));
            if (i<=3)
            {
                
                    if ((fourthLabel.frame.origin.y !=0 )|| (cellImage.frame.origin.y !=0 )) {
                        avlLabel = [UIButton buttonWithType:UIButtonTypeCustom];
                        avlLabel.frame = CGRectMake(x,fourthLabel.frame.origin.y+fourthLabel.frame.size.height+5,40,18);
                    // avlLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,fourthLabel.frame.origin.y+fourthLabel.frame.size.height+5,40,18)];
                        if (fourthLabel.frame.origin.y == 0) {
                            avlLabel = [UIButton buttonWithType:UIButtonTypeCustom];
                            avlLabel.frame = CGRectMake(x,cellImage.frame.origin.y+cellImage.frame.size.height+3,40,18);
                            //avlLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,cellImage.frame.origin.y+cellImage.frame.size.height+3,40,18)];
                        }
                    }
                    else{
                        avlLabel = [UIButton buttonWithType:UIButtonTypeCustom];
                        avlLabel.frame = CGRectMake(x,firstLabel.frame.origin.y+firstLabel.frame.size.height+5,40,18);
                        //avlLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,firstLabel.frame.origin.y+firstLabel.frame.size.height+5,40,18)];
                    }
                
                
            }
            else
            {
                if (i>=4) {
                    NSLog(@"%@",NSStringFromCGRect(avlLabel.frame));
                    if ( i==4) {
                        
                        x = 125;
                        xpos =125;
                        avlLabel = [[UIButton alloc]initWithFrame: CGRectMake(x,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18)];
                        //avlLabel.frame = CGRectMake(x,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18);
                       // avlLabel =[[UILabel alloc] initWithFrame:CGRectMake(x,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18)];
                          NSLog(@"%@",NSStringFromCGRect(avlLabel.frame));
                        avlX = x + 40;
                        
                    }
                    if ( i == 5) {
                        x = 125;
                        //xpos =125;
                        avlLabel = [[UIButton alloc]initWithFrame:CGRectMake(avlX+5,avlLabel.frame.origin.y,40,18)];
                        //avlLabel.frame = CGRectMake(avlX+5,avlLabel.frame.origin.y,40,18);
                        //avlLabel =[[UILabel alloc] initWithFrame:CGRectMake(avlX+5,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18)];
                        
                         avlX = avlX + 40;
                        
                    }
                    if ( i == 6) {
                        x = 125;
                        //xpos =125;
                        
                        avlLabel = [[UIButton alloc]initWithFrame:CGRectMake(avlX+5,avlLabel.frame.origin.y,40,18)];
                        //avlLabel.frame = CGRectMake(avlX+5,avlLabel.frame.origin.y,40,18);
                        //avlLabel =[[UILabel alloc] initWithFrame:CGRectMake(avlX+5,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18)];
                    }
                    
                    
                    
                }
                
               // avlLabel = [UIButton buttonWithType:UIButtonTypeCustom];
               // avlLabel.frame = CGRectMake(xpos,54,40,18);
                xpos=xpos+avlLabel.frame.size.width+5;
               // avlLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos,54,40,18)];
                //xpos=xpos+avlLabel.frame.size.width+5;
                
            }
            
          
           // [_button setCenter:CGPointMake(128.f, 128.f)]; // SET the values for your wishes
           // [_button setClipsToBounds:false];
//            [avlLabel setBackgroundImage:[UIImage imageNamed:@"ic_white_tick.png"] forState:UIControlStateNormal]; // SET the image name for your wishes
//            [_button setTitle:@"Button" forState:UIControlStateNormal];
//            [_button.titleLabel setFont:[UIFont systemFontOfSize:24.f]];
//            [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal]; // SET the colour for your wishes
//            [_button setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted]; // SET the colour for your wishes
            // SET the values for your wishes
            //[_button addTarget:self action:@selector(buttonTouchedUpInside:) forControlEvents:UIControlEventTouchUpInside]; // you can ADD the action to the button as well like
            
           // avlLabel.textAlignment=NSTextAlignmentCenter;
            
            avlLabel.titleLabel.font = [UIFont boldSystemFontOfSize:10];
            avlLabel.layer.borderWidth=1.0;
           
            avlLabel.layer.borderColor=[UIColor whiteColor].CGColor;
            avlLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
            avlLabel.titleLabel.textColor = [UIColor whiteColor];
            avlLabel.userInteractionEnabled = NO;
            
            NSString * value=[avialableArray objectAtIndex:i];
            NSLog(@"%@",value);
            NSString * string= [value substringToIndex:3];
            NSString *new=[NSString stringWithFormat:@"%@",string];
//            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//            attachment.image = [UIImage imageNamed:@"ic_white_tick.png"];
//            
//            
//            //attachment.bounds = CGRectMake(0, avlLabel.frame.size.height/2 -9, 12, 14);
//        
//            
//          NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment]; NSMutableAttributedString *myString = [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
//            
//            NSAttributedString *myText = [[NSMutableAttributedString alloc] initWithString:new];
//            [myString appendAttributedString:myText];
           
           // avlLabel.text=new;
            
            [avlLabel setImage: [UIImage imageNamed:@"ic_white_tick.png"] forState:UIControlStateNormal];
            [avlLabel setTitleEdgeInsets:UIEdgeInsetsMake(4.0, 2.0,2.0, 2.0)];
            //avlLabel setTitleEdgeInsets:uie
            [avlLabel setTitle:new forState:UIControlStateNormal];
            x=x+avlLabel.frame.size.width+5;
            [subView addSubview:avlLabel];
            
        }
        
        
        
        
        
    }
    
    
    
    //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    NSLog(@"%@",weeklyArray);
    
    NSString *str = [weeklyArray objectAtIndex:0];
    NSLog(@"%@",str);
    
     if (weeklyArray==(id)[NSNull null] || weeklyArray==nil || [[weeklyArray objectAtIndex:0] isEqualToString:@"No-restriction"])
     {
         
     }else
         
     {
         int x=125;
         int xpos=125;
         NSLog(@"%@",weeklyArray);
         for (int i=0; i<weeklyArray.count; i++)
         {
             if (i<=3)
             {
                 
                     if (avlLabel.frame.origin.y ==0)
                     {
                         
                         if ((fourthLabel.frame.origin.y !=0 )|| (cellImage.frame.origin.y !=0 ))
                         {
                             thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,fourthLabel.frame.origin.y+fourthLabel.frame.size.height+5,40,18)];
                         }
                         if (fourthLabel.frame.origin.y == 0 && cellImage.frame.origin.y ==0)
                         {
                             thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,firstLabel.frame.origin.y+firstLabel.frame.size.height+3,40,18)];
                         }
                         if (cellImage.frame.origin.y != 0) {
                             thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,cellImage.frame.origin.y+cellImage.frame.size.height+3,40,18)];
                         }
                         
                     
                     // thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,98,40,18)];
                 }else
                  
                 thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,40,18)];
                 
             }
             else
             {
                 
                 
                 if (i>=4) {
                     if (i==4)
                     {
                         x= 125;
                         xpos = 125;
                         thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,thirdLabel.frame.origin.y+thirdLabel.frame.size.height+5,40,18)];
                         
                         avlY = x+40;
                     }
                     if (i==5)
                     {
                         x= 125;
                         xpos = 125;
                         thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(avlY+5,thirdLabel.frame.origin.y,40,18)];
                         
                         avlY = avlY+40;
                     }
                    if (i == 6)
                    {
                        x= 125;
                        xpos = 125;
                        thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(avlY+5,thirdLabel.frame.origin.y,40,18)];
                        
                        //avlY = avlY+40;
                    }
                     
                 }

                 //thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,avlLabel.frame.origin.y+avlLabel.frame.size.height+5+avlLabel.frame.size.height+5,40,18)];
                 //thirdLabel = [[UILabel alloc] initWithFrame:CGRectMake(xpos,76,40,18)];
                 xpos=xpos+thirdLabel.frame.size.width+5;
                 
             }
            
             thirdLabel.textAlignment=NSTextAlignmentCenter;
             thirdLabel.font = [UIFont boldSystemFontOfSize:11];
             thirdLabel.layer.borderWidth=1.0;
             thirdLabel.layer.borderColor=[UIColor whiteColor].CGColor;
             thirdLabel.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
             thirdLabel.textColor = [UIColor whiteColor];
             
              NSString * value=[weeklyArray objectAtIndex:i];
              NSString * string= [value substringToIndex:3];
              NSString *new=[NSString stringWithFormat:@"X %@",string];
             NSLog(@"%@",new);
             thirdLabel.text=new;
             
             x=x+thirdLabel.frame.size.width+5;
             [subView addSubview:thirdLabel];
             
         }
         
         
     }
    
   
    
    
    
    
    if ((thirdLabel.frame.origin.y ==0) && (avlLabel.frame.origin.y !=0 ))
    {
        staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,avlLabel.frame.origin.y+avlLabel.frame.size.height+5,130, 14)];
    }
    else if (thirdLabel.frame.origin.y != 0)
    {
        
        staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,thirdLabel.frame.origin.y+thirdLabel.frame.size.height+5,130, 14)];
        
//        if (thirdLabel.frame.origin.y + thirdLabel.frame.size.height > imageView.frame.origin.y+imageView.frame.size.height)
//        {
//            staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,imageView.frame.origin.y+imageView.frame.size.height + thirdLabel.frame.size.height - 3,130, 14)];
//            
//            NSLog(@"Y is %f",imageView.frame.origin.y+imageView.frame.size.height);
//            
//        }
////        else if (secondLabel.frame.origin.y + secondLabel.frame.size.height < imageView.frame.origin.y+imageView.frame.size.height)
////        {
////            staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,secondLabel.frame.origin.y+secondLabel.frame.size.height + 25,130, 14)];
////            
////            NSLog(@"Y is %f",imageView.frame.origin.y+imageView.frame.size.height);
////            
////        }
//
//        else
//        {
//            staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,imageView.frame.origin.y+imageView.frame.size.height - 3 ,130, 14)];
//            NSLog(@"Y is %f",imageView.frame.origin.y+imageView.frame.size.height + 10);
//        }
//        
        
        NSLog(@"third label y = %f",thirdLabel.frame.origin.y+thirdLabel.frame.size.height);
        
       // staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,60,130, 14)];
        
        //staticStarRatingView.backgroundColor = [UIColor grayColor];
    }
    
       else if (avlLabel.frame.origin.y == 0)
       {
            
        
        if ((fourthLabel.frame.origin.y !=0 )|| (cellImage.frame.origin.y !=0 ))
        {
            
        
        if (fourthLabel.frame.origin.y == 0)
        {
           staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,cellImage.frame.origin.y+cellImage.frame.size.height+5,130, 14)];
          
        }
        else
        {
          staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,fourthLabel.frame.origin.y+fourthLabel.frame.size.height+5,130, 14)];
        }
        }
       }
      else if (fourthLabel.frame.origin.y == 0) {
             staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,firstLabel.frame.origin.y+firstLabel.frame.size.height+5,130, 14)];
        }
      else{
          staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(125,thirdLabel.frame.origin.y+thirdLabel.frame.size.height+5,130, 14)];
      }
    
    
    
        staticStarRatingView.canEdit = NO;
    staticStarRatingView.maxRating = 5;
    id value=self.rating_hotel;
    int rate=[value intValue];

    staticStarRatingView.rating=rate;
    [subView addSubview:staticStarRatingView];
  
    //images of three things
    int xPos=2;
     int xaxes=20;
    for (int k=0; k<2; k++)
    {
 
        UIImageView* imageView=[[UIImageView alloc]initWithFrame:CGRectMake(xPos,139, 10, 10)];
        imageView.layer.masksToBounds=YES;
        
        UILabel *bookingValue = [[UILabel alloc] initWithFrame:CGRectMake(xaxes,137,150,15)];
        //bookingValue.textAlignment=NSTextAlignmentLeft;
        bookingValue.backgroundColor=[UIColor clearColor];
        bookingValue.adjustsFontSizeToFitWidth=YES;
        bookingValue.tag=k;
        bookingValue.userInteractionEnabled=YES;
        bookingValue.font = [UIFont boldSystemFontOfSize:12];
        bookingValue.textColor = [UIColor whiteColor];
       
        
        UITapGestureRecognizer* tapForLabel=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapLabel:)];
        tapForLabel.numberOfTapsRequired=1;
        tapForLabel.numberOfTouchesRequired=1;
        [bookingValue addGestureRecognizer:tapForLabel];

        //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
      
        switch (k)
        {
            case 0:
                
            {
                imageView.image=[UIImage imageNamed:@"phone.png"];
                 bookingValue.text=booking_NUM;
            }
                break;
            
            case 1:
                
            {
                imageView.image=[UIImage imageNamed:@"web.png"];
                if ([websiteName isEqual:[NSNull null]])
                {
                     bookingValue.text=@"N.A";
                }
                else
                {
                    bookingValue.text=websiteName;

                }
            
            }
                break;
                
                
            default:
                break;
        }
        xPos+=108;
        xaxes+=105;
        [subView addSubview:imageView];
        [subView addSubview:bookingValue];
        
    }
    
   
   

    //UISCROLL VIEW
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,148,320 ,2000)];
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;
    scrollView.userInteractionEnabled = NO;
    scrollView.backgroundColor=[UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    [detail_View addSubview:scrollView];

    int y=4;
    UITextView * textView;
    NSLog(@"%@",arr);
    NSLog(@"%lu",(unsigned long)arr.count);
   
    for (int i=0; i<arr.count; i++)
    {
        UIButton *button=[UIButton buttonWithType: UIButtonTypeCustom];
        button.backgroundColor= [UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
        button.frame=CGRectMake(0, y,320,28);
        button.tag=i;
        [button setTitle:[arr objectAtIndex:i] forState:UIControlStateNormal];
        button.titleLabel.font=[UIFont boldSystemFontOfSize:14];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets=UIEdgeInsetsMake(2, 5, 0, 3);
        //[button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
       
        
        NSArray *arr1=[cu objectAtIndex:i];
               y=y+button.frame.size.height+1;
        NSLog(@"%lu",(unsigned long)arr1.count);
        for (int j=0; j<arr1.count; j++)
        {
            if([arr[i] isEqualToString:@"RESTAURANT INFORMATION"])
            {
               
                
                
                NSString *trimmed = [desc_Str stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                
                
                hobbiesTextViewSize = [self calculateHeightForString:trimmed];
                NSLog(@"%@",NSStringFromCGSize(hobbiesTextViewSize));
                //now set the hobbiesTextView frame and also the text
                UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, y, self.view.frame.size.width, hobbiesTextViewSize.height)];
                 textView.textColor=[UIColor blackColor];
                textView.backgroundColor=[UIColor whiteColor];
                textView.text = trimmed;
                 textView.font=[UIFont systemFontOfSize:12];
                textView.editable=NO;
                y=y+textView.frame.size.height+1;
                NSLog(@"%d",y);
                  [scrollView addSubview:textView];
            }
            else{
                NSLog(@"%d",y);
                UILabel* value=[[UILabel alloc]initWithFrame:CGRectMake(0, y, 320, 18)];
                value.text=[NSString stringWithFormat:@" %@",arr1[j]];
                value.textAlignment=NSTextAlignmentLeft;
                value.textColor=[UIColor blackColor];
                value.backgroundColor=[UIColor whiteColor];
                value.font=[UIFont systemFontOfSize:13];
                y=y+value.frame.size.height+1;
                [scrollView addSubview:value];
            }
            
        }
        
          [scrollView addSubview:button];
        
        
        
    
    }
    if (!IS_IPHONE_5)
    {
        y=y+textView.frame.size.height-40;

    }
    else
    {
         y=y+textView.frame.size.height-130;
        NSLog(@"%d",y);
    }
   scrollView.contentSize=CGSizeMake(self.view.frame.size.width, y);
     NSLog(@"%d",y);
    [gallery_View removeFromSuperview];
    [reviews_View removeFromSuperview];
    mainScrollView.contentSize =CGSizeMake(self.view.frame.size.width, y+50+self.view.frame.size.height);
 }

//our helper method
- (CGSize)calculateHeightForString:(NSString *)str
{
    CGSize size = CGSizeZero;
    
    UIFont *labelFont = [UIFont systemFontOfSize:13.0f];
    NSDictionary *systemFontAttrDict = [NSDictionary dictionaryWithObject:labelFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:str attributes:systemFontAttrDict];
    CGRect rect = [message boundingRectWithSize:(CGSize){self.view.frame.size.width, MAXFLOAT}
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                        context:nil];//you need to specify the some width, height will be calculated
    size = CGSizeMake(rect.size.width, rect.size.height + 5); //padding
    
    return size;
    
    
}

- (CGRect)contentSizeRectForTextView:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect textBounds = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    CGFloat width =  (CGFloat)ceil(textBounds.size.width + textView.textContainerInset.left + textView.textContainerInset.right);
    CGFloat height = (CGFloat)ceil(textBounds.size.height + textView.textContainerInset.top + textView.textContainerInset.bottom);
    return CGRectMake(0, 0, width, height);
}

-(void)galleryView
{
    [detail_View removeFromSuperview];
    [reviews_View removeFromSuperview];
    
    gallery_View=[[UIView alloc]initWithFrame:CGRectMake(0, 132, 320, 325)];
    //gallery_View = [[UIScrollView alloc]initWithFrame:CGRectMake(0,110,320 ,215)];
    gallery_View.backgroundColor=[UIColor clearColor];
    [mainScrollView  addSubview:gallery_View];
    //[self.view addSubview:gallery_View];
    
    
    if (imagesArray == nil ||imagesArray == (id)[NSNull null] )
    {
        
        
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"No images for gallery" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        //UISCROLL VIEW
        scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,320 ,325)];
        scrollView.delegate = self;
        scrollView.backgroundColor=[UIColor clearColor];
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.pagingEnabled=YES;
        [gallery_View addSubview:scrollView];
        
        
//        int y=20;
//        
//        NSInteger row=(imagesArray.count/2+imagesArray.count%2);
//        
        
        NSLog(@"%lu",(unsigned long)imagesArray.count);
        //NSLog(@"%d",row);
        
        // int row=(imagesArray.count/2+imagesArray.count%2);
        int x=0;
        int k=0;
        for (int i=0; i<imagesArray.count; i++)
            
        {
            UIImageView* imageView;
            
            if (!IS_IPHONE_5)
            {
                 imageView=[[UIImageView alloc]initWithFrame:CGRectMake(x,1, 320, 230)];
            }
            else
            {
               imageView=[[UIImageView alloc]initWithFrame:CGRectMake(x,5, 320, 310)];

                
            }
                               imageView.backgroundColor=[UIColor lightGrayColor];
                imageView.layer.borderWidth=1.0;
                imageView.tag=k;
                imageView.layer.borderColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0].CGColor;
                imageView.layer.cornerRadius=2;
            
                imageView.userInteractionEnabled=YES;
                imageView.layer.masksToBounds=YES;
                
                //UIImage * hotelImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArray objectAtIndex:k]]]];
                //imageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imagesArray objectAtIndex:k]]]];
            [imageView sd_setImageWithURL:[imagesArray objectAtIndex:i] placeholderImage:[UIImage imageNamed:@"ic-launcher.png"] options:SDWebImageRefreshCached];
               // [self imageDownloader:[imagesArray objectAtIndex:i] imageView:imageView];
                
             x+=320;
            
                [scrollView addSubview:imageView];
                scrollView.contentSize=CGSizeMake(320*(i+1), 320);
        }
        
    }
    [mainScrollView setScrollEnabled:NO];
    

  }
-(void)reviewsView
{
    [detail_View removeFromSuperview];
    [gallery_View removeFromSuperview];
    
    reviews_View=[[UIView alloc]initWithFrame:CGRectMake(0, 132, 320, 325)];
    //gallery_View = [[UIScrollView alloc]initWithFrame:CGRectMake(0,110,320 ,215)];
    reviews_View.backgroundColor=[UIColor clearColor];
    [mainScrollView addSubview:reviews_View];
    //[self.view addSubview:reviews_View];
    
      NSLog(@"allRatingdict array is = %@",allRatingdict);
    
      if (allRatingdict == nil || allRatingdict == (id)[NSNull null] ||  allRatingdict ==0)
      {
          
          
          UILabel* textViewLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,70,self.view.frame.size.width-20, 40)];
          textViewLabel.text=@"Be The First One To Review";
          textViewLabel.textColor=[UIColor whiteColor];
          textViewLabel.adjustsFontSizeToFitWidth=YES;
           textViewLabel.backgroundColor=[UIColor clearColor];
            textViewLabel.textAlignment=NSTextAlignmentCenter;
          textViewLabel.font=[UIFont systemFontOfSize:18];
          
          
          [reviews_View addSubview:textViewLabel];

          
      }
    else
    {
    
    UIScrollView* scroll=[[UIScrollView alloc]initWithFrame:CGRectMake(0,45, 320,150)];
    
    int xpos=0;
    
    for(int i=0;i<allRatingdict.count;i++)
    {
        
        NSMutableArray* ratingArr=[[NSMutableArray alloc]init];
        
        ratingArr=[allRatingdict objectAtIndex:i];
        
                
        UIView* view=[[UIView alloc]initWithFrame:CGRectMake(xpos,0, 320, 150)];
        view.backgroundColor=[UIColor clearColor];
        [scroll addSubview:view];
        
        UILabel *bookingValue = [[UILabel alloc] initWithFrame:CGRectMake(50,0,200,17)];
        bookingValue.textAlignment=NSTextAlignmentCenter;
        bookingValue.backgroundColor=[UIColor clearColor];
        bookingValue.font = [UIFont boldSystemFontOfSize:16];
        bookingValue.textColor = [UIColor whiteColor];
        if ([[ratingArr valueForKey:@"name"] isEqual:[NSNull null]])
        {
               bookingValue.text=@" ";
        }
        else
        {
               bookingValue.text=[ratingArr valueForKey:@"name"];
        }
     
        //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [view addSubview:bookingValue];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,20,100,14)];
        timeLabel.textAlignment=NSTextAlignmentCenter;
        timeLabel.font = [UIFont boldSystemFontOfSize:12];
        timeLabel.textColor = [UIColor whiteColor];
        
        if ([[ratingArr valueForKey:@"date"] isEqual:[NSNull null]])
        {
            timeLabel.text=@" ";

        }else
        {
            NSString *date=[ratingArr valueForKey:@"date"];
            NSArray* name=[date  componentsSeparatedByString:@" "];
            NSLog(@"cuisne =\n %@",name);
            timeLabel.text=[name objectAtIndex:0];

            
        }

               //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [view addSubview:timeLabel];
        
        //NSString * str=[NSString stringWithFormat:@"%@",[ratingArr valueForKey:@"rating"]];
        
        NSLog(@"%@",[ratingArr valueForKey:@"rating"]);
          if ([ratingArr valueForKey:@"rating"] == nil || [ratingArr valueForKey:@"rating"] == (id)[NSNull null])
          {
 
 
          }
        else
        {
            int rate=[[ratingArr valueForKey:@"rating"] intValue];
            NSLog(@"%d",rate);
             staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(100,40,110, 14)];
            staticStarRatingView.canEdit = NO;
            staticStarRatingView.rating=rate;
            staticStarRatingView.maxRating = 5;
            [view addSubview:staticStarRatingView];
         }
        
        UITextView* textViewLabel=[[UITextView alloc]initWithFrame:CGRectMake(20,70, 280, 70)];
        textViewLabel.textColor=[UIColor whiteColor];
        
        if ([[ratingArr valueForKey:@"review_detail"] isEqual:[NSNull null]])
        {
             textViewLabel.text=@"No Review Found";
        }else
        {
             textViewLabel.text=[ratingArr valueForKey:@"review_detail"];
        }
       
        textViewLabel.backgroundColor=[UIColor clearColor];
        textViewLabel.textAlignment=NSTextAlignmentCenter;
        textViewLabel.editable=NO;
        textViewLabel.font=[UIFont systemFontOfSize:13];
        scroll.pagingEnabled=YES;
        scroll.scrollEnabled=YES;
        
        [view addSubview:textViewLabel];
        
        xpos+=320;
        
        scroll.contentSize=CGSizeMake(320*(i+1),70);
        
        scroll.backgroundColor=[UIColor clearColor];
        
        
    }
    
    [reviews_View addSubview:scroll];
    }

    UIButton *button=[UIButton buttonWithType: UIButtonTypeCustom];
    button.backgroundColor= [UIColor colorWithRed:0.2431 green:0.6902 blue:0.6706 alpha:1.0];
    if (!IS_IPHONE_5)
    {
        button.frame=CGRectMake(145,194,30,30);
    }
    else
    {
         button.frame=CGRectMake(145,210,30,30);
    }

   
    button.layer.cornerRadius=15;
    //[button setTitle:@"Edit" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"rev.png"] forState:UIControlStateNormal];
   // button.titleLabel.font=[UIFont boldSystemFontOfSize:11];
    button.layer.masksToBounds=YES;
    //button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //button.contentEdgeInsets=UIEdgeInsetsMake(2, 5, 0, 3);
    [button addTarget:self action:@selector(editReviewButton) forControlEvents:UIControlEventTouchUpInside];
 
    [reviews_View addSubview:button];
    

    
    [mainScrollView setScrollEnabled:NO];
    
}

#pragma mark - Button-Pressed Methods
//-(void)buttonPressed:(UIButton *)sender
//{
//    for (int i=0; i<[scrollView subviews].count; i++)
//    {
//        UIButton* btn=(UIButton *)[[scrollView subviews]objectAtIndex:i];
//    
//    }
//    if (sender.tag==0)
//    {
//        [self customViews];
//    }
//    
//}
-(void)editReviewButton
{
    
    editView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 385)];
    editView.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];
    editView.userInteractionEnabled=YES;
    [reviews_View addSubview:editView];
    
    UITapGestureRecognizer* tapResign=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapForResignView)];
    tapResign.numberOfTapsRequired=1;
    tapResign.numberOfTouchesRequired=1;
    [editView addGestureRecognizer:tapResign];
//

    
    //UISCROLL VIEW
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,320 ,325)];
    scrollView.delegate = self;
    scrollView.backgroundColor=[UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [editView addSubview:scrollView];

    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,7,100,14)];
    timeLabel.textAlignment=NSTextAlignmentCenter;
    timeLabel.font = [UIFont boldSystemFontOfSize:14];
    timeLabel.textColor = [UIColor blackColor];
    timeLabel.text=@"Add a Review";
    //firstLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [scrollView addSubview:timeLabel];

    int h=40;
    int ypos;
    int x=100;
    int width=210;
    
    for (int i=0;i<reviewNameArray.count; i++)
    {
        ypos=h;
        UILabel* label;
        if (i==2)
        {
            label=[[UILabel alloc]initWithFrame:CGRectMake(10,h+40,80, 15)];

        }
        else
        {
            label=[[UILabel alloc]initWithFrame:CGRectMake(10,h,80, 15)];

        }
        label.textAlignment=NSTextAlignmentLeft;
        label.font=[UIFont systemFontOfSize:12];
        label.backgroundColor=[UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.text=[reviewNameArray objectAtIndex:i];
       
        [scrollView addSubview:label];
        
        
        
        switch (i)
        {
            case 0:
            {
                
                name_Textfield=[[UITextField alloc]initWithFrame:CGRectMake(x,ypos-4,width,24)];
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 28)];
                name_Textfield.leftView = paddingView;
                name_Textfield.leftViewMode = UITextFieldViewModeAlways;
                name_Textfield.delegate=self;
                name_Textfield.layer.borderWidth=1.0;
                name_Textfield.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                name_Textfield.layer.cornerRadius=4;
                name_Textfield.backgroundColor=[UIColor whiteColor];
                name_Textfield.textAlignment=NSTextAlignmentNatural;
                name_Textfield.returnKeyType=UIReturnKeyDefault;
                name_Textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
                
                [scrollView addSubview:name_Textfield];
                
            }
                break;
           
             case 1:
            {
                review_Textview=[[UITextView alloc]initWithFrame:CGRectMake(x,ypos-4,width,70)];
                review_Textview.delegate=self;
                review_Textview.backgroundColor=[UIColor whiteColor];
                review_Textview.textAlignment=NSTextAlignmentNatural;
                review_Textview.returnKeyType=UIReturnKeyDefault;
                review_Textview.layer.borderWidth=1.0;
                review_Textview.layer.cornerRadius=4;
                review_Textview.layer.borderColor=[UIColor colorWithRed:0.7608 green:0.7608 blue:0.7647 alpha:1.0].CGColor;
                review_Textview.autocapitalizationType = UITextAutocapitalizationTypeNone;
                
                [scrollView addSubview:review_Textview];
            }
                break;
            case 2:
            {
                
                thirdView=[[UIView alloc]initWithFrame:CGRectMake(0, ypos+43, 320, 163)];
                thirdView.backgroundColor=[UIColor clearColor];
                thirdView.tag=1;
                [scrollView addSubview:thirdView];
                
                int v=0;
                for (int j=0;j<5; j++)
             {
                    UILabel *ratings_label;
                    if (j==3)
                    {
                        ratings_label=[[UILabel alloc]initWithFrame:CGRectMake(x-18, v,110,30)];
                        
                         ratings_label.numberOfLines=3;

                    }
                    else
                    {
                        ratings_label=[[UILabel alloc]initWithFrame:CGRectMake(x-18, v,95, 14)];

                    }
                ratings_label.textAlignment=NSTextAlignmentLeft;
                ratings_label.font=[UIFont systemFontOfSize:11];
                ratings_label.backgroundColor=[UIColor clearColor];
                ratings_label.textColor=[UIColor blackColor];
               
                [thirdView addSubview:ratings_label];
                    
                staticStarRatingView=[[ASStarRatingView alloc]initWithFrame:CGRectMake(110+85,v-15,125,45)];
                staticStarRatingView.tag=j;
                staticStarRatingView.canEdit =YES;
                staticStarRatingView.maxRating = 5;
                
                [thirdView addSubview:staticStarRatingView];
                    
                    switch (j)
                    {
                        case 0:
                        {
                             ratings_label.text=@"Overall";
                            staticStarRatingView.rating=0;
                        }
                        break;
                        case 1:
                        {
                            ratings_label.text=@"Food quality";
                            staticStarRatingView.rating=0;
                        }
                            break;
                        case 2:
                        {
                            ratings_label.text=@"Customer service";
                            staticStarRatingView.rating=0;
                        }
                            break;
                        case 3:
                        {
                            ratings_label.text=@"Ease of Bite BC use";
                           
                            staticStarRatingView.rating=0;
                        }
                            break;
                        case 4:
                        {
                            ratings_label.text=@"Atmosphere";
                            staticStarRatingView.rating=0;
                        }
                            break;
                            
                         
                    }
                

                     v+=34;
                    
                    
            }
            
        }
                
           }
        
        
      h+=40;
        
        
        if (!IS_IPHONE_5)
        {
            scrollView.contentSize=CGSizeMake(320, 460);
            
        }
        else
        {
            scrollView.contentSize=CGSizeMake(320, 420);

        }
        
    }
   UIButton* submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
    [submit_Button setFrame:CGRectMake(x-15,340,225,28)];
    submit_Button.layer.borderWidth=1.0;
    submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
    [submit_Button addTarget:self action:@selector(submitt_review) forControlEvents:UIControlEventTouchUpInside];
    [submit_Button setTitle:@"SUBMIT REVIEW" forState:UIControlStateNormal];
    submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:14];
    [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [scrollView addSubview:submit_Button];
    

    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [editView removeFromSuperview];
}
-(void)postRating:(NSString *)star1 star2:(NSString *)star2 star3:(NSString *)star3 star4:(NSString *)star4 star5:(NSString *)star5
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Thank you for submitting your review !" message:@"It is with the admin for approval" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    int first=[star1 intValue];
    int second=[star2 intValue];
    int third=[star3 intValue];
    int fourth=[star4 intValue];
    int fivth=[star5 intValue];
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"productid=%@&ratings[overall]=%d&ratings[atmosphere]=%d&ratings[carduse]=%d&ratings[customer]=%d&ratings[food]=%d&customer_id=%@&detail=%@&nickname=%@",self.product_id,first,second,third,fourth,fivth,[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],review_Textview.text,name_Textfield.text];
    
     NSLog(@"request which send rating =%@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@review.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencode" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    [self hideLoadingView];
    //NSOperationQueue* qu=[[NSOperationQueue alloc]init];
    
   connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    

    
}
-(void)submitt_review
{
    NSString *overall,*food,*cust,*cardUse,*atompos;
    NSLog(@"%@",name_Textfield.text);
    NSLog(@"%@",review_Textview.text);

    if ([name_Textfield.text isEqualToString:@""] )
    {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Message" message:@"Please fill your name !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSLog(@"preesed");
        for(int i=1;i<[[thirdView subviews] count];i=i+2)
        {
            staticStarRatingView=[[thirdView subviews]objectAtIndex:i];
            
            if (i==1)
            {
                overall=[NSString stringWithFormat:@"%f",staticStarRatingView.rating];
                
                
                NSLog(@"overall.count=%@",overall);
                
            }
            if (i==3)
            {
                food=[NSString stringWithFormat:@"%f",staticStarRatingView.rating];
                NSLog(@"food.count=%@",food);
                
            }
            if (i==5)
            {
                cust=[NSString stringWithFormat:@"%f",staticStarRatingView.rating];
                NSLog(@"cust.count=%@",cust);
                
            }
            if (i==7)
            {
                cardUse=[NSString stringWithFormat:@"%f",staticStarRatingView.rating];
                NSLog(@"cardUse.count=%@",cardUse);
                
            }
            if (i==9)
            {
                atompos=[NSString stringWithFormat:@"%f",staticStarRatingView.rating];
                NSLog(@"atompos.count=%@",atompos);
                
            }
        }
        
        isSubmit=1;
        [self postRating:overall star2:atompos star3:cardUse star4:cust star5:food];

    }
    
}
#pragma mark - PostCustmerid


-(void)postCustmerid
{
    [self showLoadingView];
    postCustmer=1;
    isSubmit=1;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    

}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    // handle
}
#pragma mark - FB-Share-ButtonPressed
-(void)fbButtonPressed
{
//     // Check if the Facebook app is installed and we can present the share dialog
//    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
//
//    
//   // params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
//    params.name=self.nameOfpRestaureant_String;
//   params.caption=@"Shared via biteBC User.";
//    params.link = [NSURL URLWithString:websiteName];
//    params.picture = [NSURL URLWithString:imageOfHotel_string];
//    
//    // If the Facebook app is installed and we can present the share dialog
//    if ([FBDialogs canPresentShareDialogWithParams:params])
//    {
//        
//        // Present share dialog
//        [FBDialogs presentShareDialogWithLink:params.picture
//                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//                                          if(error) {
//                                              
//                                              NSLog(@"Error publishing story: %@", error.description);
//                                          } else {
//                                              // Success
//                                              NSLog(@"result is %@", results);
//                                          }
//                                      }];
//        
//        // If the Facebook app is NOT installed and we can't present the share dialog
//    } else {
    
    
    NSLog(@"%@",desc_Str);
    NSString * nameRes;
    NSString *per  =@"%";
    if ([offerAvl isEqualToString:@"31"])
    {
       
        nameRes=[NSString stringWithFormat:@"Enjoying 2 for 1 meals at %@ thanks to Bite BC.",self.nameOfpRestaureant_String];
        
    }
    else if ([offerAvl isEqualToString:@"100"]){
       
        nameRes=[NSString stringWithFormat:@"Enjoying 2 for 1 at %@ thanks to Bite BC.",self.nameOfpRestaureant_String];
  
        
    }
    else if ([offerAvl isEqualToString:@"101"]){
        
        nameRes=[NSString stringWithFormat:@"Enjoying 50%@ off at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];

        
        
    }
    else if ([offerAvl isEqualToString:@"102"]){
          nameRes=[NSString stringWithFormat:@"Enjoying 40%@ off at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];
        
    }
    else if ([offerAvl isEqualToString:@"103"]){
        
        nameRes=[NSString stringWithFormat:@"Enjoying 30%@ off at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];

    }
    
    else if ([offerAvl isEqualToString:@"104"]){
        nameRes=[NSString stringWithFormat:@"Enjoying 40%@ off food bill at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];
        
    }
    else if ([offerAvl isEqualToString:@"105"]){
        nameRes=[NSString stringWithFormat:@"Enjoying 30%@ off food bill at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];
        
    }
    
   

    
    else
    {
        nameRes=[NSString stringWithFormat:@"Enjoying 50%@ off at %@ thanks to Bite BC.",per,self.nameOfpRestaureant_String];
       
        
    }
    NSString* myText=[NSString stringWithFormat:@"%@",@"Bite BC is a large scale diners club in the form of a mobile app providing members with discounts on eating out.\n\n\n With our intuitive app you will be able to locate all of our participating restaurants at a touch of a button and will save hundreds of dollars over the course of the year."];

    
    
      //  NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    //   nameRes, @"name",
                                    //   @"Powered by BiteBC", @"caption",
                                     //  myText, @"description",
                                     //  @"www.bitebc.ca", @"link",
                                      // imageOfHotel_string, @"picture",
                                      // nil];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.imageURL = [NSURL URLWithString:imageOfHotel_string];
   content.contentTitle = nameRes;
   content.contentDescription = myText;
   content.contentURL =[NSURL URLWithString:@"http://www.bitebc.ca"];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
        // fallback presentation when there is no FB app
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
    
    //[FBSDKShareDialog showFromViewController:self
                               //  withContent:content
                                 //   delegate:self];

//    

}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    
}
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs)
    {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

-(void)checkinButtonPressed
{
    
    [self postCustmerid];
    
    
}
-(void)detalButtonPressed
{
    
    detailButton.enabled=NO;
    galleryButton.enabled=YES;
    reviewsButton.enabled=YES;
    
    detailButton.backgroundColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
     galleryButton.backgroundColor= [UIColor clearColor];
     reviewsButton.backgroundColor=[UIColor clearColor];
    
    [self detailView];
}
-(void)galleryButtonPressed
{
    detailButton.enabled=YES;
    galleryButton.enabled=NO;
    reviewsButton.enabled=YES;

    galleryButton.backgroundColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    detailButton.backgroundColor=[UIColor clearColor];
    reviewsButton.backgroundColor=[UIColor clearColor];
    
    [self galleryView];
}
-(void)reviewsButtonPressed
{
    
    
   
    detailButton.enabled=YES;
    galleryButton.enabled=YES;
    reviewsButton.enabled=NO;
 
    reviewsButton.backgroundColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    detailButton.backgroundColor=[UIColor clearColor];
    galleryButton.backgroundColor=[UIColor clearColor];
    
    [self reviewsView];
}
#pragma mark -  UITapgesture Delegate Methods
-(void)tapOnImage
{
    
    
    
}


#pragma mark - UITextField Delegate Methods

-(void)tapForResignView
{
    [review_Textview resignFirstResponder];
    [name_Textfield resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)mail
{
    if ([MFMailComposeViewController canSendMail])
        
    {
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"biteBC"];
        
      //  NSArray* rec=[[NSArray alloc]initWithObjects:self.client_mail, nil];
        
               NSArray* rec1=[[NSArray alloc]initWithObjects:@"biteBC@gmail.com", nil];
        //
        //
        //        NSArray* rec2=[[NSArray alloc]initWithObjects:@"fourth@example.com", nil];
        
//        NSString* body=[NSString stringWithFormat:@"Hello %@,\n\nThis is a notification to inform you we will be with you in approximately one hour to carry out the %@.\n If this is not suitable, please call 0845 180 5001.\n\n Regards\n %@",self.siteName,self.tasktype,[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"]];
        
     //   [mailComposer setMessageBody:body isHTML:NO];
        
        
        [mailComposer setToRecipients:rec1];
        
        //        [mailComposer  setCcRecipients:rec1];
        //        [mailComposer setBccRecipients:rec2];
        
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }

}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    UILabel*label;
    
    switch (result) {
            
            
        case 0:
            label.text=@"result canceeled";
            
            break;
        case 1:
            label.text=@"result saved";
            break;
        case 2:
            
            label.text=@"result sent";
            
            break;
            
        case 3:
            label.text=@"result failed";
            break;
            
            
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)bookingTap
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phone_Num];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

}
-(void)tapLabel:(UITapGestureRecognizer *)label
{
    if (label.view.tag==0)
    {
        NSLog(@"0000000");
        NSLog(@"call open %@",booking_NUM);
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:booking_NUM];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

    }
    else if (label.view.tag==1)
    {
        NSLog(@"22222");
       // [self mail];
        NSLog(@"%@",websiteName);
        
        NSString* url=[NSString stringWithFormat:@"http://%@",websiteName];
        
               
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
    }
}
//-(void)tapForResignView
//{
//    [name_Textfield resignFirstResponder];
//    [review_Textview resignFirstResponder];
//    
//}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [name_Textfield resignFirstResponder];
    [review_Textview resignFirstResponder];
   


    
}
#pragma mark - ImageDownloader Method
-(void)imageDownloader:(NSString *)urlStr imageView:(UIImageView *)imageView
{
    dispatch_queue_t imageQueue=dispatch_queue_create("imageDownloader", nil);
    dispatch_async(imageQueue, ^{
        NSURL *imageurl=[NSURL URLWithString:urlStr];
        NSData *imageData=[NSData dataWithContentsOfURL:imageurl];
        UIImage *image=[UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            imageView.image=image;
        });
        
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
