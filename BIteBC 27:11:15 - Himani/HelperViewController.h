//
//  HelperViewController.h
//  BiteBC
//
//  Created by brst on 9/13/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantTableViewController.h"
#import "DetailPageTableViewController.h"
#import "SettingTableViewController.h"
#import "NearMeViewController.h"

@interface HelperViewController : UIViewController<UIScrollViewDelegate,UITabBarControllerDelegate,UIAlertViewDelegate>
{
    
    UIImageView * imageView;
    
    
    UIButton * skipButton;
    UIBarButtonItem * barButton;
    
    
    NSMutableArray * imagesArry;
}
@property(nonatomic,strong) UITabBarController* tabBarController;
@property(nonatomic,strong) NSString* planTime;
@end
