//
//  DetailOfRestaurantViewController.h
//  BiteBC
//
//  Created by brst on 7/8/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "ASStarRatingView.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SelectPlanViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface DetailOfRestaurantViewController : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate,FBSDKSharingDelegate,FBSDKSharingDialog>

{
    UIScrollView *mainScrollView;
    UIImageView* hotelImageView;
    
    
    
    UILabel *adressLabel,*cuisineLabel;
    
    UIButton* checkinButton,*mapButton,*fbButton;
    
    UIButton * detailButton,* galleryButton,* reviewsButton;
    
    UIView *detail_View,*gallery_View,*reviews_View;
    
    UILabel *firstLabel,*secondLabel,*thirdLabel,*fourthLabel;
    
    ASStarRatingView *staticStarRatingView;
    
    UIScrollView* scrollView;
    
    NSMutableArray *reviewNameArray,*arrayOfName,*imagesArray,*allRatingdict;
    
    MFMailComposeViewController *mailComposer;


    NSURLConnection *connection;
    
    int avlY;
    int avlX;
     NSMutableDictionary* detailDict;
    // NSMutableDictionary* allRatingdict;
    
    NSMutableDictionary* dict;
    NSMutableData* data;
    
    UIImage*  imageOf_hotel;
    
    NSString *offerPeriod,*offerAvl,*noOfPeople,*websiteName,*imageStr,*numPeople,*desc_Str,*booking_NUM,*phone_Num,*zipcode_string,*calling_str;
    CGSize hobbiesTextViewSize;
    
    NSMutableArray * weeklyArray;
    NSMutableArray *avialableArray;
    UIImageView * cellImage;
    

}

@property (nonatomic, retain) NSMutableDictionary *sourceDictionary;

@property(strong,nonatomic)NSString* nameOfpRestaureant_String;
@property(strong,nonatomic)NSString* adressOfRestaurant_String;
@property(strong,nonatomic)NSString* cuisine_String;
@property(strong,nonatomic)NSString* rating_hotel;
@property(strong,nonatomic)NSString* subTotalOfPlan_String;
@property(strong,nonatomic)NSString* imageOfHotel_string;

@property(strong,nonatomic)NSString* weekly_str;
@property(strong,nonatomic)NSString* monthly_str;

@property(strong,nonatomic)UIImage*  imageOf_hotel;

 



@property(strong,nonatomic)NSString* product_id;
@end
