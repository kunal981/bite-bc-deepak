//
//  FilterViewController.m
//  BiteBC
//
//  Created by brst on 7/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "FilterViewController.h"
#import "AppDelegate.h"
#import "AppDelegate.h"

@interface FilterViewController ()
{
    
    UIScrollView* offerBackView;
    UIView* numberView;
    UIView* offerView;
    UIScrollView* availabilityView;
}

@end

@implementation FilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
   
    
}
-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}
- (void)viewDidLoad
{
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"cuisine"]);
      NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype"]);
      NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"noofpeople"]);
    
     self.navigationItem.title=@"Filter";
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    nameOfList=[[NSMutableArray alloc]initWithObjects:@"NO. OF PEOPLE",@"CUISINE",@"OFFER TYPE",@"AVAILABILITY",nil];
       
    foodList=[[NSMutableArray alloc]initWithObjects:@"American",@"Brazilian",@"British",@"Caribbean",@"Chinese",@"Eastern European",@"Ethiopian",@"French",@"Fusion",@"German",@"Greek",@"Indian",@"Italian",@"Japanese",@"Mexican",@"Middle Eastern",@"Modern European",@"Persian",@"Seafood",@"Spanish",@"Sushi",@"Tapas",nil];
  
    daysList=[[NSMutableArray alloc]initWithObjects:@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday",nil];
    
    offerArr = [[NSMutableArray alloc]initWithObjects:@"2 for 1 meals",@"50% off food bill",@"2 for 1",@"50% off",@"30% off",@"40% off", @"30% off food bill",@"40% off food bill", nil];
    
    [self linesView];
    
    //[self postData_Filter];
    
    
  UIButton *submit_Button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    if (!IS_IPHONE_5)
    {
            [submit_Button setFrame:CGRectMake(2,333,316,30)];
    }else
    {
            [submit_Button setFrame:CGRectMake(2,350,316,30)];
    }

    submit_Button.layer.borderWidth=1.0;
    submit_Button.layer.borderColor=[UIColor whiteColor].CGColor;
    [submit_Button addTarget:self action:@selector(submit_ButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [submit_Button setTitle:@"SUBMIT" forState:UIControlStateNormal];
    submit_Button.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [submit_Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submit_Button.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [self.view addSubview:submit_Button];

    
}
-(void)linesView
{
    
    int yPos=19;
        int h=40;
    for (int i=0; i<nameOfList.count; i++)
    {
        
        UIView* lineView=[[UIView alloc]initWithFrame:CGRectMake(0,h, 320, 1)];
        lineView.backgroundColor=[UIColor lightGrayColor];
        h+=80;
        [self.view addSubview:lineView];
        
        UILabel* hotelName_Label = [[UILabel alloc] initWithFrame:CGRectMake(100,yPos,120,18)];
        hotelName_Label.font = [UIFont boldSystemFontOfSize:14];
        hotelName_Label.textAlignment=NSTextAlignmentCenter;
        hotelName_Label.backgroundColor=[UIColor clearColor];
        hotelName_Label.text=[nameOfList objectAtIndex:i];
        hotelName_Label.textColor = [UIColor whiteColor];
        // hotelName_Label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        yPos+=80;
        [self.view addSubview:hotelName_Label];
 
    }
    [self noOfPeopleView];
    [self cuisineView];
    [self offerTypeView];
    [self availabilityView];
   
    
}
#pragma mark - All views Method
 -(void)noOfPeopleView
{
    
    backView=[[UIScrollView alloc]initWithFrame:CGRectMake(20,56,280, 30)];
    backView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:backView];
    
    int xPos=2;
    
    for (int num=1; num<=20; num++)
    {
        
          numberView=[[UIView alloc]initWithFrame:CGRectMake(xPos, 1, 22, 22)];
               xPos+=32;
        numberView.tag=num;
        numberView.userInteractionEnabled=YES;
        [backView addSubview:numberView];
        
        
        UILabel* valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(3, 3, 16, 16)];
        valueLabel.text=[NSString stringWithFormat:@"%d",num];
        valueLabel.backgroundColor=[UIColor clearColor];
        valueLabel.textAlignment=NSTextAlignmentCenter;
        valueLabel.layer.cornerRadius=7;
        valueLabel.tag=num;
        valueLabel.textColor=[UIColor whiteColor];
        valueLabel.userInteractionEnabled=YES;
        valueLabel.font=[UIFont boldSystemFontOfSize:14];
        //x+=28;
        [numberView addSubview:valueLabel];
        
        UITapGestureRecognizer* tap3=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnnumber:)];
        tap3.numberOfTapsRequired=1;
        tap3.numberOfTouchesRequired=1;
        [numberView addGestureRecognizer:tap3];
        
       
        backView.contentSize=CGSizeMake(32*num, 25);
        
    }
    for (int j=0; j<2; j++)
    {
        
    }
    
}
-(void)cuisineView
{
    
     cuisineBckView=[[UIScrollView alloc]initWithFrame:CGRectMake(20, 137,280, 30)];
    cuisineBckView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:cuisineBckView];
    
    int xPos=3;
    int x=0;
    for (int num=0; num<foodList.count; num++)
    {
        
        foodNameView=[[UIView alloc]initWithFrame:CGRectMake(xPos, 1, 75, 20)];
        
        xPos+=75;
      
        foodNameView.tag=num;
        foodNameView.userInteractionEnabled=YES;
        [cuisineBckView addSubview:foodNameView];
        
        UILabel* valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(x, 1,72 , 16)];
        valueLabel.text=[foodList objectAtIndex:num];
        valueLabel.backgroundColor=[UIColor clearColor];
        valueLabel.textAlignment=NSTextAlignmentCenter;
        valueLabel.adjustsFontSizeToFitWidth=YES;
        valueLabel.layer.cornerRadius=7;
        valueLabel.textColor=[UIColor whiteColor];
        valueLabel.font=[UIFont boldSystemFontOfSize:14];
       // x+=28;
        [foodNameView addSubview:valueLabel];
        
        UITapGestureRecognizer* tap4=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOncuisine:)];
        tap4.numberOfTapsRequired=1;
        tap4.numberOfTouchesRequired=1;
        [foodNameView addGestureRecognizer:tap4];
        
        
        cuisineBckView.contentSize=CGSizeMake(80*num, 25);
    }


    
}
-(void)offerTypeView
{
    offerBackView=[[UIScrollView alloc]initWithFrame:CGRectMake(20, 217, 280, 34)];
    offerBackView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:offerBackView];

    int xPos=3;
    int x=0;
    
   // int xp=1;
    for (int j =0; j<8; j++)
    {

        
        offerView=[[UIView alloc]initWithFrame:CGRectMake(xPos, 1, 160, 20)];
        offerView.backgroundColor=[UIColor clearColor];
        offerView.tag=j;
        xPos+=160;
        offerView.userInteractionEnabled = YES;
        [offerBackView addSubview:offerView];

        UILabel* oferAmount=[[UILabel alloc]initWithFrame:CGRectMake(x, 1,140 , 16)];
        oferAmount.text=[offerArr objectAtIndex:j];
        oferAmount.backgroundColor=[UIColor clearColor];
        oferAmount.textAlignment=NSTextAlignmentCenter;
        oferAmount.adjustsFontSizeToFitWidth=YES;
        oferAmount.layer.cornerRadius=7;
        oferAmount.textColor=[UIColor whiteColor];
        oferAmount.font=[UIFont boldSystemFontOfSize:14];
       // xp+=60;
        
        [offerView addSubview:oferAmount];
        
        UITapGestureRecognizer* tap4=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnOffer_choice:)];
        tap4.numberOfTapsRequired=1;
        tap4.numberOfTouchesRequired=1;
        [offerView addGestureRecognizer:tap4];
        
        offerBackView.contentSize=CGSizeMake(200*j, 25);


    }
    
//    UILabel* valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(30, 1,80 , 16)];
//    valueLabel.text=@"2 for 1 meals";
//    valueLabel.backgroundColor=[UIColor clearColor];
//    valueLabel.textAlignment=NSTextAlignmentCenter;
//    valueLabel.layer.cornerRadius=7;
//    valueLabel.textColor=[UIColor whiteColor];
//    valueLabel.font=[UIFont boldSystemFontOfSize:13];
//    // x+=28;
//    [offerView addSubview:valueLabel];
//    
//   
//    
//     valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(170, 1,100 , 16)];
//    valueLabel.text=@"50% of food bill";
//    valueLabel.backgroundColor=[UIColor clearColor];
//    valueLabel.textAlignment=NSTextAlignmentCenter;
//    valueLabel.layer.cornerRadius=7;
//    valueLabel.textColor=[UIColor whiteColor];
//    valueLabel.font=[UIFont boldSystemFontOfSize:13];
//    // x+=28;
//    [offerView addSubview:valueLabel];

    
    
}

-(void)availabilityView
{
    
    
    availabilityView=[[UIScrollView alloc]initWithFrame:CGRectMake(20, 303, 280, 23)];
    availabilityView.backgroundColor=[UIColor clearColor];
    
    [self.view addSubview:availabilityView];
    
    int xPos=3;
    int x=0;
    for (int i=0; i<daysList.count; i++)
    {
        
       dayView=[[UIView alloc]initWithFrame:CGRectMake(xPos, 1, 70, 22)];
        
        
        xPos+=74;
        
        dayView.tag=i;
        dayView.userInteractionEnabled=YES;
        [availabilityView addSubview:dayView];
        
        UILabel* valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(x, 2.5,68 , 16)];
        valueLabel.text=[daysList objectAtIndex:i];
        valueLabel.backgroundColor=[UIColor clearColor];
        valueLabel.textAlignment=NSTextAlignmentCenter;
        valueLabel.adjustsFontSizeToFitWidth=YES;
        valueLabel.layer.cornerRadius=7;
        valueLabel.textColor=[UIColor whiteColor];
        valueLabel.font=[UIFont boldSystemFontOfSize:13];
        // x+=28;
        [dayView addSubview:valueLabel];
        
        UITapGestureRecognizer* tap4=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnDaysView:)];
        tap4.numberOfTapsRequired=1;
        tap4.numberOfTouchesRequired=1;
        [dayView addGestureRecognizer:tap4];
        
        
        availabilityView.contentSize=CGSizeMake(88*i, 22);

    }
   
    
    
//    int xp=3;
//    for (int j =1; j<=3; j++)
//    {
//        UIButton *toggleButton=[UIButton buttonWithType: UIButtonTypeCustom];
//        [toggleButton setImage:[UIImage imageNamed:@"btn.png"] forState:UIControlStateNormal];
//
//        toggleButton.tag=j;
//        
//        toggleButton.frame=CGRectMake(xp, 1, 16, 16);
//        [toggleButton addTarget:self action:@selector(toggleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        xp+=90;
//        [availabilityView addSubview:toggleButton];
//        
//    }
//
//    
//    UILabel* valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(27, 1,55 , 16)];
//    valueLabel.text=@"Friday";
//    valueLabel.backgroundColor=[UIColor clearColor];
//    valueLabel.textAlignment=NSTextAlignmentCenter;
//    valueLabel.layer.cornerRadius=7;
//    valueLabel.textColor=[UIColor whiteColor];
//    valueLabel.font=[UIFont boldSystemFontOfSize:13];
//    // x+=28;
//    [availabilityView addSubview:valueLabel];
//    
//    valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(110, 1,65 , 16)];
//    valueLabel.text=@"Saturday";
//    valueLabel.backgroundColor=[UIColor clearColor];
//    valueLabel.textAlignment=NSTextAlignmentCenter;
//    valueLabel.layer.cornerRadius=7;
//    valueLabel.textColor=[UIColor whiteColor];
//    valueLabel.font=[UIFont boldSystemFontOfSize:13];
//    // x+=28;
//    [availabilityView addSubview:valueLabel];
//    
//    valueLabel=[[UILabel alloc]initWithFrame:CGRectMake(210, 1,55 , 16)];
//    valueLabel.text=@"Sunday";
//    valueLabel.backgroundColor=[UIColor clearColor];
//    valueLabel.textAlignment=NSTextAlignmentCenter;
//    valueLabel.layer.cornerRadius=7;
//    valueLabel.textColor=[UIColor whiteColor];
//    valueLabel.font=[UIFont boldSystemFontOfSize:13];
//    // x+=28;
//    [availabilityView addSubview:valueLabel];
// 
}

#pragma mark - UITapGestureRecognizer Method

 -(void)tapOnnumber:(UITapGestureRecognizer*)recognizer
{
    
    
    NSLog(@"no of people value is= %ld",(long)recognizer.view.tag);
    
    NSString *number=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    
    [[NSUserDefaults standardUserDefaults]setObject:number forKey:@"noofpeople"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   for(int i=0;i<[backView subviews].count;i++)
    {
        UIView *v=[[backView subviews]objectAtIndex:i];
        v.backgroundColor= [UIColor clearColor];
         v.layer.borderColor=[UIColor clearColor].CGColor;
    }
    
  
    
    
    recognizer.view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
    recognizer.view.layer.cornerRadius=11;
    recognizer.view.layer.borderWidth=1.5;
    recognizer.view.layer.borderColor=[UIColor whiteColor].CGColor;

    
}
-(void)tapOncuisine:(UITapGestureRecognizer*)cuisineView
{
    
    
    NSLog(@"%ld",(long)cuisineView.view.tag);
    
    NSLog(@"cuisine view name is= %@",[foodList objectAtIndex:cuisineView.view.tag]);
  
    [[NSUserDefaults standardUserDefaults]setObject:[foodList objectAtIndex:cuisineView.view.tag] forKey:@"cuisine"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
    for(int i=0;i<[cuisineBckView subviews].count;i++)
    {
        UIView *v=[[cuisineBckView subviews]objectAtIndex:i];
        v.backgroundColor= [UIColor clearColor];
        v.layer.borderColor=[UIColor clearColor].CGColor;
    }

     cuisineView.view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
     cuisineView.view.layer.cornerRadius=3;
     cuisineView.view.layer.borderWidth=1.5;
     cuisineView.view.layer.borderColor=[UIColor whiteColor].CGColor;
}
-(void)tapOnDaysView:(UITapGestureRecognizer*)selectDays
{
    NSLog(@"%ld",(long)selectDays.view.tag);
    
    NSLog(@"cuisine view name is= %@",[daysList objectAtIndex:selectDays.view.tag]);
    
    [[NSUserDefaults standardUserDefaults]setObject:[daysList objectAtIndex:selectDays.view.tag] forKey:@"availble"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
     NSLog(@"cuisine view name is= %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"availble" ]);
    
    for(int i=0;i<[availabilityView subviews].count;i++)
    {
        UIView *v=[[availabilityView subviews]objectAtIndex:i];
        v.backgroundColor= [UIColor clearColor];
        v.layer.borderColor=[UIColor clearColor].CGColor;
    }
    
    selectDays.view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
    selectDays.view.layer.cornerRadius=3;
    selectDays.view.layer.borderWidth=1.5;
    selectDays.view.layer.borderColor=[UIColor whiteColor].CGColor;

}
-(void)tapOnOffer_choice:(UITapGestureRecognizer*)offer_View
{
    NSLog(@"%ld",(long)offer_View.view.tag);
    
    if (offer_View.view.tag==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"31" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
    }
    else if (offer_View.view.tag==1)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"32" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    else if (offer_View.view.tag==2)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"100" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    
    else if (offer_View.view.tag==3)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"101" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    else if (offer_View.view.tag==4)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"103" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    else if (offer_View.view.tag==5)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"102" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    else if (offer_View.view.tag==6)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"105" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    else if (offer_View.view.tag==7)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"104" forKey:@"offertype"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"offertype" ]);
        
    }
    
    for(int i=0;i<[offerBackView subviews].count;i++)
    {
        UIView *v=[[offerBackView subviews]objectAtIndex:i];
        v.backgroundColor= [UIColor clearColor];
        v.layer.borderColor=[UIColor clearColor].CGColor;
    }

    offer_View.view.backgroundColor=[UIColor colorWithRed:0.0000 green:0.4667 blue:0.4196 alpha:1.0];
    offer_View.view.layer.cornerRadius=3;
    offer_View.view.layer.borderWidth=1.5;
    offer_View.view.layer.borderColor=[UIColor whiteColor].CGColor;

    
}

#pragma mark - Post Method
-(void)submit_ButtonPressed
{
    
        
    [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"filter"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

 - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
