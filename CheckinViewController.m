//
//  CheckinViewController.m
//  BiteBC
//
//  Created by brst on 7/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "CheckinViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"

@interface CheckinViewController ()

@end

@implementation CheckinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:19],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    self.title=@"Identification Card";

    
}

-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor colorWithRed:0.9216 green:0.9255 blue:0.9255 alpha:1.0];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self profileView];
   // [self checkPlan];
}
#pragma mark -  sendidForReastaurant
-(void)sendidForReastaurant
{
    
    NSLog(@"%@",self.restid);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]);
    
    
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"customerid=%@&rest_id=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"],self.restid];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@check_in.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];

    
}

-(void)profileView
{
    UIView *whiteView;
    UILabel *label;
    UILabel *noteLabel;
    if (!IS_IPHONE_5)
    {
        whiteView=[[UIView alloc]initWithFrame:CGRectMake(10, 30, 300, 200)];
        label=[[UILabel alloc]initWithFrame:CGRectMake(5,235,self.view.frame.size.width-10,20)];
           noteLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,270,300,40)];

    }
    else
    {
         whiteView=[[UIView alloc]initWithFrame:CGRectMake(10, 45, 300, 200)];
         label=[[UILabel alloc]initWithFrame:CGRectMake(5,250,self.view.frame.size.width-10,20)];
           noteLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,280,300,40)];
    }
    whiteView.backgroundColor=[UIColor whiteColor];
    whiteView.layer.cornerRadius=3;
    whiteView.layer.borderWidth=1.0;
    whiteView.layer.borderColor=[UIColor blackColor].CGColor;
    [self.view addSubview:whiteView];
    
    UIImageView *imageV=[[UIImageView alloc]initWithFrame:CGRectMake(110,-5,60,60)];
    imageV.backgroundColor=[UIColor clearColor];
    imageV.image=[UIImage imageNamed:@"ic_launcher.png"];
    [whiteView addSubview:imageV];
    
    
    UIView * greenView=[[UIView alloc]initWithFrame:CGRectMake(0, 40 ,300 ,120)];
    greenView.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [whiteView addSubview:greenView];
    
    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(70, 3, 150,20)];
    nameLabel.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"];
    nameLabel.textAlignment=NSTextAlignmentCenter;
    nameLabel.adjustsFontSizeToFitWidth=YES;
    nameLabel.textColor=[UIColor whiteColor];
    nameLabel.font=[UIFont boldSystemFontOfSize:16];
    [greenView addSubview:nameLabel];
    
    
    NSString* urlString=[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [[NSUserDefaults standardUserDefaults]valueForKey:@"password"]];
    NSLog(@"%@",urlString);
   
    UIImageView *profileImage=[[UIImageView alloc]initWithFrame:CGRectMake(100,30,90,80)];
    profileImage.layer.masksToBounds=YES;
    profileImage.backgroundColor=[UIColor clearColor];
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"login"] isEqualToString:@"fbMode"])
    {
        [profileImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];

    }else{
         [profileImage sd_setImageWithURL:[NSURL URLWithString:self.profileImage_String] placeholderImage:[UIImage imageNamed:@"ic_launcher.png"] options:SDWebImageRefreshCached];
        //profileImage.image=[UIImage imageNamed:@"ic_launcher.png"];
    }
    
    
    [greenView addSubview:profileImage];

    UILabel *planName=[[UILabel alloc]initWithFrame:CGRectMake(20,170,250,20)];
    planName.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"pack"]);
    planName.textAlignment=NSTextAlignmentCenter;
    planName.textColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    planName.font=[UIFont boldSystemFontOfSize:18];
    [whiteView addSubview:planName];

   
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor redColor];
    label.textAlignment=NSTextAlignmentCenter;
    label.numberOfLines=2;
    label.adjustsFontSizeToFitWidth=YES;
    label.text= [NSString stringWithFormat:@"Membership expires on - %@ (Days left %@)",self.expiry_Date,[[NSUserDefaults standardUserDefaults]valueForKey:@"daysleft"]];
    label.font=[UIFont boldSystemFontOfSize:15];
    [self.view addSubview:label];
    
 
    noteLabel.backgroundColor=[UIColor clearColor];
    noteLabel.textColor=[UIColor grayColor];
    noteLabel.textAlignment=NSTextAlignmentCenter;
    noteLabel.adjustsFontSizeToFitWidth=YES;
    noteLabel.text=@"Please show this screen to the server and the server must click the confirm button below.";
    noteLabel.numberOfLines=2;
    noteLabel.font=[UIFont boldSystemFontOfSize:15];
    [self.view addSubview:noteLabel];

    //PLACE-HOLDER BUTTON
    checkinBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    if (!IS_IPHONE_5)
    {
        [checkinBtn setFrame:CGRectMake(10,330,300,30)];
    }else
    {
        [checkinBtn setFrame:CGRectMake(10,340,300,30)];
    }
    
    checkinBtn.layer.borderWidth=1.0;
    checkinBtn.layer.borderColor=[UIColor blackColor].CGColor;
    [checkinBtn addTarget:self action:@selector(sendidForReastaurant) forControlEvents:UIControlEventTouchUpInside];
    [checkinBtn setTitle:@"Confirm" forState:UIControlStateNormal];
    checkinBtn.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [checkinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    checkinBtn.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
   
    [self.view addSubview:checkinBtn];

    
}



// #pragma mark -  CheckMembership Plan
//-(void)checkPlan
//{
//    
//    [self showLoadingView];
//    
//    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
//    
//    
//    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
//    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
//    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//    
//    
//    NSLog(@"request=  %@",request);
//    [request setTimeoutInterval:180];
//    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setHTTPBody:postData];
//    connection= [[NSURLConnection alloc]initWithRequest:request delegate:self];
//    
//    
//}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
    [self hideLoadingView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDictionary=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"JSON= %@",dataDictionary);
    
    NSString *status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"success"]];
      NSString *check_status=[NSString stringWithFormat:@"%@",[dataDictionary valueForKey:@"status"]];
    
    NSLog(@"%@",status);
    if ([status isEqualToString:@"1"])
        
    {
        
        NSLog(@"%@",check_status);
        
        
        if ([check_status isEqualToString:@"1"])
        {
            checkinBtn.hidden=YES;
            //[checkinBtn removeFromSuperview];
          
            NSString * message=[NSString stringWithFormat:@"%@ Bite membership confirmed. Thank you.",[[NSUserDefaults standardUserDefaults]valueForKey:@"fullname"]];
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:message message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        else
        {
            
        }

  
        
    }
    else
    {
        [self hideLoadingView];
    }
    
   
    
}

//To check the network connection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
