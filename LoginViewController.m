//
//  RegisterViewController.m
//  BiteBC
//
//  Created by brst on 10/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "LoginViewController.h"
#import "SelectPlanViewController.h"
#import "MBProgressHUD.h"
#import "Register1ViewController.h"
#import "AppDelegate.h"
#import "RestaurantTableViewController.h"
#import "DetailPageTableViewController.h"
#import "SettingTableViewController.h"
#import "NearMeViewController.h"


@interface LoginViewController ()
{
    int flag;
}


@end
static int tag=0;
@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithRed:0.1647 green:0.1647 blue:0.1686 alpha:1.0];
    

    [self loginView];
    // Do any additional setup after loading the view.
}
-(void)navBarView
{
    UIView * navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]];
    [self.view addSubview:navView];
    
    UILabel * tittleLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 64)];
    tittleLbl.text=@"Login";
    tittleLbl.textAlignment=NSTextAlignmentCenter;
    tittleLbl.textColor=[UIColor whiteColor];
    tittleLbl.font=[UIFont fontWithName:@"Helvetica Neue" size:19];
    [navView addSubview:tittleLbl];
    
//    UIButton * doneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    doneBtn.frame=CGRectMake(5,30 ,50,25);
//    [doneBtn setImage:[UIImage imageNamed:@"bck.png"] forState:UIControlStateNormal];
//    //[doneBtn setTitle:@"Back" forState:UIControlStateNormal];
//    //[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    //doneBtn.titleLabel.font=[UIFont boldSystemFontOfSize:15.5];
//    [doneBtn addTarget:self action:@selector(done_Btn_Pressed) forControlEvents:UIControlEventTouchUpInside];
//    [navView addSubview:doneBtn];
    
    
}
-(void)done_Btn_Pressed
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self navBarView];
}


-(void)loginView
{
    
    UIScrollView * scroll_View=[[UIScrollView alloc]initWithFrame:CGRectMake(0,64, self.view.frame.size.width, self.view.frame.size.height-45)];
    scroll_View.backgroundColor=[UIColor clearColor];
    [self.view addSubview:scroll_View];
    
    profileImage=[[UIImageView alloc]initWithFrame:CGRectMake(110, 10, 100, 100)];
    profileImage.backgroundColor=[UIColor clearColor];
    profileImage.image=[UIImage imageNamed:@"icon_bitebc.png"];
    profileImage.layer.masksToBounds=YES;
    profileImage.layer.cornerRadius=5.0;
    [scroll_View addSubview:profileImage];
 
    
    
    
    lbl_free=[[UILabel alloc]initWithFrame:CGRectMake(15, profileImage.frame.size.height+profileImage.frame.origin.y+15,self.view.frame.size.width-30, 35)];
    lbl_free.text=@"Sign in with your account or register for free";
    lbl_free.textColor=[UIColor whiteColor];
    lbl_free.numberOfLines=2;
    lbl_free.adjustsFontSizeToFitWidth=YES;
    lbl_free.textAlignment=NSTextAlignmentCenter;
    lbl_free.backgroundColor=[UIColor clearColor];
    lbl_free.font=[UIFont systemFontOfSize:21];
    lbl_free.textAlignment=NSTextAlignmentCenter;
    [scroll_View addSubview:lbl_free];
    
    //PLACE-HOLDER BUTTON
    registerBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [registerBtn setFrame:CGRectMake(18,  lbl_free.frame.origin.y + lbl_free.frame.size.height+10, self.view.frame.size.width-36, 35)];
    registerBtn.layer.borderWidth=1.0;
    registerBtn.layer.borderColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0].CGColor;
    registerBtn.backgroundColor=[UIColor whiteColor];
    
    [registerBtn addTarget:self action:@selector(registerPressed) forControlEvents:UIControlEventTouchUpInside];
    [registerBtn setTitle:@"Register" forState:UIControlStateNormal];
    registerBtn.titleLabel.font=[UIFont boldSystemFontOfSize:16];
    registerBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
    registerBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    [registerBtn setTitleColor:[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0] forState:UIControlStateNormal];
    [scroll_View addSubview:registerBtn];
    

//    UIView * blackView=[[UIView alloc]initWithFrame:CGRectMake(0, profileImage.frame.origin.y + profileImage.frame.size.height+15, self.view.frame.size.width, 170)];
//    blackView.backgroundColor=[UIColor colorWithRed:0.6000 green:0.8000 blue:0.7961 alpha:1.0];
//    blackView.alpha=0.10;
//    [scroll_View addSubview:blackView];
//
//    lbl_text=[[UILabel alloc]initWithFrame:CGRectMake(10, profileImage.frame.size.height+profileImage.frame.origin.y+15,self.view.frame.size.width-20, 20)];
//    lbl_text.text=@"Remember the food and meals you love";
//    lbl_text.textColor=[UIColor whiteColor];
//    //registerLabel.numberOfLines=2;
//    lbl_text.adjustsFontSizeToFitWidth=YES;
//    lbl_text.textAlignment=NSTextAlignmentCenter;
//    lbl_text.backgroundColor=[UIColor clearColor];
//    lbl_text.font=[UIFont boldSystemFontOfSize:18];
//    lbl_text.textAlignment=NSTextAlignmentCenter;
//    [self.view addSubview:lbl_text];

     self.txt_login=[[UITextField alloc]initWithFrame:CGRectMake(12,  registerBtn.frame.origin.y + registerBtn.frame.size.height+30, self.view.frame.size.width-24, 35)];
    UIView *viewPadding2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    self.txt_login.leftView = viewPadding2;
    self.txt_login.leftViewMode = UITextFieldViewModeAlways;
    self.txt_login.layer.cornerRadius=3;
    self.txt_login.borderStyle=UITextBorderStyleNone;
    self.txt_login.tag=0;
    self.txt_login.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"];
    self.txt_login.backgroundColor=[UIColor whiteColor];
    self.txt_login.layer.borderWidth=1.0;
    self.txt_login.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txt_login.textAlignment=NSTextAlignmentLeft;
    self.txt_login.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txt_login.returnKeyType=UIReturnKeyNext;
    self.txt_login.keyboardType=UIKeyboardTypeEmailAddress;
    self.txt_login.delegate=self;
    self.txt_login.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Email"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    
    [scroll_View addSubview:self.txt_login];
    
    self.txt_password=[[UITextField alloc]initWithFrame:CGRectMake(12,  self.txt_login.frame.origin.y + self.txt_login.frame.size.height+10, self.view.frame.size.width-24, 35)];
    UIView *viewPadding7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    self.txt_password.leftView = viewPadding7;
    self.txt_password.leftViewMode = UITextFieldViewModeAlways;
    self.txt_password.layer.cornerRadius=3;
    self.txt_password.tag=1;
    self.txt_password.borderStyle=UITextBorderStyleNone;
    self.txt_password.returnKeyType=UIReturnKeyDone;
    self.txt_password.backgroundColor=[UIColor whiteColor];
    self.txt_password.layer.borderWidth=1.0;
    self.txt_password.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"password"];
    self.txt_password.layer.borderColor=[UIColor lightGrayColor].CGColor;
    //self.txtEmail.textAlignment=NSTextAlignmentLeft;
    self.txt_password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txt_password.keyboardType=UIKeyboardTypeEmailAddress;
    self.txt_password.delegate=self;
    self.txt_password.secureTextEntry=YES;
    self.txt_password.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"Password"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                 }
     ];
    [scroll_View addSubview:self.txt_password];
    
    //PLACE-HOLDER BUTTON
    loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setFrame:CGRectMake(18,  self.txt_password.frame.origin.y + self.txt_password.frame.size.height+20, self.view.frame.size.width-36, 35)];
    loginButton.layer.borderWidth=1.0;
    loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [loginButton addTarget:self action:@selector(login_Api_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    loginButton.titleLabel.font=[UIFont boldSystemFontOfSize:17];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];
    [scroll_View addSubview:loginButton];
    
    forgetButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [forgetButton setFrame:CGRectMake(loginButton.frame.size.width-150+18,  loginButton.frame.origin.y + loginButton.frame.size.height+10,150, 30)];
    [forgetButton addTarget:self action:@selector(forget_button_Pressed) forControlEvents:UIControlEventTouchUpInside];
    [forgetButton setTitle:@"Forgot password?" forState:UIControlStateNormal];
    //forgetButton.titleLabel.textAlignment=NSTextAlignmentRight;
    forgetButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    forgetButton.titleLabel.font=[UIFont systemFontOfSize:15];
    //forgetButton.backgroundColor=[UIColor colorWithRed:0.0000 green:0.3529 blue:0.3412 alpha:1.0];

    [forgetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scroll_View addSubview:forgetButton];

    
    
 
 
    if (!IS_IPHONE_5)
    {
        scroll_View.contentSize=CGSizeMake(self.view.frame.size.width, loginButton.frame.origin.y + loginButton.frame.size.height+255);
        
    }else
    {
        scroll_View.contentSize=CGSizeMake(self.view.frame.size.width, loginButton.frame.origin.y + loginButton.frame.size.height+200);
        
        
    }

}
#pragma mark - Forget_Button Method
-(void)forget_button_Pressed{

    alert_Field=[[UIAlertView alloc]initWithTitle:@"Please enter your email" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
alert_Field.tag=1;
    
    
   [alert_Field setAlertViewStyle:UIAlertViewStylePlainTextInput];

[alert_Field show];

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    

    NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex== 1 && alertView.tag==1)
    {
         textF=[alertView textFieldAtIndex:0];
        
        if(![self NSStringIsValidEmail:textF.text]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter valid email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=2;
            [alert show];
            
        }else
        {
            NSLog(@"%@",textF.text);
            flag=0;
            [self foregetApi:textF.text];
            
        }
        
        
    }
    else if (buttonIndex== [alertView cancelButtonIndex] && alertView.tag==2)
    {
        [alert_Field show];
    }
}

-(void)registerPressed
{
    Register1ViewController * registerVC=[[Register1ViewController alloc]init];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
   
    NSLog(@"button pressed");
    
}
#pragma mark - LoginButton Method
-(void)login_Api_Pressed
{
    if (self.txt_login.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if(self.txt_password.text.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"bitebcMode" forKey:@"login"];
        [[NSUserDefaults standardUserDefaults]setObject:self.txt_login.text forKey:@"mail"];
        [[NSUserDefaults standardUserDefaults]setObject:self.txt_password.text forKey:@"password"];
        
        
        
        [[NSUserDefaults standardUserDefaults]synchronize];

        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"login"]);
        
        [self postData];
    }
    
    
}
-(void)postcustomerID
{
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"coustomerid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"customerid"]];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@checkin.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    con= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    
}

-(void)foregetApi:(NSString *)emailText
{
    [self showLoadingView];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"email=%@",emailText];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *urlString=[NSString stringWithFormat:@"%@forgot_pwd.php?",post_url];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    NSLog(@"request=  %@",request);
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    con= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    

}

-(void)postData
{
    tag=0;
    flag=-1;
         NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"%@",uid);
    
     [self showLoadingView];
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@&section=%@",self.txt_login.text,self.txt_password.text,@"email"];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
 
    NSString *urlString=[NSString stringWithFormat:@"%@login.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    con= [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
    dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"JSON=  \n%@",dict);
    
    
    if (flag==0)
    {
        
        [self hideLoadingView];
        flag=1;
        tag=-1;
        
       
          NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        
        if ([sucess isEqualToString:@"success"])
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[dict valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }else
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Message" message:[dict valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    }
    if (tag==1)
    {
        
        tag=-1;
        
        NSString* sucess=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"status"]];
        
        NSString* daysleft=[NSString stringWithFormat:@"%@",[dict valueForKey:@"daysleft"]];
        
        NSString * imageis =[NSString stringWithFormat:@"%@",[dict valueForKey:@"profileimg_url"]];
        NSLog(@"%@",imageis);
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"daysleft"] forKey:@"daysleft"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"package_name"] forKey:@"pack"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"planid"] forKey:@"planid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        if  ([sucess isEqualToString:@"0"] && [status isEqualToString:@"not purchased"])
        {
            SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
            [self.navigationController pushViewController:selectPlan animated:YES];
        }
        else if([daysleft isEqualToString:@"0"])
        {
            if ([[dict valueForKey:@"status"] isEqualToString:@"paused"])
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your membership plan has expired" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Your Bite membership has expired please purchase a new membership plan." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
                [self.navigationController pushViewController:selectPlan animated:YES];
                
            }
            
            
        }
        
        else
            
        {
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"city"] forKey:@"city"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"cityid"] forKey:@"CITYID"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"filter"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"verified" forKey:@"verifyScreen"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self tabBar];
            
            
            [self hideLoadingView];
        }
        [self hideLoadingView];
        
    }
    
    
    if (tag==0)
    {
        NSString* status=[NSString stringWithFormat:@"%@",[dict valueForKey:@"success"]];
        if ([status isEqualToString:@"1"])
        {
            tag=1;
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"sessionid"] forKey:@"sessionId"];
         
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"customerid"] forKey:@"customerid"];
            
            
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"firstname"] forKey:@"first"];
             [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"lastname"] forKey:@"last"];
            
            NSString * fullName=[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"first"],[[NSUserDefaults standardUserDefaults]valueForKey:@"last"]];
            [[NSUserDefaults standardUserDefaults]setObject:fullName forKey:@"fullname"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
            //[[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"email"] forKey:@"mail"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            NSLog(@"customerid = %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mail"]);
            
            
            [self postcustomerID];
            //        SelectPlanViewController* selectPlan=[[SelectPlanViewController alloc]init];
            //        [self.navigationController pushViewController:selectPlan animated:YES];
            
        }
        else
        {
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[dict valueForKey:@"message"] message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [self hideLoadingView];
            
        }
        
        
        
    }
    
   }

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    [self hideLoadingView];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please check your network connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}
#pragma mark - UITabBarController Method
-(void)tabBar
{
    //    //self.tabBarController=[[UITabBarController alloc]init];
    //    self.tabBarController.delegate=self;
    //    [self.tabBarController.tabBar setTranslucent:NO];
    //    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]];
    //    self.tabBarController.tabBar.tintColor=[UIColor blackColor];
    //
    
    
    //    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    //    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"my.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //    restaurantTable.tabBarItem.title=@"Activity";
    //    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    //
    //
    //    //restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"activity.png"];
    //    restaurantTable.tabBarItem.tag=1;
    //
    //   [self.navigationController pushViewController:restaurantTable animated:YES];
    //
    //   // UINavigationController* navigateTOrestaurant=[[UINavigationController alloc]init];
    //
    //
    //
    //
    //    NSArray* array=[NSArray arrayWithObjects:navigateTOrestaurant, nil];
    //    self.tabBarController.viewControllers = array;
    //
    
    // create the tab controller and add the view controllers
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate=self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar.png"]];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:0.2431 green:0.6863 blue:0.6706 alpha:1.0];
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    RestaurantTableViewController* restaurantTable=[[RestaurantTableViewController alloc]init];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"my.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Restaurants";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"my.png"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    NearMeViewController* nearMeView=[[NearMeViewController alloc]init];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"nearme.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Near Me";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"nearme.png"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DetailPageTableViewController* searchView=[[DetailPageTableViewController alloc]init];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"search.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Search";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"search.png"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingTableViewController* settingView=[[SettingTableViewController alloc]init];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"settings.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"settings.png"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
    
    
    
    
    //    SecondView *secondView = [[SecondView alloc] initWithNibName:@"SecondView" bundle:nil];
    //    [viewControllers addObject:secondView];
    
    [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
    [self.navigationController pushViewController:self.tabBarController animated:YES];
    
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGRect frame1=self.view.frame;
    frame1.origin.y=-100;
    [self.view setFrame:frame1];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    CGRect frame1=self.view.frame;
    frame1.origin.y=0;
    [self.view setFrame:frame1];
    
}

 -(BOOL)textFieldShouldReturn:(UITextField *)textField{
 
     if (textField.tag==0)
     {         [self.txt_password becomeFirstResponder];
     }
     else
     {
         [textField resignFirstResponder];
     }
    
    return YES;
}
#pragma mark - Validation Email Method
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txt_login resignFirstResponder];
    [self.txt_login resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
#pragma mark - Loading View Method's
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
 
 /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
