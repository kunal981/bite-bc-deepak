//
//  FilterViewController.h
//  BiteBC
//
//  Created by brst on 7/7/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FilterViewController : UIViewController<NSURLConnectionDataDelegate>

{
    
    NSMutableArray *nameOfList,*foodList,*daysList,*offerArr;
    
    UIView *foodNameView;
    
    UIScrollView *backView,*cuisineBckView;
    
    UIButton *rightArrowButton,*leftArrowButton;
    
    UIButton *cuisineRightArrowButton,*cuisineLeftArrowButton;
    
  
    NSURLConnection *connection;
    
    NSMutableDictionary* dict;
    NSMutableData* data;
    UIView * dayView;
    
}

@end
