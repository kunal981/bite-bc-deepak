//
//  RegisterViewController.h
//  BiteBC
//
//  Created by brst on 10/23/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,UINavigationControllerDelegate,UITabBarControllerDelegate,UIAlertViewDelegate>
{
   UIButton* loginButton,*registerBtn,*forgetButton;
   UIImageView*  profileImage;
    
    UILabel * registerLabel,*lbl_text,*lbl_free;
    
    NSURLConnection* con;
    UITextField* textF;
    NSMutableData * data;
    NSMutableDictionary * dict;
    
    UIAlertView*  alert_Field;
}
-(void)tabBar;
@property(nonatomic,strong) UITabBarController* tabBarController;

@property(strong,nonatomic)UITextField *txt_login;
@property(strong,nonatomic)UITextField *txt_password;
 @end
