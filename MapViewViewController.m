//
//  MapViewViewController.m
//  BiteBC
//
//  Created by brst on 7/30/1936 SAKA.
//  Copyright (c) 1936 SAKA Karan Bharara. All rights reserved.
//

#import "MapViewViewController.h"
#import "MBProgressHUD.h"

@interface MapViewViewController ()
{
    
}

@end

@implementation MapViewViewController
 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:17],NSFontAttributeName,nil]];
    self.navigationController.navigationBar.translucent=NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar.png"]]];
    
    
     
}
-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor whiteColor]];
    self.view = view;
    
}

- (void)viewDidLoad
{
    
    self.navigationItem.title=self.nameOfRest;
    
    
    
    [self postMethod];

}
-(void)postMethod
{
    [self showLoadingView];
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    
    NSString *post = [NSString stringWithFormat:@"datasearch=%@",self.zipcode_str];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@nearby.php?",post_url];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    
    
    [request setTimeoutInterval:180];
    
   
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
  
    
    
    connectionIS= [[NSURLConnection alloc]initWithRequest:request delegate:self];

}
#pragma mark - Uiconnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    data=[[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
    [data appendData:theData];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        [self hideLoadingView];
    
    
    if (!IS_IPHONE_5)
    {
        self.mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0,0, 320, 370)];
    }
    else
    {
        self.mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0,0, 320, 460)];
        
    }
    self.mapView.delegate=self;
    self.mapView.zoomEnabled = YES;
    self.mapView.showsUserLocation=YES;
    self.mapView.mapType=MKMapTypeStandard;
    [self.view addSubview:self.mapView];
    
    

    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    dataDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"detail data of res is  = \n %@",dataDict);
    
    
    NSLog(@"%@",self.entityId_str);
    NSString * status= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"success"]];
    
     if ([status isEqualToString:@"0"])
     {
         
     }else
     {
         
         dataArr=[[NSMutableArray alloc]init];
         dataArr=[dataDict valueForKey:@"products"];
         for (int i=0; i<dataArr.count; i++)
         {
            detailDict=[[dataArr objectAtIndex:i]valueForKey:@"detail"];
             
             rest_id=[detailDict valueForKey:@"entity_id"];
            
             if([rest_id isEqualToString:self.entityId_str])
             {
                 latitude=[detailDict valueForKey:@"latitude"];
                 longitude=[detailDict valueForKey:@"longitude"];
                 restName=[detailDict valueForKey:@"name"];

                 
                 NSLog(@"%@,%@,%@",latitude,longitude,restName);

             }
         }
         
         self.locationManager=[[CLLocationManager alloc]init];
         self.locationManager.delegate=self;
        // self.locationManager.pausesLocationUpdatesAutomatically = NO;
         
         int value=[[[UIDevice currentDevice] systemVersion]intValue];
         NSLog(@"%d",value);
         
         if (value>=8)
         {
             [self.locationManager requestWhenInUseAuthorization];  
             [self.locationManager requestAlwaysAuthorization];

         }
         
                   [self.locationManager startUpdatingLocation];
         
         
         
         
     }
}
#pragma mark - Uiloading_view Methods
-(void)showLoadingView
{
    MBProgressHUD *hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = YES;
    hud.labelText = @"Loading..";
    hud.dimBackground = YES;
    
}
-(void)hideLoadingView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.locationManager stopUpdatingLocation];
    
    float lat=[latitude floatValue];
    float longitudeV=[longitude floatValue];
    
    NSLog(@"%f,%f",lat,longitudeV);
  
    CLLocation * currentLocation=[[CLLocation alloc]initWithLatitude:lat longitude:longitudeV];
    NSLog(@"%@",currentLocation);

    
    MKCoordinateSpan span;
    //You can set span for how much Zoom to be display like below
    span.latitudeDelta=.01;
    span.longitudeDelta=.01;
    
    //set Region to be display on MKMapView
    MKCoordinateRegion cordinateRegion;
    cordinateRegion.center=currentLocation.coordinate;
    cordinateRegion.span=span;
    //set That Region mapView
    [self.mapView setRegion:cordinateRegion animated:YES];

    
    
    CLLocationCoordinate2D corridnate;
    corridnate.latitude=lat;
    corridnate.longitude=longitudeV;
    
     MKPointAnnotation *notaion;
    notaion=[[MKPointAnnotation alloc]init];
    notaion.title=self.nameOfRest;
    [notaion setCoordinate:corridnate];
//    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:notaion];
    
    
   

 //[self.mapView setCenterCoordinate:notaion.coordinate animated:YES];
    
}
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != self.mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        
        //pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.canShowCallout = YES;
        //pinView.animatesDrop = YES;
        
        pinView.image = [UIImage imageNamed:@"map.png"];
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        imgView.image =[UIImage imageNamed:@"ppl1.jpeg"];
        
        pinView.leftCalloutAccessoryView =imgView;
        
        
        
        //as suggested by Squatch
    }
    else {
          }
    return pinView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
